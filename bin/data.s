.text
$printf:
move	$4	$30
$label_printf_Iter:
	lb	$2	0($30)
	ori	$5	$0	37
	beq	$5	$2	$label_printf_special_out
	bne	$0	$2	$label_printf_scan_next
		ori	$2	$0	4
		syscall
		j $label_printf_end
$label_printf_scan_next:
	add	$30	$30	1
	j $label_printf_Iter
$label_printf_end:
j	$31
$label_printf_special_out:
	beq	$30	$4	$label_printf_check_char
	lb	$5	0($30)
	sb	$0	0($30)
	ori	$2	$0	4
	syscall
	sb	$5	0($30)
$label_printf_check_char:
	lb	$4	1($30)
	$label_printf_d:	
	ori	$2	$0	'd'
		bne	$2	$4	$label_printf_c
			lw	$4	0($3)
			ori	$2	$0	1
			syscall
			add $3	$3	4
			add	$30	$30	2
			move	$4	$30
			j	$label_printf_Iter
	$label_printf_c:
	ori	$2	$0	'c'
		bne	$2	$4	$label_printf_s
			la	$4	$$L1
			lw	$2	0($3)
			sb	$2	0($4)
			ori	$2	$0	4
			syscall
			add	$4	$30	2
			add	$30	$30	2
			add $3	$3	4
			j	$label_printf_Iter
	$label_printf_s:	
	ori	$2	$0	's'
		bne	$2	$4	$label_printf_04d
			lw	$4	0($3)
			ori	$2	$0	4
			syscall
			add $3	$3	4
			add	$4	$30	2
			add	$30	$30	2
			move	$4	$30
			j	$label_printf_Iter
	$label_printf_04d:	
		ori	$2	$0	1	
		lw	$4	0($3)
		bge $4 	1000 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	100 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	10 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	1 $$$label_printf_num
		li	$4	0
		syscall
		$$$label_printf_num:
		lw	$4	0($3)
		syscall
			add $3	$3	4
			add	$30	$30	4
			move	$4	$30
			j	$label_printf_Iter

.data
.align 2
$$L0:
.space 1024
.align 2
$$L1:
.space 4
.align 2
$$L2:
.word 0
.align 2
$$L9:
.asciiz "%d %d\n\000"
.text
$$addSmall:
	sw  $ra 8($sp)
#
	lw $t1 0($sp) 
	lw $t0 0($sp) 
	lw $t2 0($t0)
	add $t0 $t2 1
	sw $t0 0($t1)
	lw $ra 8($sp) 
	j   $ra
.text
$$addMiddle:
	sw  $ra 16($sp)
	lw $a2 8($sp) 
	addu $t0 $sp 4
	sw $0 0($t0)
#
	li $a2 1 
$$L3:
	sle $t0 $a2 10
	beqz  $t0 $$L5
#
#
	addu $t1 $sp 4
	move $t0 $t1 
	sub $sp $sp 12
	sw  $t0 0($sp)
	sw  $a2 4($sp)
	jal  $$addSmall 
	lw $a2 4($sp) 
	add $sp $sp 12
#
	addu $t1 $sp 4
	lw $t2 4($sp) 
	add $t0 $t2 1
	sw $t0 0($t1)
$$L4:
	add $t0 $a2 1
	move $a2 $t0 
	j   $$L3
$$L5:
#
	lw $t1 0($sp) 
	lw $t0 0($sp) 
	lw $t2 0($t0)
	lw $t3 4($sp) 
	add $t0 $t2 $t3
	sw $t0 0($t1)
	sw  $a2 8($sp)
	lw $ra 16($sp) 
	j   $ra
.text
main:
	sub $sp $sp 20
	sw  $ra 16($sp)
	lw $a2 0($sp) 
	addu $t1 $sp 4
	la $t2 $$L2 
	move $t0 $t2 
	sw $t0 0($t1)
#
	li $a2 1 
$$L6:
	sle $t0 $a2 10
	beqz  $t0 $$L8
#
	addu $t0 $sp 8
	sw $0 0($t0)
#
	addu $t1 $sp 8
	move $t0 $t1 
	sub $sp $sp 20
	sw  $t0 0($sp)
	sw  $a2 12($sp)
	jal  $$addMiddle 
	lw $a2 12($sp) 
	add $sp $sp 20
#
	lw $t1 4($sp) 
	lw $t0 4($sp) 
	lw $t2 0($t0)
	lw $t3 8($sp) 
	add $t0 $t2 $t3
	sw $t0 0($t1)
#
	la $t1 $$L2 
	lw $t2 $$L2 
	add $t0 $t2 1
	sw $t0 0($t1)
#
	la $t0 $$L9 
	lw $t1 $$L2 
	la $v1 $$L0 
	sw  $a2 0($v1)
	sw  $t1 4($v1)
	move $s8 $t0 
jal $printf

$$L7:
	add $t0 $a2 1
	move $a2 $t0 
	j   $$L6
$$L8:
	sw  $a2 0($sp)
	lw $ra 16($sp) 
	add $sp $sp 20
	j   $ra
