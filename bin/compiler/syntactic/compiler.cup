package compiler.syntactic;

import java.io.InputStream;
import compiler.ast.*;

action code {:
	static compiler.symbol.Symbol symbol(String s) {
		return compiler.symbol.Symbol.symbol(s);
	}
:};

parser code {:
	public void report_error(String message, Object info) {
		StringBuffer m = new StringBuffer("\tParsing error");
		if (info instanceof java_cup.runtime.Symbol) {
			java_cup.runtime.Symbol s = (java_cup.runtime.Symbol) info;

			if (s.left >= 0) {
				m.append(" in line " + (s.left +1 ));
				if (s.right >= 0) {
					m.append(", column " + (s.right + 1));
				}
			}
		}
		m.append(" : " + message);
		System.err.println(m);
	}

	public Parser(InputStream inp) {
		this(new Lexer(inp));
	}
:};

terminal TYPEDEF, VOID, CHAR, INT, STRUCT, UNION, IF, ELSE, WHILE, FOR, CONTINUE, BREAK, SIZEOF, RETURN;
terminal LPAREN, RPAREN, SEMICOLON, COMMA, ASSIGN, LBRACE, RBRACE, LBRACKET, RBRACKET, VERTICAL, CARET, AND, LT, GT, PLUS, MINUS, STAR, DIVIDE, PERCENT, TILDA, EXCLAM, DOT;
terminal LOGOR, LOGAND, EQ, NE, LE, GE, SHL, SHR, INC, DEC, PTR, ELLIPSIS, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, ADD_ASSIGN, SUB_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, AND_ASSIGN, XOR_ASSIGN, OR_ASSIGN;
terminal	String ID;
terminal	Integer NUM;
terminal	String	STRING;
terminal	Character	CONSTCHAR;

non terminal Program			program;
non terminal Declaration		declaration;
non terminal FunctionDefinition	function_definition;
non terminal Parameters			parameters;
non terminal Parameters			plain_declarations_with_comma;
non terminal Declarators		declarators;
non terminal Declarators		declarators_with_comma;
non terminal InitDeclarators	init_declarators;
non terminal InitDeclarators	init_declarators_with_comma;
non terminal InitDeclarator		init_declarator;
non terminal Initializer		initializer;
non terminal Initializers		initializers_with_comma;
non terminal TypeSpecifier		type_specifier;
non terminal Members			type_specifiers_with_SEMICOLON;
non terminal TypeSpecifier.OpType	struct_or_union;
non terminal PlainDeclaration	plain_declaration;
non terminal Declarator			declarator;
non terminal BracketConstants	bracket_constants;
non terminal PlainDeclarator	plain_declarator;
non terminal Statement			statement;
non terminal ExpressionStatement	expression_statement;
non terminal CompoundStatement		compound_statement;
non terminal Declarations			declarations;
non terminal Statements				statements;
non terminal SelectionStatement		selection_statement;
non terminal IterationStatement		iteration_statement;
non terminal Expression				expression_or_not;
non terminal JumpStatement			jump_statement;

non terminal Expression		expression;
non terminal Expression		assignment_expression;
non terminal BinaryExpression.OpType	assignment_operator;
non terminal Expression		constant_expression;

non terminal Expression		logical_or_expression;
non terminal Expression		logical_and_expression;
non terminal Expression		inclusive_or_expression;
non terminal Expression		exclusive_or_expression;
non terminal Expression		and_expression;
non terminal Expression		equality_expression;
non terminal BinaryExpression.OpType	equality_operator;
non terminal Expression		relational_expression;
non terminal BinaryExpression.OpType	relational_operator;
non terminal Expression		shift_expression;
non terminal BinaryExpression.OpType	shift_operator;
non terminal Expression		additive_expression;
non terminal BinaryExpression.OpType	additive_operator;
non terminal Expression	multiplicative_expression;
non terminal BinaryExpression.OpType	multiplicative_operator;

non terminal Expression			cast_expression;

non terminal TypeName				type_name;
non terminal Stars					stars;

non terminal Expression		unary_expression;
non terminal UnaryExpression.OpType unary_operator_cast;
non terminal UnaryExpression.OpType unary_operator_unary;

non terminal Expression		postfix_expression;
non terminal Postfixs				postfixs;
non terminal Postfix				postfix;

non terminal Arguments				arguments;
non terminal Arguments				comma_arg_lists;

non terminal Expression		primary_expression;

non terminal Constant				constant;

precedence right	ASSIGN;
precedence nonassoc	EQ, NE, GT, LT, GE, LE;
precedence left		PLUS, MINUS;
precedence left		STAR, DIVIDE;
precedence left 	ELSE;

start with program;

//Remark:LALR allows left_recursion 
//Remark:Re op's

//____________________________Top_Level___________________________
program::= declaration:d program:p 				{: RESULT = new DeclarationProgram(d, p); :}  
		| function_definition:f program:p		{: RESULT = new FunctionProgram(f, p); :}
		| declaration:d							{: RESULT = new DeclarationProgram(d, null); :}
		| function_definition:f					{: RESULT = new FunctionProgram(f, null); :}
		;
//%c Program()
//%c DeclarationProgram(Declaration decl, Program prog) extends Program
//%c FunctionProgram(FunctionDefinition func, Program prog) extends Program

declaration::= type_specifier:ts init_declarators:ids SEMICOLON 	{: RESULT = new Declaration(ts, ids); :}
		| type_specifier:ts SEMICOLON							{: RESULT = new	Declaration(ts, null); :}
		;
//%c Declaration(TypeSpecifier typeSpec, InitDeclarators initDors)

function_definition::= type_specifier:ts plain_declarator:pd LPAREN parameters:ps RPAREN compound_statement:cs
			{: RESULT = new FunctionDefinition(ts, pd, ps, cs); :}
		| type_specifier:ts plain_declarator:pd LPAREN RPAREN compound_statement:cs
			{: RESULT = new FunctionDefinition(ts, pd, null, cs); :} 
		;
//%c FunctionDefinition(TypeSpecifier typeSpec, PlainDeclarator plainDor, Parameters paraList, CompoundStatement compStmt)

parameters::= plain_declaration:pd plain_declarations_with_comma:pc
			{: RESULT = new Parameters(pd, pc); :}
		;
		plain_declarations_with_comma::= COMMA plain_declaration:pd plain_declarations_with_comma:pc
				{: RESULT = new Parameters(pd, pc); :}
			| /* empty */ {: RESULT = null; :}
			;		
//plain_declaration:pd plain_declarations_with_comma:pc COMMA ELLIPSIS
//			{: RESULT = new EllipsisParameters(pd, pc); :}
//		| 
//%c Parameters(PlainDeclaration plainDon, Parameters paraList)
//%d EllipsisParameters(PlainDeclaration plainDon, Parameters paraList) extends Parameters

declarators::= declarator:d declarators_with_comma:dc						{: RESULT = new Declarators(d, dc); :}
	;
	declarators_with_comma::= COMMA declarator:d declarators_with_comma:dc	{: RESULT = new Declarators(d, dc); :}
		| /* empty */ 														{: RESULT = null; :}
		;
//%c Declarators(Declarator dor, Declarators dorList)

init_declarators::= init_declarator:d init_declarators_with_comma:dc						{: RESULT = new InitDeclarators(d, dc); :}
	;
	init_declarators_with_comma::= COMMA init_declarator:d init_declarators_with_comma:dc	{: RESULT = new InitDeclarators(d, dc); :}
		| /* empty */ 																		{: RESULT = null; :}
		;
//%c InitDeclarators(InitDeclarator initDor, InitDeclarators initDors)

init_declarator::= declarator:d				{: RESULT = new InitDeclarator(d, null); :}
	| declarator:d ASSIGN initializer:i		{: RESULT = new InitDeclarator(d, i); :}
	;
//%c InitDeclarator(Declarator dor, Initializer init)

initializer::= assignment_expression:ae											 {: RESULT = new Initializer(ae); :}
		| LBRACE initializer:i initializers_with_comma:ic RBRACE					 {: RESULT = new Initializers(i, ic); :}
		;
		initializers_with_comma::= COMMA initializer:i initializers_with_comma:ic {: RESULT = new Initializers(i, ic); :}
			| /* empty */														 {: RESULT = null; :}
			;
//%c Initializer(Expression exp); 
//%c Initializers(Initializer init, Initializers initList) extends Initializer

type_specifier::= VOID 					{: RESULT = new TypeSpecifier(TypeSpecifier.OpType.VOID, null, null); :}
	| CHAR 								{: RESULT = new TypeSpecifier(TypeSpecifier.OpType.CHAR, null, null); :}
	| INT 								{: RESULT = new TypeSpecifier(TypeSpecifier.OpType.INT, null, null); :}
	//| typedef_name
	| struct_or_union:su ID:i LBRACE type_specifiers_with_SEMICOLON:tss RBRACE		{: RESULT = new TypeSpecifier(su, symbol("Struct "+i), tss); :}
	| struct_or_union:su LBRACE type_specifiers_with_SEMICOLON:tss RBRACE			{: RESULT = new TypeSpecifier(su, null, tss); :}
	| struct_or_union:su ID:i 														{: RESULT = new TypeSpecifier(su, symbol("Struct "+i), null); :}
	;
	type_specifiers_with_SEMICOLON::= type_specifier:ts declarators:drs SEMICOLON			{: RESULT = new Members(ts, drs, null); :}
		| type_specifier:ts declarators:drs SEMICOLON type_specifiers_with_SEMICOLON:tss	{: RESULT = new Members(ts, drs, tss); :}
		;
struct_or_union::= STRUCT 				{: RESULT = TypeSpecifier.OpType.STRUCT; :}
	| UNION								{: RESULT = TypeSpecifier.OpType.UNION; :} //cheat
	;
//%c TypeSpecifier(TypeSpecifier.OpType op, compiler.symbol.Symbol sym, Members memList)
	//add enum OpType VOID CHAR INT STRUCT UNION
//%c Members(TypeSpecifier typeSpec, Declarators dorList, Members memList)

plain_declaration::= type_specifier:ts declarator:d				{: RESULT = new PlainDeclaration(ts, d); :}
	;
//%c PlainDeclaration(TypeSpecifier typeSpec, Declarator dor)

declarator::= plain_declarator:pd LPAREN parameters:ps RPAREN	{: RESULT = new FunctionDeclarator(pd, ps); :}
	| plain_declarator:pd LPAREN RPAREN							{: RESULT = new FunctionDeclarator(pd, null); :}
	| plain_declarator:pd bracket_constants:bc					{: RESULT = new DataDeclarator(pd, bc); :}
	;
	bracket_constants::= LBRACKET constant_expression:ce RBRACKET bracket_constants:bc	{: RESULT = new BracketConstants(ce, bc); :}
		| /* empty */													{: RESULT = null; :}
		;
//%c abstract Declarator()
//%c FunctionDeclarator(PlainDeclarator plainDor, Parameters paraList) extends Declarator
//%c DataDeclarator(PlainDeclarator plainDor, BracketConstants bracConList) extends Declarator
//%c BracketConstants(ConstantExpression consExp, BracketConstants bracConList)

plain_declarator::= stars:ss ID:i								
{: RESULT = new PlainDeclarator(ss, symbol(i));  :}
	;
//%c PlainDeclarator(Stars stars, compiler.symbol.Symbol sym)

//________________________________Statement______________________________________

statement::= expression_statement:s			{: RESULT = s; :}
	  | compound_statement:s				{: RESULT = s; :}
	  | selection_statement:s				{: RESULT = s; :}
	  | iteration_statement:s				{: RESULT = s; :}
	  | jump_statement:s					{: RESULT = s; :}
	  ;
//%c abstract Statement()

expression_statement::= expression:e SEMICOLON	{: RESULT = new ExpressionStatement(e); :}
	| SEMICOLON									{: RESULT = null; :} 
	;
//%c ExpressionStatement(Expression exp) extends Statement

compound_statement::= LBRACE declarations:ds statements:ss RBRACE		{: RESULT = new CompoundStatement(ds, ss); :}
	;
	declarations::= declaration:d declarations:ds						{: RESULT = new Declarations(d, ds); :}
		| /* empty */													{: RESULT = null; :}
		;	
	statements::= statement:s statements:ss								{: RESULT = new Statements(s, ss); :}
		| /* empty */													{: RESULT = null; :}
		;
//%c CompoundStatement(Declarations donList, Statements statList) extends Statement
//%c Declarations(Declaration don, Declarations donList)
//%c Statements(Statement stat, Statements statList)

selection_statement::= IF LPAREN expression:ie RPAREN statement:ts 			{: RESULT = new SelectionStatement(ie, ts, null); :}
	| IF LPAREN expression:ie RPAREN statement:ts ELSE statement:es			{: RESULT = new SelectionStatement(ie, ts, es); :}
	;
//%c SelectionStatement(Expression exp, Statement ifStat, Statement elseStat) extends Statement

iteration_statement::= WHILE LPAREN expression:e RPAREN statement:s			{: RESULT = new WhileIterationStatement(e, s); :}
				| FOR LPAREN expression_or_not:e1 SEMICOLON expression_or_not:e2 SEMICOLON expression_or_not:e3 RPAREN statement:s
					{: RESULT = new ForIterationStatement(e1, e2, e3, s); :}
				;	
	expression_or_not::= expression:e							{: RESULT = e; :}
		| /* empty */											{: RESULT = null; :}
		;
//%c IterationStatement() extends Statement
//%c ForIterationStatement(Expression exp1, Expression exp2, Expression exp3, Statement stat) extends IterationStatement
//%c WhileIterationStatement(Expression exp, Statement stat) extends IterationStatement

jump_statement::= CONTINUE SEMICOLON			{: RESULT = new ContinueJumpStatement(); :}
		   | BREAK SEMICOLON					{: RESULT = new BreakJumpStatement(); :}
		   | RETURN expression:e SEMICOLON		{: RESULT = new ReturnJumpStatement(e); :}
		   | RETURN SEMICOLON					{: RESULT = new ReturnJumpStatement(null); :}
		   ;
//%c JumpStatement() extends Statement
//%c ContinueJumpStatement() extends JumpStatement
//%c BreakJumpStatement() extends JumpStatement
//%c ReturnJumpStatement(Expression exp) extends JumpStatement

//________________________________Expressions___________________________________
//%c abstract Expression()

expression::= assignment_expression:agnExp										{: RESULT = agnExp; :}
	| assignment_expression:agnExp COMMA expression:exp							
		{: RESULT = new BinaryExpression(agnExp, BinaryExpression.OpType.COMMA, exp); :}
	;
//%c BinaryExpression(Expression lOprand, BinaryExpression.OpType op, Expression rOprand) extends Expression
	//add OpType COMMA ASSIGN MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN SUB_ASSIGN SHL_ASSIGN SHR_ASSIGN AND_ASSIGN XOR_ASSIGN OR_ASSIGN
		// EQ NE
		// LT GT LE GE
		// SHL SHR
		// PLUS MINUS
		// LOGOR LOGAND XOR
		// AND OR 
		// MUL DIV MOD
		

assignment_expression::= logical_or_expression:logOrExp							{: RESULT = logOrExp; :}
	| unary_expression:unaExp assignment_operator:agnOp assignment_expression:agnExp
		{: RESULT = new BinaryExpression(unaExp, agnOp, agnExp); :}
	;
assignment_operator::= ASSIGN 													{: RESULT = BinaryExpression.OpType.ASSIGN; :}
	| MUL_ASSIGN 																{: RESULT = BinaryExpression.OpType.MUL_ASSIGN; :}
	| DIV_ASSIGN 																{: RESULT = BinaryExpression.OpType.DIV_ASSIGN; :}
	| MOD_ASSIGN 																{: RESULT = BinaryExpression.OpType.MOD_ASSIGN; :}
	| ADD_ASSIGN 																{: RESULT = BinaryExpression.OpType.ADD_ASSIGN; :}
	| SUB_ASSIGN 																{: RESULT = BinaryExpression.OpType.SUB_ASSIGN; :}
	| SHL_ASSIGN 																{: RESULT = BinaryExpression.OpType.SHL_ASSIGN; :}
	| SHR_ASSIGN 																{: RESULT = BinaryExpression.OpType.SHR_ASSIGN; :}
	| AND_ASSIGN 																{: RESULT = BinaryExpression.OpType.AND_ASSIGN; :}
	| XOR_ASSIGN 																{: RESULT = BinaryExpression.OpType.XOR_ASSIGN; :}
	| OR_ASSIGN																	{: RESULT = BinaryExpression.OpType.OR_ASSIGN; :}
	;

constant_expression::= logical_or_expression:le	{: RESULT = new ConstantExpression(le); :}
	;
//%c ConstantExpression(Expression exp)

logical_or_expression::= logical_and_expression:logAndExp						{: RESULT = logAndExp; :}
	| logical_or_expression:logOrExp LOGOR logical_and_expression:logAndExp 
		{: RESULT = new BinaryExpression(logOrExp, BinaryExpression.OpType.LOGOR, logAndExp); :}
	;

logical_and_expression::= inclusive_or_expression:incOrExp						{: RESULT = incOrExp; :}
	| logical_and_expression:logAndExp LOGAND inclusive_or_expression:incOrExp 
		{: RESULT = new BinaryExpression(logAndExp, BinaryExpression.OpType.LOGAND, incOrExp); :}
	;

inclusive_or_expression::= exclusive_or_expression:excExp						{: RESULT = excExp; :}
	| inclusive_or_expression:incOrExp	 VERTICAL exclusive_or_expression:excExp 
		{: RESULT = new BinaryExpression(incOrExp, BinaryExpression.OpType.OR, excExp ); :}
	;

exclusive_or_expression::= and_expression:andExp								{: RESULT = andExp; :}
	| exclusive_or_expression:excOrExp CARET and_expression:andExp 
		{: RESULT = new BinaryExpression(excOrExp, BinaryExpression.OpType.XOR, andExp); :}
	;
	
and_expression::= equality_expression:equExp									{: RESULT = equExp; :}
	| and_expression:andExp AND equality_expression:equExp 
		{: RESULT = new BinaryExpression(andExp, BinaryExpression.OpType.AND, equExp); :}
	;

equality_expression::= relational_expression:relExp								{: RESULT = relExp; :}
	|  equality_expression:equExp equality_operator:equOp relational_expression:relExp
		{: RESULT = new BinaryExpression(equExp, equOp, relExp); :}
	;
equality_operator::= EQ 														{: RESULT = BinaryExpression.OpType.EQ; :}
	| NE																		{: RESULT = BinaryExpression.OpType.NE; :}
	;

relational_expression::= shift_expression:sftExp								{: RESULT = sftExp; :}
	|  relational_expression:relExp relational_operator:relOp shift_expression:sftExp
		{: RESULT = new BinaryExpression(relExp, relOp, sftExp); :}
	;
relational_operator::= LT														{: RESULT = BinaryExpression.OpType.LT; :}
	| GT																		{: RESULT = BinaryExpression.OpType.GT; :}
	| LE																		{: RESULT = BinaryExpression.OpType.LE; :}
	| GE																		{: RESULT = BinaryExpression.OpType.GE; :}
	;
	
shift_expression::= additive_expression:addExp									{: RESULT = addExp; :}
	| shift_expression:sftExp shift_operator:sftOp additive_expression:addExp 
		{: RESULT = new BinaryExpression(sftExp, sftOp, addExp); :}
	;
shift_operator::= SHL															{: RESULT = BinaryExpression.OpType.SHL; :}
	| SHR																		{: RESULT = BinaryExpression.OpType.SHR; :}
	;
	
additive_expression::= multiplicative_expression:mulExp							{: RESULT = mulExp; :}
	|  additive_expression:addExp additive_operator:addOp multiplicative_expression:mulExp
		{: RESULT = new BinaryExpression(addExp, addOp, mulExp); :}
	;
additive_operator::= PLUS														{: RESULT = BinaryExpression.OpType.PLUS; :}
	| MINUS																		{: RESULT = BinaryExpression.OpType.MINUS; :}
	;
	
multiplicative_expression::= cast_expression:cstExp								
		{: RESULT = new BinaryExpression(cstExp, null, null); :}	
	| multiplicative_expression:mulExp multiplicative_operator:mulOp cast_expression:cstExp
		{: RESULT = new BinaryExpression(mulExp, mulOp, cstExp); :}
	;
multiplicative_operator::= STAR													{: RESULT = BinaryExpression.OpType.MUL; :}
	| DIVIDE																	{: RESULT = BinaryExpression.OpType.DIV; :}
	| PERCENT																	{: RESULT = BinaryExpression.OpType.MOD; :}
	;

cast_expression::= unary_expression:ue							{: RESULT = ue; :}
			| LPAREN type_name:tn RPAREN cast_expression:ce		{: RESULT = new RealCastExpression(tn, ce); :}
			;
//%c abstract CastExpression() extends Expression
//%c RealCastExpression(TypeName typeName, CastExpression castExp) extends CastExpression

type_name::= type_specifier:ts stars:ss 	{: RESULT = new TypeName(ts, ss); :}
	;
	stars::= STAR stars:strs				{: RESULT = new Stars(strs); :}
		| /* empty */						{: RESULT = null; :}
		;
//%c TypeName(TypeSpecifier typeSpec, Stars stars)
//%c Stars(Stars stars)
	//add inc()

 
unary_expression::= postfix_expression:postExp									{: RESULT = postExp; :}
			 | unary_operator_unary:unaOp unary_expression:unaExp				{: RESULT = new UnaryExpression(unaOp, unaExp); :}
			 | unary_operator_cast:unaOp cast_expression:castExp				{: RESULT = new UnaryExpression(unaOp, castExp); :}
			 | SIZEOF LPAREN type_name:tn RPAREN								{: RESULT = new SizeOfTypeUnaryExpression(tn); :}
			 | SIZEOF unary_expression:unaExp									
			 	{: RESULT = new UnaryExpression(UnaryExpression.OpType.SIZEOF, unaExp); :}
			 ;
unary_operator_cast::= AND														{: RESULT = UnaryExpression.OpType.AND; :} 
	| STAR 																		{: RESULT = UnaryExpression.OpType.STAR; :} 
	| PLUS 																		{: RESULT = UnaryExpression.OpType.PLUS; :} 
	| MINUS 																	{: RESULT = UnaryExpression.OpType.MINUS; :} 
	| TILDA 																	{: RESULT = UnaryExpression.OpType.NOT; :} 
	| EXCLAM																	{: RESULT = UnaryExpression.OpType.LOGNOT; :} 
	;
unary_operator_unary::=INC														{: RESULT = UnaryExpression.OpType.INC; :}
	| DEC																		{: RESULT = UnaryExpression.OpType.DEC; :}	
	;

//%c UnaryExpression(UnaryExpression.OpType op, Expression exp) extends CastExpression
	//add static OpType AND STAR PLUS MINUS NOT LOGNOT INC SIZEOF DEC
//%c SizeOfTypeUnaryExpression(TypeName typeName) extends UnaryExpression

postfix_expression::= primary_expression:priExp postfixs:ps	{: RESULT = new PostfixExpression(priExp, ps); :} 
	;
	postfixs ::= postfix:p postfixs:ps					{: RESULT = new Postfixs(p, ps); :}
			| /* empty */								{: RESULT = null; :}
			;
//%c PostfixExpression(PrimaryExpression priExp, Postfixs postfixs) extends UnaryExpression
//%c Postfixs(Postfix postfix, Postfixs postfixs)

postfix::= LBRACKET expression:e RBRACKET			{: RESULT = new BracketPostfix(e); :}
	| LPAREN arguments:a RPAREN						{: RESULT = new ParenPostfix(a); :}
	| LPAREN RPAREN									{: RESULT = new ParenPostfix(null); :}
	| DOT ID:i										{: RESULT = new DotPostfix(symbol(i)); :}
	| PTR ID:i										{: RESULT = new PtrPostfix(symbol(i)); :}
	| INC											{: RESULT = new IncPostfix(); :}
	| DEC											{: RESULT = new DecPostfix(); :}
	;
//%c BracketPostfix(Expression exp) extends Postfix
//%c ParenPostfix(Arguments args) extends Postfix
//%c DotPostfix(compiler.symbol.Symbol sym) extends Postfix
//%c PtrPostfix(compiler.symbol.Symbol sym) extends Postfix
//%c IncPostfix() extends Postfix
//%c DecPostfix() extends Postfix
//%c abstract Postfix()

arguments::= assignment_expression:ae comma_arg_lists:c						{: RESULT = new Arguments(ae, c); :}
	;
	comma_arg_lists::= COMMA assignment_expression:ae comma_arg_lists:cl	{: RESULT = new Arguments(ae, cl); :}
		| /* empty */														{: RESULT = null; :}
		;
//%c Arguments(BinaryExpression binExp, Arguments args)

primary_expression::= ID:i							{: RESULT = new IdPrimaryExpression(symbol(i)); :}
	| constant:c									{: RESULT = new ConstantPrimaryExpression(c); :}
	| STRING:s										{: RESULT = new StringPrimaryExpression(s); :}
	| LPAREN expression:e RPAREN					{: RESULT = new ExpPrimaryExpression(e); :} 
	;
//%c abstract PrimaryExpression() extends Expression
//%c IdPrimaryExpression(compiler.symbol.Symbol sym) extends PrimaryExpression
//%c ConstantPrimaryExpression(Constant cons) extends PrimaryExpression
//%c StringPrimaryExpression(String str) extends PrimaryExpression
//%c ExpPrimaryExpression(Expression exp) extends PrimaryExpression

constant::= NUM:n				{: RESULT = new NumConstant(n.intValue()); :}
	 | CONSTCHAR:c				{: RESULT = new CharConstant(c.charValue()); :}
	;
//%c abstract Constant()
//%c NumConstant(int value) extends Constant
//%c CharConstant(char ch) extends Constant



      