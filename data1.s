.data
	operand:
		.word 8
	.align 2
	StringLabel:
		.ascii "Hellow%d%sBaby\000"
	.align 2
	operandString:
		.ascii "Word\000"
.text
$label_printf_special_out:
		beq	$30	$4	$label_printf_check_char
		lb	$5	0($30)
		
		sb	$0	0($30)
		ori	$2	$0	4
		syscall
		
		sb	$5	0($30)
		
	$label_printf_check_char:
		lb	$4	1($30)
		#add	$30	$30	2
		$label_printf_d:	
		ori	$2	$0	'd'
			bne	$2	$4	$label_printf_c
				lw	$4	operand
				ori	$2	$0	1
				syscall
				add	$30	$30	2
				move	$4	$30
				j	$label_printf_Iter
		$label_printf_c:
		ori	$2	$0	'c'
			bne	$2	$4	$label_printf_s
				sb	$2	1($30)	#correct:Operand
				add	$4	$30	1
				add	$30	$30	2
				j	$label_printf_Iter
		$label_printf_s:	
		ori	$2	$0	's'
			bne	$2	$4	$label_printf_Iter
				la	$4	operandString
				ori	$2	$0	4
				syscall
				add	$4	$30	2
				add	$30	$30	2
				move	$4	$30
				j	$label_printf_Iter

main:
	la	$30	StringLabel
	move	$4	$30
	ori $5	$0	37
	
	$label_printf_Iter:
		lb	$2	0($30)
		beq	$5	$2	$label_printf_special_out
		bne	$0	$2	$label_printf_scan_next
			ori	$2	$0	4
			syscall
			j $label_printf_end
	$label_printf_scan_next:
		add	$30	$30	1
		j $label_printf_Iter
	$label_printf_end:
	j $31
