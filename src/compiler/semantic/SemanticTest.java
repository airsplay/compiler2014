package compiler.semantic;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import compiler.ast.Program;

import compiler.type.*;
import compiler.syntactic.*;

public final class SemanticTest {
	
	public static void parse(String filename) throws IOException {
		InputStream inp = new FileInputStream(filename);
		
		Parser parser = new Parser(inp);
		java_cup.runtime.Symbol parseTree = null;
		try {
			parseTree = parser.parse();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Error(e.toString());
		} finally {
			inp.close();
		}
		
		Program prog = (Program) parseTree.value;
		Semantic semantic = new Semantic(prog);
		
	}

	public static void main(String argv[]) throws IOException {
		parse("src/compiler/main/example1.c");
	}
	
}