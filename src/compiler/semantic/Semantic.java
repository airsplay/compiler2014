/**Flag:
 * //todo 
 * //danger
 * //check
 * //phase3
 * //cheat
 * translate : changed for Translator
 */
// zuozhi 
//statment's value int?
//void
package compiler.semantic;
import compiler.ast.*;
import compiler.main.File;
import compiler.runtime.*;
import compiler.symbol.*;
import compiler.type.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.io.IOException;
import compiler.error.CompilerError;

public class Semantic implements RegisterConfig{
	Function nowFunc;
	int loopCnt;
	Environment env;
	boolean inLoopIterator = false;
	compiler.ast.ForIterationStatement nowFor = null;
	
	class LoopRegister{
		boolean b1 = false;
		boolean b2 = false;
		Register a1 = A2;
		Register a2 = A3;
		public void clear(){
			b1 = false;
			b2 = false;
		}
		public Register allocate(){
			if (!b1) {
				b1 = true;
				return a1;
			}
			else if (!b2) {
				b2 = true;
				return a2;
 			}
			else return null;
		}
	}
	LoopRegister loopRegister = new LoopRegister();
	public Semantic(Program prog) {
		loopCnt = 0;
		env = new Environment();
//		env.push(Symbol.symbol("malloc"), new Function(Int.instance(), new Pointer(new Int())));
//		semanticDecl(prog);
		
		semantic(prog);
	}
	
	private void semanticDecl(Program prog){
		if (prog == null) {
			return;
		}
		if (prog instanceof DeclarationProgram) {
			semantic((DeclarationProgram) prog);
			return;
		}
		else if (prog instanceof FunctionProgram) {
			semantic(((FunctionProgram) prog).prog);

		}
	}
	
	private void semantic(Program prog) /* throws */ {
		if (prog == null) {
			return;
		}
		if (prog instanceof DeclarationProgram) {
			semantic((DeclarationProgram) prog);
			return;
		}
		else if (prog instanceof FunctionProgram) {
			semantic((FunctionProgram) prog);
			return;
		}
	}

	private void semantic(DeclarationProgram prog) /* throws */ {
		if (prog == null) {
			return;
		}
		semantic(prog.decl);
		semantic(prog.prog);
	}

	private void semantic(FunctionProgram prog) /* throws */ {
		if (prog == null) {
			return;
		}
		semantic(prog.func);
		semantic(prog.prog);
	}

	private void semantic(Declaration don) /* throws */ {
		if (don == null) {
			return;
		}
		Type type;
		if (don.initDors == null){
			type = semantic(don.typeSpec, true);
		}
		else {
			type = semantic(don.typeSpec);
		}
		semantic(don.initDors, type);
	}

	private Function semantic(FunctionDefinition funcDef) /* throws */ {
		if (funcDef == null) {
			return null;
		}
		
		loopRegister.clear();
		Type type = semantic(funcDef.typeSpec);
		Field returnAndName = funPlainDeclarator(funcDef.plainDor, type);
		Function funcType = (Function) semantic(funcDef.paraList, returnAndName.type());
		env.push(returnAndName.name(), funcType);
		funcType.name = returnAndName.name().toString();
		
//		env.upLevel();
		Function iterFunc = funcType;
		Type tmpType;
		
		while (iterFunc.argumentName() != null){
			PRINT("arg " + iterFunc.argumentName().toString());
//			if (iterFunc.argumentType() instanceof Array){
//				iterFunc.argumentType = new Pointer(iterFunc.argumentType);
//			}
			VarInfo v = env.pushPara(iterFunc.argumentName(), iterFunc.argumentType());
			funcType.addArg(v);//translate 
			iterFunc = (Function) iterFunc.returnType();
		}
		funcType.finalReturnType = iterFunc.returnType();
		funcType.returnVarInfo = new VarInfo(funcType.finalReturnType);
		funcDef.type = funcType;
		
		nowFunc = funcType;
		semantic(funcDef.compStmt);
		nowFunc = null;
		
		
//		env.downLevel();
		return funcType;//can eliminate
	}

	private Type semantic(Parameters paraList, Type type) /* throws */ {
		if (paraList == null) {
			return new Function(type, (Type)null);
		}
//		if (paraList instanceof EllipsisParameters) {
//			semantic((EllipsisParameters) paraList);
//			return Function();
//		}
		Field argType = semantic(paraList.plainDon);
		Type returnType = semantic(paraList.paraList, type);
		
		//todo chekcType
		return new Function (returnType, argType);
	}

//	private void semantic(EllipsisParameters par) /* throws */ {
//		if (par == null) {
//			return;
//		}
//		semantic(par.plainDon);
//		semantic(par.paraList);
//	}

	private List<Field> semantic(Declarators dorList, Type type, List<Field> lField) /* throws */ {
		if (dorList == null) {
			return lField;
		}
		lField.add(semantic(dorList.dor, type));
		return semantic(dorList.dorList, type, lField);
	}

	private void semantic(InitDeclarators initDors, Type type) /* throws */ {
		if (initDors == null) {
			return;
		}
		semantic(initDors.initDor, type);
		semantic(initDors.initDors, type);
	}

	private void semantic(InitDeclarator initDor, Type type) /* throws */ {
		if (initDor == null) {
			return;
		}
		Field field = semantic(initDor.dor, type);
//		PRINT(field.type());
		initDor.varInfo = env.push(field.name(), field.type());
		PRINT(initDor.varInfo.type);
		if (nowFunc != null){
			nowFunc.addData(initDor.varInfo);
		}
		semantic(initDor.init, field.type());	//todo check assignment
	}

	private void semantic(Initializer init, Type type) /* throws */ {//check Type type or Field field
		if (init == null) {
			return;
		}
//		init.type = type;
//		Type type = field.type();
		if (init instanceof Initializers) {
			if (type instanceof Array){
				semantic((Initializers) init, ((Array)type).elementType());
			}
			else if (type instanceof Name){
				Type nameType = ((Name) type).type;
				if (nameType instanceof Struct){
					Iterator<Field> iter = ((Struct)nameType).fields().iterator();
					semantic((Initializers) init, iter);
				}
				else {
					//todo Union
				}
			}
			return;
		}
		Type typeExp = semantic(init.exp);
		//debug
//		if (typeExp instanceof Immediate){
//			File.out.println(111);
//		}
		if (!checkBinExp(type, "=", typeExp)){
			throw new Error("unmatched initial");
		}
	}
	
	
	private void semantic(Initializers initList, Type type) /* throws */ {
		if (initList == null) {
			return;
		}
		semantic(initList.init, type);
		semantic(initList.initList, type);
	}

//  Iterator it = list.iterator();
//  while (it.hasNext()) {
//      personnelID= (String) it.next();
//  }
	private void semantic(Initializers initList, Iterator<Field> iField) /* throws */{
		if (initList == null) {
			return;
		}
		if (iField.hasNext()){
			semantic(initList.init, (iField.next()).type());
			semantic(initList.initList, iField);
		}
	}
	private Type semantic(TypeSpecifier typeSpec){
		return semantic(typeSpec, false);
	}
	
	private Type semantic(TypeSpecifier typeSpec, boolean isPureDeclaration) /* throws */ {
		if (typeSpec == null) {
			return null;
		}//VOID CHAR INT STRUCT UNION
		switch (typeSpec.op) {
		case VOID:
			return new compiler.type.Void();
		case CHAR:
			return new compiler.type.Char();
		case INT:
			return new compiler.type.Int();
		case STRUCT:{
			
			Type nameType = env.get(typeSpec.sym);
			
			if ((nameType == null) && (typeSpec.sym != null)){ //struct A | A hasn't been seen before
				nameType = new Name();
				env.push(typeSpec.sym, nameType);
				((Name)nameType).setType(new Unknown());
			}
			
			List<Field> lField = new LinkedList<Field>();
			List<Field> finalList = semantic(typeSpec.memList, lField);
			
			env.upLevel();
			Iterator<Field> iter = finalList.iterator();
			while (iter.hasNext()){
				Field field = iter.next();
				env.push(field);
			}
			env.downLevel();
			
			if (typeSpec.sym == null){		//Anonymous
				nameType = new Name();
				((Name)nameType).setType(new compiler.type.Struct(finalList));
			}
			else { 
				if (!finalList.isEmpty()){			//Defination
					if (((Name)nameType).type instanceof Unknown){
						((Name) nameType).setType(new compiler.type.Struct(finalList));
					}
					else throw new Error("Multi-Definition of struct "+typeSpec.sym);
				}
			}
			return nameType;
		}
		case UNION:{
			Type nameType = env.get(typeSpec.sym);
			
			if ((nameType == null) && (typeSpec.sym != null)){ //struct A | A hasn't been seen before
				nameType = new Name();
				env.push(typeSpec.sym, nameType);
				((Name)nameType).setType(new Unknown());
			}
			
			List<Field> lField = new LinkedList<Field>();
			List<Field> finalList = semantic(typeSpec.memList, lField);
			
			env.upLevel();
			Iterator<Field> iter = finalList.iterator();
			while (iter.hasNext()){
				Field field = iter.next();
				env.push(field);
			}
			env.downLevel();
			
			if (typeSpec.sym == null){		//Anonymous
				nameType = new Name();
				((Name)nameType).setType(new compiler.type.Struct(finalList));
			}
			else { 
				if (!finalList.isEmpty()){			//Defination
					if (((Name)nameType).type instanceof Unknown){
						((Name) nameType).setType(new compiler.type.Struct(finalList));
					}
					else throw new Error("Multi-Definition of struct "+typeSpec.sym);
				}
			}
			
			if (nameType instanceof Name){
				((Struct) ((Name)nameType).type).isUnion = true;
			}
			return nameType;
		}
		default:
			return null;
		}
	}

	private List<Field> semantic(Members memList, List<Field> lField) /* throws */ {
		if (memList == null) {
			return lField;
		}
		Type type = semantic(memList.typeSpec);
		List<Field> tmp = semantic(memList.dorList, type, lField);
		return semantic(memList.memList, tmp);
	}

	private Field semantic(PlainDeclaration plainDon) /* throws */ {
		if (plainDon == null) {
			return null;
		}
		Type type = semantic(plainDon.typeSpec);
		return semantic(plainDon.dor, type);
	}
	private Field semantic(Declarator dor, Type type) /* throws */ {
		if (dor == null) {
			return null;
		}
		if (dor instanceof FunctionDeclarator) {
//			PRINT("funcDecl");
			return semantic((FunctionDeclarator) dor, type);
		}
		else if (dor instanceof DataDeclarator) {
//			PRINT("dataDecl");
			return semantic((DataDeclarator) dor, type);
		}
		return null;
	}

	private Field semantic(FunctionDeclarator funDor, Type type) /* throws */ {
		if (funDor == null) {
			return null;
		}
		env.upLevel();
		Field returnAndName = semantic(funDor.plainDor, type);
		Function func = (Function) semantic(funDor.paraList, returnAndName.type());
		env.downLevel();
		return new Field(func, returnAndName.name());
	}

	private Field semantic(DataDeclarator dataDor, Type type) /* throws */ {
		if (dataDor == null) {
			return null;
		}
		Field typeAndName = semantic(dataDor.plainDor, type);
		Type finalType;
		if (dataDor.bracConList != null) {
			finalType = semantic(dataDor.bracConList, typeAndName.type());
		}
		else {
			finalType = typeAndName.type();
		}
		return new Field(finalType , typeAndName.name());
	}

	private Type semantic(BracketConstants bracConList, Type type) /* throws */ {
		if (bracConList == null) {
			return type;
		}
		int index;
		Type cons = semantic(bracConList.consExp);
		if (cons instanceof Immediate){
			Immediate immediate = (Immediate) cons;
			if (immediate.type() instanceof Int) {
				index = ((Integer) (immediate.content())) . intValue();
			}
			else if (immediate.type() instanceof Char){
				index = (int) ((Character) immediate.content()).charValue();
			}
			else {
				throw new Error("the array's index is non-integer");
			}
		}
		else {
			throw new Error("the array's index is non-Constant");
		}
		return new Array(semantic(bracConList.bracConList, type), index);
	}

	private Field funPlainDeclarator(PlainDeclarator plainDor, Type type) /* throws */ {
//		PRINT("PlainDeclarator " + plainDor.sym.toString());
		if (plainDor == null) {
			return null;
		}
		Type finalType = semantic(plainDor.stars, type);		
		if (finalType instanceof Name){//因为所有不完整类型均会经过这里
			Type nameType = ((Name) finalType).type;
			if (nameType instanceof Unknown){
				throw new Error("Use a class without complete definitio");
			}
		}
		return new Field(finalType, semantic(plainDor.sym));
	}


	private Field semantic(PlainDeclarator plainDor, Type type) /* throws */ {
//		PRINT("PlainDeclarator " + plainDor.sym.toString());
		if (plainDor == null) {
			return null;
		}
		Type finalType = semantic(plainDor.stars, type);
//		finalType
		if (finalType instanceof Name){//因为所有不完整类型均会经过这里
			Type nameType = ((Name) finalType).type;
			if (nameType instanceof Unknown){
				throw new Error("Use a class without complete definitio");
			}
		}
		else if (finalType instanceof compiler.type.Void){
			throw new Error("Use Void besides functions");
		}
		return new Field(finalType, semantic(plainDor.sym));
	}

	
//---------------------------------------------------------------------------------
	private void semantic(Statement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		if (stat instanceof ExpressionStatement) {
			semantic((ExpressionStatement) stat);
			return;
		}
		else if (stat instanceof CompoundStatement) {
			semantic((CompoundStatement) stat);
			return;
		}
		else if (stat instanceof SelectionStatement) {
			semantic((SelectionStatement) stat);
			return;
		}
		else if (stat instanceof IterationStatement) {
			semantic((IterationStatement) stat);
			return;
		}
		else if (stat instanceof JumpStatement) {
			semantic((JumpStatement) stat);
			return;
		}
	}

	private void semantic(ExpressionStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		semantic(stat.exp);
	}

	private void semantic(CompoundStatement compStmt) /* throws */ {
		env.upLevel();
		if (compStmt == null) {
			return;
		}
		semantic(compStmt.donList);
		semantic(compStmt.statList);
		env.downLevel();
	}

	private void semantic(Declarations donList) /* throws */ {
		if (donList == null) {
			return;
		}
		semantic(donList.don);
		semantic(donList.donList);
	}

	private void semantic(Statements statList) /* throws */ {
		if (statList == null) {
			return;
		}
		semantic(statList.stat);
		semantic(statList.statList);
	}

	private void semantic(SelectionStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		Type type = semantic(stat.exp);
		if (!checkBinExp(Int.instance(), "=", type) ){
			throw new Error("If expression is not int-valued");
		}
		semantic(stat.ifStat);
		semantic(stat.elseStat);
	}

	private void semantic(IterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		loopCnt ++;
		if (stat instanceof ForIterationStatement) {
			semantic((ForIterationStatement) stat);
			loopCnt --;
			return;
		}
		else if (stat instanceof WhileIterationStatement) {
			semantic((WhileIterationStatement) stat);
			loopCnt --;
			return;
		}
	}

	private void semantic(ForIterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		semantic(stat.exp1);
		Type type = semantic(stat.exp2);
		if (!checkBinExp(Int.instance(), "=", type) ){
			throw new Error("for(a;b;c),and b is not int-valued");
		}
		
		nowFor = (ForIterationStatement) stat;
		this.inLoopIterator = true;
		semantic(stat.exp3);
		this.inLoopIterator = false;
		nowFor = null;
		
		semantic(stat.stat);
	}

	private void semantic(WhileIterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		Type type = semantic(stat.exp);
		if (!checkBinExp(Int.instance(), "=", type) ){
			throw new Error("While(a),and a is not int-valued");
		}
		semantic(stat.stat);
	}

	private void semantic(JumpStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		if (stat instanceof ContinueJumpStatement) {
			semantic((ContinueJumpStatement) stat);
			return;
		}
		else if (stat instanceof BreakJumpStatement) {
			semantic((BreakJumpStatement) stat);
			return;
		}
		else if (stat instanceof ReturnJumpStatement) {
			semantic((ReturnJumpStatement) stat);
			return;
		}
	}

	private void semantic(ContinueJumpStatement stat) /* throws */ {
		if (stat == null) {
			return; 
		}
		if (loopCnt == 0){
			throw new Error("Continue outside any loop.");
		}
	}

	private void semantic(BreakJumpStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		if (loopCnt == 0) {
			throw new Error("Break without loop.");
		}
	}

	private void semantic(ReturnJumpStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		semantic(stat.exp);
	}

	private Type semantic(Expression exp) /* throws */ {
//		Type type;
		if (exp == null) {
			return null;
		}
		if (exp instanceof BinaryExpression) {
			exp.type =  semantic((BinaryExpression) exp);
		}
		else if (exp instanceof CastExpression) {
			exp.type =  semantic((CastExpression) exp);
		}
		else if (exp instanceof PrimaryExpression) {
			exp.type =  semantic((PrimaryExpression) exp);
		}
		else if (exp instanceof ConstantExpression){
			exp.type =  semantic((ConstantExpression) exp);
		}
		else {
			exp.type = null;
		}
//		exp.exp.type = type;
		return exp.type;
	}

	private int calculate(int a, BinaryExpression.OpType op, int b){
		int flag = 0;
		
		switch (op) {
			case PLUS:	return (a + b);
			case MINUS:	return (a - b);
			case MUL: 	return (a * b);
			case DIV: 	return (a / b);
			case MOD: 	return (a % b);
			case SHL: 	return (a << b);
			case SHR: 	return (a >> b);
			case XOR: 	return (a ^ b);
			case AND: 	return (a & b);
			case OR:  	return (a | b); 
			default: flag = 1;
		}
		
		boolean tmp;
		
		switch(op) {
			case EQ: tmp = (a == b);	break;
			case NE: tmp = (a != b);	break;
			case LT: tmp = (a < b);		break;
			case GT: tmp = (a > b);		break;
			case LE: tmp = (a <= b);	break;
			case GE: tmp = (a >= b);	break;
			default: throw new Error("invalid opration via constant");
		}
		
		if (flag == 1){
			if (tmp) return 1;
			else return 0;
		}
		else {
			return 0; //danger
		}
	}
	
	private Integer calculate(Immediate l, BinaryExpression.OpType op, Immediate r){
		return new Integer (calculate (l.toInt(), op, r.toInt()));
	}
//	UnaryExpression AND STAR PLUS MINUS NOT LOGNOT INC SIZEOF DEC ENDING
	private int calculate(UnaryExpression.OpType op, int i){
		switch (op){
		case PLUS:
			return i;
		case MINUS:
			return -i;
		case NOT:
			if (i > 0){
				return 0;
			}
			else {
				return 1;
			}
		case LOGNOT:
			return ~i;
		default:
			return 21474836;
		}
	}
	private Integer calculate(UnaryExpression.OpType op, Immediate r){
		return new Integer (calculate(op, r.toInt()));
	}
	
	private boolean checkBinExp(Type lType, String op, Type rType){
		if (op == "="){
			
			if ((lType instanceof Name) && (rType instanceof Name)){
				if (lType.equals(rType)){
					return true;
				}
				else throw new Error("want a = b, a&b are both Name(sturct), but don't match");
			}
			if ((lType instanceof Array) && (rType instanceof Array)){
				if (lType.equals(rType)){
					return true;
				}
				else throw new Error("want a = b, Array but not Match");
			}
			else if ((lType instanceof Int) || (lType instanceof Char) || (lType instanceof Pointer)){
				if ((rType instanceof Int) || (rType instanceof Char) || (rType instanceof Array)
						|| (rType instanceof Function) ||(rType instanceof Pointer) || (rType instanceof Immediate)) {
					return true;
				}
				else {
					throw new Error("not match =");
				}
			}
			else 
				throw new Error("not match =");
		}
		else if (op == "+") {
			if (((lType instanceof Int) || (lType instanceof Char)) && 
					((rType instanceof Int) || (rType instanceof Char) || (rType instanceof Array)
							|| (rType instanceof Pointer) || (rType instanceof Function))){
				return true;
			}
			else if (((lType instanceof Pointer) || (lType instanceof Function) || (lType instanceof Array))
					&& ((rType instanceof Int) || (rType instanceof Char))){
				return true;
			}
			else throw new Error("not match +");

		}
		else if (op == "-"){
			if ((lType instanceof Function) && (rType instanceof Function) || 
				 (lType instanceof Array) && (rType instanceof Array) ||
				 (lType instanceof Pointer) && (rType instanceof Pointer) ){//f-f/a-a/p-p
				if (lType.equals(rType)){
					return true;
				}
				else throw new Error("want a - b, a&b are both Function/Pointer/Array, but doesn't same");
			}
			else if (((lType instanceof Function) || (lType instanceof Pointer) || (lType instanceof Array))
					 && ((rType instanceof Int) || (rType instanceof Char))){
				return true;
			}
			else if ((lType instanceof Array) && (rType instanceof Pointer)){//a-p
				if (((Array)lType).elementType().equals(((Pointer)rType).elementType())) return true;
			}
			else if ((lType instanceof Pointer) && (rType instanceof Array)){//p-a
				if (((Array)rType).elementType().equals(((Pointer)lType).elementType())) return true;
			}
			else if (((lType instanceof Int) || (lType instanceof Char)) && 
					((rType instanceof Int) || (rType instanceof Char))){
				return true;
			}
			else throw new Error("not match -");
		}
		else if (op == "*"){
			if (((lType instanceof Int) || (lType instanceof Char)) && 
					((rType instanceof Int) || (rType instanceof Char))){
				return true;
			}
			else throw new Error("a multiply-operator b, but a or b is not int-Valued");
		}
		else if (op == ","){
			return true;
		}
		else if (op == "<"){
			if ((lType instanceof Name) || (rType instanceof Name))
				throw new Error("not match boolType operator (encounter Struct)");
			return true;
		}
		return false;
	}
	
	
	private Type typeBinExp(Type lType, String string, Type rType){
		if (checkBinExp(lType, string ,rType)){
			if (string == ","){
				return rType;
			}
			else if (string == "*"){
				return new Int();
			}
			else if (string == "<"){
				return new Int();
			}
			else if ((string == "+") || (string == "=")){
				if (rType instanceof Pointer){
					return rType;
				}
				else if (rType instanceof Function){
					return rType;
				}
				else if (lType instanceof Array){//only +
					return new Pointer( ((Array)lType).elementType());
				}
				else if (rType instanceof Array){
					if (string == "+") return new Pointer( ((Array) rType).elementType());
				}
				if (((lType instanceof Int) || (lType instanceof Char)) && 
						((rType instanceof Int) || (rType instanceof Char))){
					return new Int();
				}
			}
			else if (string == "-"){
				if ((lType instanceof Pointer) || (lType instanceof Array)){
					if ((rType instanceof Pointer) || (rType instanceof Array)){
						return new Int();
					}
				}
				else if (lType instanceof Function){
					if (rType instanceof Function){
						return new Int();
					}
				}
			}
		}
		else throw new Error("Binary Expression doesn't match, but I don't know what it is");
		return lType;
	}
	
	private Type semantic(BinaryExpression binExp) /* throws */ {
		if (binExp == null) {
			return null;
		}
		Type lType = semantic(binExp.lOprand);
		Type rType = semantic(binExp.rOprand);
		if (rType == null){//It's a Unary Expression
			return lType;
		}
		//caculate the Constant
		if ((lType instanceof Immediate) && (rType instanceof Immediate)){
			Immediate l = (Immediate) lType;
			Immediate r = (Immediate) rType;
			//check the char*'s address if not decide until translator
			return new Immediate(Int.instance(), calculate(l, binExp.op, r));
		}
		else {
			if (lType instanceof Immediate) {
				if (binExp.op == null)
					return lType;
				else lType = ((Immediate) lType).type();
			}
			if (rType instanceof Immediate) rType = ((Immediate) rType).type();
		}
		
		if (binExp.op != null){
			if ((lType instanceof compiler.type.Void) || (rType instanceof compiler.type.Void))
				throw new Error("use Void in Binary Expression");
		}
		
		if (binExp.op != null){//如果op为空，表示为unaryExpression，否则为BinaryExpression
			if ((binExp.op == BinaryExpression.OpType.ASSIGN)//deal with lValue;
			|| (binExp.op == BinaryExpression.OpType.MUL_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.DIV_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.MOD_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.ADD_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.SUB_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.SHL_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.SHR_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.AND_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.XOR_ASSIGN )
			|| (binExp.op == BinaryExpression.OpType.OR_ASSIGN)){
//				if (!binExp.lOprand.lValue())
//					throw new Error("the left Oprand isn't lValued");
			}
//			BinaryExpression COMMA ASSIGN MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN SUB_ASSIGN SHL_ASSIGN SHR_ASSIGN
//				AND_ASSIGN XOR_ASSIGN OR_ASSIGN
//			 EQ NE
//			 LT GT LE GE
//			 SHL SHR
//			 PLUS MINUS
//			 LOGOR LOGAND XOR
//			 AND OR 
//			 MUL DIV MOD ENDING		
			switch(binExp.op){
			case ASSIGN:
				return typeBinExp(lType, "=", rType);		
			case PLUS:
			case ADD_ASSIGN:
				return typeBinExp(lType, "+", rType);		
			case MINUS:
			case SUB_ASSIGN:
				return typeBinExp(lType, "-", rType);
			case EQ:case NE:case LT:case GT:case LE:case GE:case LOGOR:case LOGAND:
				return typeBinExp(lType, "<", rType);
			case MUL_ASSIGN:case DIV_ASSIGN:case MOD_ASSIGN:case SHL_ASSIGN:case SHR_ASSIGN:case AND_ASSIGN:
			case OR_ASSIGN:case XOR_ASSIGN:case SHL:case SHR:case MUL:case DIV:case MOD:case XOR:case AND:case OR:
				return typeBinExp(lType, "*", rType);
			case COMMA:
				return typeBinExp(lType, ",", rType);
			default:
				throw new Error("unkown operation in Binary expression");
			}
		}
		else {
			return lType;
		}
	}

	private Type semantic(ConstantExpression consExp) /* throws */ { //todo
		if (consExp == null) {
			return null;
		}
		return semantic(consExp.exp);
	}

	private Type semantic(CastExpression castExp) /* throws */ {
		if (castExp == null) {
			return null;
		}
		if (castExp instanceof RealCastExpression) {
			return semantic((RealCastExpression) castExp);
		}
		else if (castExp instanceof UnaryExpression) {
			return semantic((UnaryExpression) castExp);
		}
		return null;
	}
	
	private Type semantic(RealCastExpression exp) /* throws */ {
		if (exp == null) {
			return null;
		}
		Type typeDest = semantic(exp.typeName);
		Type typeOrigin = semantic(exp.castExp);
		if (checkBinExp(typeDest, "=", typeOrigin)){
			if (typeOrigin instanceof Immediate){
				return typeOrigin;
			}
			else return typeDest;
		}
		else {
			throw new Error("Illigal explicit cast");
		}
	}

	private Type semantic(TypeName typeName) /* throws */ {
		if (typeName == null) {
			return null;
		}
		Type type = semantic(typeName.typeSpec);
		Type finalType = semantic(typeName.stars, type);
		return finalType;
	}

	private Type semantic(Stars stars, Type type) /* throws */ {
		if (stars == null) {
			return type;
		}
		return new Pointer(semantic(stars.stars, type));
	}
	
	private int sizeof(Type type){
		if (type instanceof Int){
			return 4;
		}
		else if (type instanceof Char){
			return 4;
		}
		else if (type instanceof Function){
			return 4;
		}
		else if (type instanceof Pointer){
			return 4;
		}
		else if (type instanceof Name){
			Type nameType = ((Name) type).type;
			if (nameType instanceof Unknown){//如果Struct没有定义报错
				throw new Error("size of uncomplete struct");
			}
			else{
				return sizeof(nameType);
			}
		}
		else if (type instanceof Struct){//sizeof Struct是所有成员之和
			Iterator<Field> iter = ((Struct) type).fields().iterator();
			int ans = 0;
			while (iter.hasNext()){
				Field field = iter.next();
				ans += sizeof(field.type());
			}
			return ans;
		}
		else if (type instanceof Union){
			return 0;//todo Union
		}
		else if (type instanceof Array){
			Array a = (Array) type;
			return (a.capacity() * sizeof(a.elementType()));
		}
		else if (type instanceof Immediate){
			return sizeof(((Immediate) type).type());
		}
		else throw new Error("want sizeof Unknown type(maybe void)");
	}
	
//	private int calculate(Immediate imme){
//		if (imme.type() instanceof Int){
//			return ((Integer)imme.content()).intValue();
//		}
//		else if (imme.type() instanceof Char){
//			return (int) ((Character) imme.content()).charValue();
//		}
//		else {
//			throw new Error("want the value of a Constant, but the constant is not int-values");
//		}
//	}
//	UnaryExpression AND STAR PLUS MINUS NOT LOGNOT INC SIZEOF DEC ENDING
	private Type semantic(UnaryExpression exp) /* throws */ {
		if (exp == null) {
			return null;
		}
		if (exp instanceof SizeOfTypeUnaryExpression) {
			return semantic((SizeOfTypeUnaryExpression) exp);
		}
		else if (exp instanceof PostfixExpression) {
			return semantic((PostfixExpression) exp);
		}
		
		Type type = semantic(exp.exp);
		PRINT(type);
		switch (exp.op){
		case AND:
			
			if (exp.exp.lValue()){
				exp.allowlValue();
			}
			else {
				throw new Error("& must be followed by lValue");
			}
			return new Pointer(type);
		case STAR:
			if (type instanceof Pointer){
				Pointer p = (Pointer) type;
				Type elementType = p.elementType();
				if (!(elementType instanceof compiler.type.Void)){
					exp.allowlValue();
				}
				if (elementType instanceof Name){
					Type nameType = ((Name) elementType).type;
					if (nameType instanceof Unknown){
						throw new Error("want *p, p point to a Struct but not Defined before");
					}
				}
				return (elementType);
			}
			else if (type instanceof Array){
				Array p = (Array) type;
				Type elementType = p.elementType();
				exp.allowlValue();
				if (elementType instanceof Name){
					Type nameType = ((Name) elementType).type;
					if (nameType instanceof Unknown){
						throw new Error("want *p, p point to a Struct but not Defined before");
					}
				}
				return (elementType);				
			}
			else {
				throw new Error("want (*a), but a is not a Pointer");
			}
		case PLUS:
			if (type instanceof Immediate){
				return new Immediate(Int.instance(), calculate(exp.op, (Immediate)type));
			}
			if ((type instanceof Char) || (type instanceof Int))
				return type;
			else {
				throw new Error("want (+a), but a is not integer-valued");
			}
		case MINUS:
			if (type instanceof Immediate){
				return new Immediate(Int.instance(), calculate(exp.op, (Immediate)type));
			}
			if ((type instanceof Char) || (type instanceof Int))
				return type;
			else {
				throw new Error("want (-a), but a is not integer-valued");
			}
		case NOT:
			if (type instanceof Immediate){
				return new Immediate(Int.instance(), calculate(exp.op, (Immediate)type));
			}
			if ((type instanceof Char) || (type instanceof Int))
				return type;
			else {
				throw new Error("want (!a), but a is not integer-valued");
			}
		case LOGNOT:
			if (type instanceof Immediate){
				return new Immediate(Int.instance(), calculate(exp.op, (Immediate)type));
			}
			if ((type instanceof Char) || (type instanceof Int))
				return type;
			else {
				throw new Error("want (~a), but a is not integer-valued");
			}
		case SIZEOF:
			return new Immediate(Int.instance(), sizeof(type));
		case INC://todo lOperand
			if ((type instanceof Char) || (type instanceof Int) || (type instanceof Pointer) || 
					(type instanceof Function))
				return type;
			else {
				throw new Error("want (++a), but a is not satisfied");
			}
		case DEC://todo lOperand
			if ((type instanceof Char) || (type instanceof Int) || (type instanceof Pointer) || 
					(type instanceof Function))
				return type;
			else {
				throw new Error("want (--a), but a is not satisfied");
			}
		default:throw new Error("what the operation of UnaryExpression it is?");
		}
			
	}

	private Immediate semantic(SizeOfTypeUnaryExpression exp) /* throws */ {
		if (exp == null) {
			return null; 
		}
		Type type = semantic(exp.typeName);
		return new Immediate(Int.instance(), new Integer(sizeof(type)));
	}

	private Type semantic(PostfixExpression exp) /* throws */ {
		if (exp == null) {
			return null;
		}
//		PRINT("postfix");
		Type type = semantic(exp.priExp);
//		PRINT("postfix ending");
//		PRINT(type);
		Type finalType = semantic(exp.postfixs, type, exp.priExp.lValue());
//Stack 
		
//		if (type instanceof Name){
//			if (exp.priExp.lValue()){
//				PRINT("Name Allow");
//				exp.allowlValue();
//			}
//		}
//		else if (finalType.lValue()){
//			exp.allowlValue();
//		}
//		if (exp.lValue()){
////			PRINT("ALLOWLVALUE");
//		}
		if (exp.postfixs == null){
			if (exp.priExp.lValue())
				exp.allowlValue();
		}
		else 
			if (exp.postfixs.lValue())
				exp.allowlValue();
		return finalType;
	}

	private Type semantic(Postfixs postfixs, Type type, boolean lValue) /* throws */ {
		if (postfixs == null) {
			return type;
		}
		Type tmpType = semantic(postfixs.postfix, type, lValue);
		PRINT(tmpType);
		Type finalType = semantic(postfixs.postfixs, tmpType, postfixs.postfix.lValue());
		if (postfixs.postfixs == null){
			if (postfixs.postfix.lValue())
				postfixs.allowlValue();
		}
		else {
			if (postfixs.postfixs.lValue())
				postfixs.allowlValue();
		}
		return finalType;
	}
	
	private Type semantic(Postfix postfix, Type type, boolean lValue) /* throws */ {
		if (postfix == null) {
			return type;
		}
		postfix.type = type;
		if (postfix instanceof BracketPostfix) {
			return semantic((BracketPostfix) postfix, type);
		}
		else if (postfix instanceof ParenPostfix) {
			return semantic((ParenPostfix) postfix, type);
		}
		else if (postfix instanceof DotPostfix) {
			return semantic((DotPostfix) postfix, type, lValue);
		}
		else if (postfix instanceof PtrPostfix) {
			return semantic((PtrPostfix) postfix, type);
		}
		else if (postfix instanceof IncPostfix) {
			return semantic((IncPostfix) postfix, type, lValue);
		}
		else if (postfix instanceof DecPostfix) {
			return semantic((DecPostfix) postfix, type, lValue);
		}
		return null;
	}
	private Type semantic(BracketPostfix postfix, Type type) /* throws */ {
		if (postfix == null) {
			return null;
		}
		Type index = semantic(postfix.exp);
		
		if (!((index instanceof Int) || (index instanceof Char))){//判断下标是否为整形
			if (index instanceof Immediate){
				Type iType = ((Immediate) index).type();
				if (!((iType instanceof Int) || (index instanceof Char))){//判断下标是否为整形
					throw new Error("the index can't convert to int");
				}
			}
			else throw new Error("the index can't convert to int");
		}
		
		if (type instanceof Array){//只有数组或者指针可能用到下标
			if (((Array) type).elementType().lValue())//如果元素类型可以作为左值，无问题
				postfix.allowlValue();
			return ((Array) type).elementType();
		}
		else if (type instanceof Pointer){
			if (((Pointer) type).elementType().lValue())
				postfix.allowlValue();
			return ((Pointer) type).elementType();
		}
		else {
			throw new Error("want get index of not Array or Pointer");
		}
	}

	private Type semantic(ParenPostfix postfix, Type type) /* throws */ {
		if (postfix == null) {
			return null;
		}
		if (type instanceof SpecialFunction){
			semantic_without_check(postfix.args);
			postfix.type = type;
			return type;
		}	
		else if (type instanceof Function){
			postfix.type = type;
			return semantic(postfix.args, (Function) type);
		}
		else {
			throw new Error("I don't know this Function");
		}
	}
	private void semantic_without_check(Arguments args){
		if (args == null) {
			return;
		}
		semantic(args.binExp);
		semantic_without_check(args.args);
	}
	private Type semantic(Arguments args, Function func) /* throws */ {
//		PRINT("arguments");
		if (args == null) {
			if (func.argumentType() != null){
				throw new Error("when call Function, the arguments'num doesn't match: Def:f(int) use:f()");
			}
			else return func.returnType();
		}
		Type type = semantic(args.binExp);
		args.type = type;
//		PRINT(type);
		PRINT(func.argumentType());
		if (checkBinExp(func.argumentType(), "=", type) 
			|| ((type instanceof Array) && (func.argumentType() instanceof Array))){
			Type returnType = func.returnType();
			if (returnType instanceof Function) {
				return semantic(args.args, (Function)returnType);
			}
			else {
				if (args.args == null){
					return func.returnType();
				}
				else throw new Error("more arguments : Def:f() use f(int)");
			}
		}
		else {
			throw new Error("the call of function's argument doesn't match: Def:f(int) Use:f(struct)");
		}
	}

	private Field find(List<Field> lField, Symbol sym){
		Iterator<Field> iter = lField.iterator();
		while (iter.hasNext()){
			Field field = iter.next();
			if (field.name() == sym){
				return field;
			}
		}
		return null;
	}
	
	private Type semantic(DotPostfix postfix, Type type, boolean lValue) /* throws */ {
		if (postfix == null) {
			return null;
		}
		//phase3 Union
		if (type instanceof Name){
			Type nameType = ((Name) type).type;
			if (nameType instanceof Unknown){
				throw new Error("want (a.X), but a is only declaration");
			}
			else {
				Struct s = (Struct) nameType;
				Field field = find(s.fields(), postfix.sym);
				
				if (field == null){
					throw new Error("want (a.X) , but no such field X");
				}
				else {
					if (lValue){
						if (field.type().lValue())
							postfix.allowlValue();
					}
					return field.type();
				}
			}
		}
		else {
			throw new Error("want (a.X), but a is not a Struct or Union");
		}
	}

	private Type semantic(PtrPostfix postfix, Type type) /* throws */ {
		if (postfix == null) {
			return null;
		}
		
		//todo Union
		if (type instanceof Pointer){
			Type insideType = ((Pointer) type).elementType();
			if (insideType instanceof Name){
//				Type nameType = env.get( ( (Name) insideType).sym() );
				Type nameType = ((Name)insideType).type;
				if (nameType instanceof Unknown){
					throw new Error("want (a->X), but a is only declaration");
				}
				else {
					Struct s = (Struct) nameType;
					Field field = find(s.fields(), postfix.sym);
					if (field == null){
						throw new Error("want (a->X) , but no such field X");
					}
					else {
						postfix.allowlValue();
						return field.type();
					}
				}
			}
			else {
				throw new Error("want (a->X), but a is a Pointer but not point to a Struct or Union");
			}
		}
		else {
			throw new Error("want (a->X), but a is not a Pointer");
		}
	}

	private Type semantic(IncPostfix postfix, Type type, boolean lValue) /* throws */ {//todo lOperand
		if (postfix == null) {
			return type;
		}
		if (lValue){
			if (type instanceof Char){
				PRINT("CHAR Correct");
				return type;
			}
			else if (type instanceof Int) {
				return type;
			}
			else if (type instanceof Pointer) {
				return type;
			}
			else {
				throw new Error("want a++, but a is not such Type");
			}
		}
		else {
			throw new Error("want a++, but a is not lValue");
		}
	}

	private Type semantic(DecPostfix postfix, Type type, boolean lValue) /* throws */ {
		if (postfix == null) {
			return type;
		}
		if (lValue){
			if (type instanceof Char){
				return type;
			}
			else if (type instanceof Int) {
				return type;
			}
			else if (type instanceof Pointer) {
				return type;
			}
			else {
				throw new Error("want a--, but a is not such Type");
			}
		}
		else {
			throw new Error("want a--, but a is not lValue");
		}
	}





	private Type semantic(PrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return null;
		}
		if (priExp instanceof IdPrimaryExpression) {
			return semantic((IdPrimaryExpression) priExp);
		}
		else if (priExp instanceof ConstantPrimaryExpression) {
			return semantic((ConstantPrimaryExpression) priExp);
		}
		else if (priExp instanceof StringPrimaryExpression) {
			return semantic((StringPrimaryExpression) priExp);			
		}
		else if (priExp instanceof ExpPrimaryExpression) {
			return semantic((ExpPrimaryExpression) priExp);
		}
		return null;
	}

	private Type semantic(IdPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return null;
		}
		
		String s = priExp.sym.toString();
//		if (s == "printf"){//phase3
//			return new SpecialFunction("printf");
//		}
//		else if (s == ){
//			
//		}
//		Iterator<String> iter = SpecialFunction.funcList.iterator();
		for (int i = 0; i < SpecialFunction.funcList.length; i++){
			String funcName = SpecialFunction.funcList[i];
			if (s == funcName){
				return new SpecialFunction(s);
			}
		}
//		while (iter.hasNext()){
//			String funcName = iter.next();
//			if (s == funcName){
//				return new SpecialFunction(s);
//			}
//		}
		
		
		
		PRINT(priExp.sym.toString());
		Type type = env.get(priExp.sym);
		priExp.varInfo = env.getVarInfo(priExp.sym);
		//PRINT(priExp.varInfo.toString());
		if (type == null){
			throw new Error("No such identifier");
		}
		if (type.lValue())
			priExp.allowlValue();
		
		if (this.inLoopIterator && (priExp.varInfo.register == null) && (type instanceof Int)){
			PRINT("inLoopIterator");
			priExp.varInfo.register = loopRegister.allocate();
			if (priExp.varInfo.register != null){
				nowFunc.loopRegList.add(priExp.varInfo);
			}
			this.inLoopIterator = false;
			nowFor.register = priExp.varInfo.register;
		}
		
		return	type;
	}

	private Type semantic(ConstantPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return null;
		}
		return semantic(priExp.cons);
	}

	private Type semantic(StringPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return null;
		}
		return semantic(priExp.str);
	}

	private Type semantic(ExpPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return null;
		}
		if (priExp.exp.lValue())
			priExp.allowlValue();
		return semantic(priExp.exp);
	}

	private Immediate semantic(Constant cons) /* throws */ {
		if (cons == null) {
			return null;
		}
		if (cons instanceof NumConstant) {
			return semantic((NumConstant) cons);
			
		}
		else if (cons instanceof CharConstant) {
			return semantic((CharConstant) cons);
			
		}
		return null;
	}

	private Immediate semantic(NumConstant numCons) /* throws */ {
		if (numCons == null) {
			return null;
		}
		return semantic(numCons.value);
	}

	private Immediate semantic(CharConstant charCons) /* throws */ {
		if (charCons == null) {
			return null;
		}
		return semantic(charCons.ch);
	}

	private Symbol semantic(Symbol sym){
		if (sym == null){
			return null;
		}
		else {
			if (sym.toString() instanceof String){
//				File.out.println(sym.toString());
			}
			return sym;
		}
	}
	
	private Immediate semantic(int i){
		File.out.println(i);
		return new Immediate(new Int(), new Integer(i));
	}
	
	private Immediate semantic(char ch){
		return new Immediate(new Char(), new Character(ch));
//		File.out.println(ch);
	}
	
	private Immediate semantic(String str){
		return new Immediate(new Pointer(new Char()), str.intern());//note can eliminate CharArray()
//		File.out.println(str);
	}
	
	private void PRINT(String s){
		File.out.println(s);
	}
	private void PRINT(Type type){
		if (type instanceof Pointer){
			File.out.println("Pointer");
		}
		if (type instanceof Int){
			File.out.println("Int");
		}
		if (type instanceof Array){
			File.out.println("Array");
		}
		if (type instanceof Function){
			File.out.println("Function");
		}
		
		if (type instanceof Name){
			File.out.println("Name");
		}
//		if (type instanceof Int){
//			File.out.println("Int");
//		}
	}
}
