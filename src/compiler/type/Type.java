package compiler.type;

public abstract class Type {
	public boolean isPara = false;
	
	abstract public boolean lValue();
	abstract public int size();
	@Override
	abstract public boolean equals(Object obj);
}
