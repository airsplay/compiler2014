package compiler.type;

public class Void extends Type {
	@Override
	public boolean lValue(){
		return false;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Void)){
			return false;
		}
		else return true;
		
	}
	@Override
	public int size(){
		return 0;
	}

}
