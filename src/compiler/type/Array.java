package compiler.type;

public final class Array extends Type {
	Type elementType;
	int capacity;
	
	@Override
	public boolean lValue(){
		return false;
	};
	
	public Array(Type x, int y){
		elementType = x;
		capacity = y;
	}
	
	public Type elementType(){
		return elementType;
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Array)){
			return false;
		}
		else {
			if (this.elementType.equals( ((Array)obj).elementType() ) ){
				return true;
			}
			else return false;
		}
	}
	
	public int capacity(){
		return capacity;
	}
	
	@Override
	public int size(){
		return this.capacity * elementType.size();
	}
}
