package compiler.type;

public final class Pointer extends Type {
	@Override
	public boolean lValue(){
		return true;
	};
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Pointer)){
			return false;
		}
		else {
			if (this.elementType.equals( ((Pointer)obj).elementType() ) ){
				return true;
			}
			else return false;
		}
	}
	@Override
	public int size(){
		return 4;
	}
	
	Type elementType;
	public Pointer(Type t){
		elementType = t;
	}
	
	public Type elementType(){
		return elementType;
	}
}
