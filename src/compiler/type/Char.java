package compiler.type;

public final class Char extends Type {
	@Override
	public boolean lValue(){
		return true;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Char)){
			return false;
		}
		else return true;
		
	}
	@Override
	public int size(){
		return 1;
	}
}
