package compiler.type;
import java.util.Iterator;
import java.util.List;

import compiler.symbol.Symbol;
public final class Union extends Record {
	@Override
	public boolean lValue(){
		return true;
	};
	@Override
	public boolean equals(Object obj){
		
		if (!(obj instanceof Union)){
			return false;
		}
		else if (fields.equals(((Union)obj).fields)){
			return true;
		}
		else return false;
		
	}
	@Override
	public int size(){
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			ans += field.type().size();
		}
		return ans;
	}
	
	public Union(List<Field> lr){
		fields = lr;
	}
	
	@Override
	public int find(Symbol sym){
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			if (sym == field.name){
				return ans;
			}
			ans += field.type.size();
		}
		return -1;
	}
	
	@Override 
	public Type getType(Symbol sym){
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			if (sym == field.name){
				return field.type;
			}
		}
		return null;
	}
}
