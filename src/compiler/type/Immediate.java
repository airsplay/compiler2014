package compiler.type;
import compiler.quad.Operand;
public final class Immediate extends Type implements Operand{
	@Override
	public boolean lValue(){
		return false;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Immediate)){
			return false;
		}
		else return true;
		
	}
	@Override
	public int size(){
		return type.size();
	}
	@Override
	public String toString() {
		return content.toString();
	}
	
	Type type;
	Object content;
	
	public Immediate(Type x, Object y){
		type = x;
		content = y;
	}
	
	public Immediate(int size) {
		type = new Int();
		content = new Integer(size);
	}
	public Type type(){
		return type;
	}
	
	public Object content(){
		return content;
	}
	
	public int toInt(){
		if (type instanceof Int){
			return ((Integer) content).intValue();
		}
		else{
			return (int) ((Character) content).charValue();
		}
	}
	
	public void add(int x){
		content = ((Integer) content) + x;
	}
}
