package compiler.type;

public final class Unknown extends Type {
	@Override
	public boolean lValue(){
		return false;
	};
	@Override
	public boolean equals(Object obj){
		return false;
	}
	@Override
	public int size(){
		return 0;
	}

}
