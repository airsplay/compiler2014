//如果两个类需要使用同一个方法调用，理应是把他们放在一个Super Object里面
package compiler.type;
import compiler.symbol.Symbol;
import compiler.runtime.Memory;

import compiler.runtime.Register;
import compiler.runtime.VarInfo;
import java.util.List;
import java.util.LinkedList;

import compiler.quad.FunctionLabel;
import compiler.quad.Label;
import compiler.runtime.RegisterConfig;
public class Function extends Type implements RegisterConfig{
	@Override
	public boolean lValue(){
		return false;
	};
	@Override
	public boolean equals(Object obj){
		
		if (!(obj instanceof Struct)){
			return false;
		}
		else {
			Function func = (Function) obj;
			if (argumentType.equals(func.argumentType) && (returnType.equals(func.returnType))){
				return true;
			}
			else return false;
		}
		
	}
	@Override
	public int size(){
		return 4;
	}
	
	public Type finalReturnType;
	public Type returnType;
	public Type argumentType;
    public Symbol argumentName;
    
	public List<VarInfo> dataList = new LinkedList<VarInfo>();
	public List<VarInfo> argList = new LinkedList<VarInfo>();
    public List<Register> regList = new LinkedList<Register>();
    public List<Memory> regMemList = new LinkedList<Memory>();
    public List<VarInfo> loopRegList = new LinkedList<VarInfo>();
    
	public VarInfo returnVarInfo;
	public Immediate memSize = new Immediate(0);
	public String name;
    public FunctionLabel label;
    public Memory memRA;
	
    public Function() {
    	
    }
    
    public Function(Type x, Type y) {
    	returnType = x;
    	argumentType = y;
    	argumentName = null;
    }
    
    public Function(Type x, Field y){
    	returnType = x;
    	argumentType = y.type();
    	argumentName = y.name();
    }
    
    public Type returnType(){
    	return returnType;
    }
    
    public Type argumentType(){
    	return argumentType;
    }
    public Symbol argumentName(){
    	return argumentName;
    }
    
    public void addData(VarInfo v){
    	dataList.add(v);
    }
    public void addArg(VarInfo v){
    	argList.add(v);
    }
    public void addSavedReg(Register r){
    	regList.add(r); 
    	allocateReg();
    }
    
    public void align(){
    	int module = memSize.toInt() % 4;
    	if (module != 0){
    		memSize.add(4-module);
    	}
    }
    public void allocate(VarInfo v){
    	if (v.mem instanceof Register){
    		return;
    	}
    	if (!(v.type instanceof Char)) align();
    	((Memory)v.mem).set(memSize.toInt(), SP);
    	memSize.add(v.type.size());
    }
    public void allocate_arg(VarInfo v){
    	if (!(v.type instanceof Char)) align();
    	((Memory)v.mem).set(memSize.toInt(), SP);
    	if (v.type instanceof Array)
    		memSize.add(4);
    	else 
    		memSize.add(v.type.size());
    }
    public Memory allocateReg(){
    	align();
    	Memory mem = new Memory();
    	regMemList.add(mem);
    	mem.set(memSize.toInt(), SP);
    	memSize.add(SIZE_OF_REGISTER);
    	return mem;
    }
}
