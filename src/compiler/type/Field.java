package compiler.type;

import compiler.symbol.Symbol;
public class Field {
	
	
	Type type;
	Symbol name;
	public Field(Type x, Symbol y){
		type = x;
		name = y;
	}
	public Type type(){
		return type;
	}
	
	public Symbol name(){
		return name;
	}
}
