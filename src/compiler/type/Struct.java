package compiler.type;

import java.util.List;
import java.util.Iterator;
import compiler.type.Field;
import compiler.symbol.*;

public final class Struct extends Record {
	public boolean isUnion = false;
	@Override
	public boolean lValue(){
		return true;
	};
	
	public Struct(List<Field> lr){
		this.fields = lr;
	}
	public List<Field> fields(){
		return fields;
	}
	
	@Override
	public boolean equals(Object obj){
		
		if (!(obj instanceof Struct)){
			return false;
		}
		else {
			boolean flag = true;
			Iterator<Field> iter = fields.iterator();
			Iterator<Field> iter2 = ((Record)obj).fields.iterator();
			while (iter.hasNext()){
				if (!iter2.hasNext()) {
					flag = false;
					break;
				}
				else {
					if (iter2.next().equals(iter.next())){
					}
					else {
						flag = false;
					}
				}
			}
			if (iter2.hasNext()) flag = false;
			return flag;
		}
		
	}
	private int align(int a){
		int alignSize = 4;
		if (a % alignSize != 0){
			a += (alignSize - a % alignSize);
		}
		return a;
	}
	@Override
	public int size(){
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			if (!(field.type() instanceof Char)){
				ans = align(ans);
			}
			ans += field.type().size();
		}
		ans = align(ans);
		return ans;
	}
	
	@Override
	public int find(Symbol sym){
		if (isUnion) return 0;
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			if (!(field.type() instanceof Char)){
				ans = align(ans);
			}
			if (sym == field.name){
				return ans;
			}
			ans += field.type.size();
		}
		return -1;
	}
	
	@Override 
	public Type getType(Symbol sym){
		Iterator<Field> iter = fields.iterator();
		int ans = 0;
		while (iter.hasNext()){
			Field field = iter.next();
			if (sym == field.name){
				return field.type;
			}
		}
		return null;
	}
}
