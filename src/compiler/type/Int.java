package compiler.type;

public final class Int extends Type {
	@Override
	public boolean lValue(){
		return true;
	};
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Int)){
			return false;
		}
		else return true;
		
	}
	@Override
	public int size(){
		return 4;
	}
	
	static public Int example = new Int(); 
	static public Int instance(){
		return example;
	}
}
