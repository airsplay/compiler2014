package compiler.type;

import java.util.List;

import compiler.symbol.Symbol;
public abstract class Record extends Type {
	List<Field> fields;
	abstract public int find(Symbol sym);
	abstract public Type getType(Symbol sym);
	
}
