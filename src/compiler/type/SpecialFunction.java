package compiler.type;

public final class SpecialFunction extends Function {
//	static SpecialFunction example = new SpecialFunction();
	public static String[] funcList = {"malloc", "printf"};
	public String name;
	
	public SpecialFunction (String s) {
		name = s;
	}
	
	@Override
	public boolean lValue() {
		return false;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof SpecialFunction)){
			return false;
		}
		else return true;
		
	}
	@Override
	public int size(){
		return 4;
	}

}
