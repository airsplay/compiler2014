package compiler.type;

import java.util.Iterator;
import compiler.symbol.Symbol;

public final class Name extends Type {
	@Override
	public boolean lValue(){
		return true;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Name)){
			return false;
		}
		else if (name == ((Name) obj).name){
			return true;
		}
		else return false;
		
	}
	@Override
	public int size(){
		return type.size();
	}
	
	
	Symbol name;
	public Type type;
	static int cnt = 0;
	public Name(Symbol sym){
		String s = sym.toString();
		name = Symbol.symbol("_Class_" + s);
	}
	public Name(){
		cnt ++;
		name = Symbol.symbol("_AnonymousClass_" + cnt);
	}
	
	public void setType(Type finalType){
//		System.out.println("SET");
		type = finalType;
	}
	
	public Symbol sym(){
		return name;
	}
	

	
	public static Symbol newSym(Symbol sym){
		String s = sym.toString();
		return Symbol.symbol("_Class_" + s);
	}
}
