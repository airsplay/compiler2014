package compiler.type;

public final class CharArray extends Type{
	@Override
	public boolean lValue(){
		return false;
	};
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof CharArray)){
			return false;
		}
		else return true;
	}
	@Override
	public int size(){
		return 0;
	}
}
