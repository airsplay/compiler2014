package compiler.syntactic;

%%

%class Lexer 			
// push into class lexer
%unicode				 
// use unicode
%cup					
// use cup
%line					
// use lineflag yyline
%column 				
// use columnflag yycolumn
%implements Symbols	 	
// interfaces

%{
	private int commentCount = 0;
	StringBuffer stringContent = new StringBuffer();
	char charBuffer;

	private void err(String message) {
		System.out.println("Scanning error in line " + yyline + ", column " + yycolumn + ": " + message);
	}

	private java_cup.runtime.Symbol tok(int kind) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn);
	}

	private java_cup.runtime.Symbol tok(int kind, Object value) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn, value);
	}
	private int hec (String string){
		int ans = 0;
		for (int i = 2; i < string.length(); i++){
			char ch = string.charAt(i);
			if ((ch >= '0') && (ch <= '9'))
				ans = ans*16 + ch-'0';
			else if ((ch >= 'A') && (ch <= 'Z'))
				ans = ans*16 + ch-'A'+10;
			else if ((ch >= 'a') && (ch <= 'z'))
				ans = ans*16 + ch-'a'+10;
		}
		return ans;
	}
	private int dec (String string)  {
		int ans = 0;
		for (int i = 0; i < string.length(); i++){
			char ch = string.charAt(i);
			if ((ch >= '0') && (ch <= '9'))
				ans = ans*10 + ch-'0';
		}
		return ans;
	}
	private int oct (String string){
		int ans = 0;
		for (int i = 1; i < string.length(); i++){
			char ch = string.charAt(i);
			if ((ch >= '0') && (ch <= '7'))
				ans = ans*8 + ch-'0';
		}
		return ans;
	}
%}

%eofval{ 			//the things to do when file ending 
	{
//		if (yystate() == YYCOMMENT) {
//			err("Comment symbol do not match (EOF)!");
//		}
		return tok(EOF, null);
	}
%eofval}


//-----------------------------------------------------------------


LineTerm = \n | \r | \r\n
OctInteger = 0 [0-7]+
DecInteger = ([1-9]  [0-9]*) | 0
HecInteger = 0 [xX] [0-9a-zA-Z]+

Whitespace = {LineTerm} | [ \t\f]


Letter = [a-zA-Z]
Digit = [0-9]

Identifier = {Letter} ({Letter} | {Digit} | [_$])*
InputChar = [^\n\r]
lineComment = "//" {InputChar}* 


%state SSTRING
%state LINECOMMENT
%state COMMENT
%state SCHAR
%% 

<YYINITIAL> {
	{lineComment}	{ }
	"#"				{ yybegin(LINECOMMENT);	}
	"//"			{ yybegin(LINECOMMENT); }
	"/*"			{ yybegin(COMMENT);		}

	{OctInteger} 	{ return tok(NUM, new Integer( oct(yytext()) ) ); }
	{HecInteger}	{ return tok(NUM, new Integer( hec(yytext()) ) ); }
	{DecInteger}	{ return tok(NUM, new Integer( yytext() ) ); }

	\"  			{ stringContent.setLength(0);yybegin(SSTRING); }

 	"typedef"		{ return tok(TYPEDEF); }
 	"void"			{ return tok(VOID); }
 	"char"			{ return tok(CHAR); }
 	"int"			{ return tok(INT);	}
 	"struct"		{ return tok(STRUCT); }
 	"union"			{ return tok(UNION); }
 	"if"			{ return tok(IF); }
 	"else"			{ return tok(ELSE); }
 	"while" 		{ return tok(WHILE); }
 	"for"			{ return tok(FOR);	}
 	"continue"		{ return tok(CONTINUE); }
 	"break"			{ return tok(BREAK);	}
 	"sizeof"		{ return tok(SIZEOF);}
 	"return"		{ return tok(RETURN); }

	"("				{ return tok(LPAREN); }
	")" 			{ return tok(RPAREN); } 
	";" 			{ return tok(SEMICOLON); }
	"," 			{ return tok(COMMA); } 
	"=" 			{ return tok(ASSIGN); }
	"{" 			{ return tok(LBRACE); }
	"}" 			{ return tok(RBRACE); }
	"[" 			{ return tok(LBRACKET); } 
	"]" 			{ return tok(RBRACKET); } 
	"*"				{ return tok(STAR); } 
	"|" 			{ return tok(VERTICAL); }
	"^" 			{ return tok(CARET); } 
	"&" 			{ return tok(AND); } 
	"<" 			{ return tok(LT); } 
	">" 			{ return tok(GT); } 
	"+" 			{ return tok(PLUS); }
	"-"				{ return tok(MINUS); }
	"/" 			{ return tok(DIVIDE); }
	"%" 			{ return tok(PERCENT); } 
	"~" 			{ return tok(TILDA); } 
	"!" 			{ return tok(EXCLAM); } 
	"." 			{ return tok(DOT); } 

	"||" 			{ return tok(LOGOR); }
	"&&" 			{ return tok(LOGAND); }
	"==" 			{ return tok(EQ); }
	"!=" 			{ return tok(NE); }
	"<=" 			{ return tok(LE); }
	">=" 			{ return tok(GE); }
	"<<" 			{ return tok(SHL); }
	">>" 			{ return tok(SHR); }
	"++" 			{ return tok(INC); }
	"--"			{ return tok(DEC); }
	"->" 			{ return tok(PTR); }
	"..."			{ return tok(ELLIPSIS); }
	"*=" 			{ return tok(MUL_ASSIGN); }
	"/=" 			{ return tok(DIV_ASSIGN); }
	"%=" 			{ return tok(MOD_ASSIGN); }
	"+=" 			{ return tok(ADD_ASSIGN); }
	"-=" 			{ return tok(SUB_ASSIGN); }
	"<<=" 			{ return tok(SHL_ASSIGN); }
	">>=" 			{ return tok(SHR_ASSIGN); }
	"&=" 			{ return tok(AND_ASSIGN); }
	"^="			{ return tok(XOR_ASSIGN); }
	"|=" 			{ return tok(OR_ASSIGN); }
	

	'				{ yybegin(SCHAR); }
	{Identifier} 	{ return tok(ID, yytext()); }
	{Whitespace} 	{ /* skip */ }
	
	[^] { throw new RuntimeException("Illegal character " + yytext() + " in line " + (yyline + 1) + ", column " + (yycolumn + 1)); }
}

<SSTRING> {
	\" { yybegin(YYINITIAL);
		 return tok(STRING, stringContent.toString()); 
		}
	[^\n\r\"\\]+ 	{ stringContent.append( yytext() ); }
	\\t 			{ stringContent.append("\\t"); }
	\\n     		{ stringContent.append("\\n"); }
	\\r 			{ stringContent.append("\\r"); }
	\\\"			{ stringContent.append("\\\""); }
	\\ 				{ stringContent.append("\\"); }
}

<LINECOMMENT> {
	"\n" | "\r"		{ yybegin(YYINITIAL); }
	[^]				{ /* skep */ }
	
}

<COMMENT> {
	~"*/"			{ yybegin(YYINITIAL); }
}

<SCHAR>	{
	'			{  yybegin(YYINITIAL);
					return tok(CONSTCHAR, new Character(charBuffer)); }
	\\n			{ charBuffer = '\n'; }
	\\{OctInteger} 	{ charBuffer =  (char)oct(yytext()); }
	\\{HecInteger}	{ charBuffer = (char)hec(yytext()); }
	\\{DecInteger}	{ charBuffer = (char)dec(yytext()); }
	
	[^]		{ charBuffer = yytext().charAt(0); }
}