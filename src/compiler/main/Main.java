package compiler.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;


import compiler.ast.Program;
import compiler.semantic.Semantic;
import compiler.syntactic.Parser;
import compiler.translate.*;
import compiler.codegen.*;

public class Main {

	public static String pathOf(String filename) {
		return Main.class.getResource(filename).getPath();
	}

	private static void compile(String filename) throws IOException {
		File.init();
//		File.out.println('n');
//		File.out.print('\n');
//		File.out.println('n');
		InputStream inp = new FileInputStream(filename);
		FileOutputStream out=new FileOutputStream("data.s");
        PrintStream p=new PrintStream(out);
        
		Parser parser = new Parser(inp);
		Integer a = new Integer(1);
		Integer b = new Integer(2);
		Character c = new Character('c');
		Integer d = c % a;
		java_cup.runtime.Symbol parseTree = null;
		File.out.println(d);
		try {
			File.out.println("startParse");
			parseTree = parser.parse();
			File.out.println("endParse");
		} catch (Throwable e) {
			File.out.println("catch");
			 e.printStackTrace();
			System.exit(1);
		} finally {

			inp.close();
		}
		Program prog = (Program) parseTree.value;
		try {
			Semantic semantic = new Semantic(prog);
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
		Translator translator = new Translator(prog);
		new CodeGenerator(p);
		System.exit(0);
		// File.out.print("2");
		//
		// PrintWriter out = new PrintWriter(new FileOutputStream(
		// filename.replace(".c", ".s")));
		// out.println("########################################");
		// out.close();
		// File.out.println(filename.replace(".c", ".s"));
	}

	public static void main(String argv[]) throws IOException {
		//System.out.println(argv[0]);
		compile(argv[0]);
		// compile(pathOf("example2.c"));
		// compile(pathOf("example3.c"));
	}
}
