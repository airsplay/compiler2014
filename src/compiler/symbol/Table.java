package compiler.symbol;

import compiler.symbol.Symbol;
import compiler.type.*;
import compiler.runtime.Memory;
import compiler.runtime.VarInfo;
//import compiler.type.Field;



public class Table {
	class Binder{
		Binder pre;
		Binder last;
		int level;
		Symbol sym;
		VarInfo info;
		
		Binder(Binder a, Binder b, int c, Symbol d, VarInfo v){
			pre = a;
			last = b;
			level = c;
			sym = d;
			info = v;
		}
	}
	
	private java.util.Dictionary<Symbol, Binder> dict = new java.util.Hashtable<Symbol, Binder>();
	private Binder now = null;
	private int level = 0;
	
	public Table() {
	}
	
	public void upLevel(){
		System.out.println("UpLevel");
		level++;
	}
	
	public VarInfo get(Symbol sym){
		Binder a = dict.get(sym);//if there's no such sym, return null
		if (a == null)
			return null;
		else
			return a.info;
	}
	
	public void downLevel(){
		System.out.println("DownLevel");
		while ((now != null) && (now.level == level)){
			if (now.last == null)
				dict.remove(now.sym);
			else
				dict.put(now.sym, now.last);
			now = now.pre;
		}
		level--;
	}
	
//	public void push(Symbol sym, Type type, Memory mem){
//		Binder a = dict.get(sym);
//		System.out.println("Push "+ sym.toString());
//		if (a != null){
//			if (a.level == level)
//				throw new Error("conflict definition/declaration");
//		}
//		Binder tmp = new Binder(now, a, level, sym, new VarInfo(type, mem));
//		now = tmp;
//		dict.put(sym, tmp);
//	}
	
//	public void push(Symbol sym, Type type){
//		push(sym, type, new Memory());
//	}
	
//	public void pushPara(Symbol sym, Type type){
//		int tmpLevel = level+1;
//		Binder a = dict.get(sym);
//		System.out.println("Push "+ sym.toString());
//		
//		if (a != null){
//			if (a.level == tmpLevel)
//				throw new Error("conflict definition/declaration");
//		}
//		Binder tmp = new Binder(now, a, tmpLevel, sym, type);
//		now = tmp;
//		dict.put(sym, tmp);
//	}
//	public void pushPara(Symbol sym, Type type){
//		
//	}
	
	public int getLevel(){
		
		return level;
	}
}

