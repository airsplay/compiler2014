package compiler.symbol;

import compiler.symbol.Binder;
import compiler.symbol.Symbol;
import compiler.type.*;
import compiler.main.File;
import compiler.runtime.VarInfo;
//import compiler.type.Field;

class Binder{
	Binder pre;
	Binder last;
	int level;
	Symbol sym;
	Type type;
	VarInfo varInfo;
	
	Binder(Binder a, Binder b, int c, Symbol d, Type e){
		pre = a;
		last = b;
		level = c;
		sym = d;
		type = e;
		varInfo = new VarInfo(type, level);
	}
	
	public VarInfo getVarInfo(){
		return varInfo;
	}
}

public class Environment {
	private java.util.Dictionary<Symbol, Binder> dict = new java.util.Hashtable<Symbol, Binder>();
	private Binder now = null;
	private int level = 0;
	
	public Environment() {
	}
	
	public void upLevel(){
		File.out.println("UpLevel");
		level++;
	}
	
	public Type get(Symbol sym){
		Binder a = dict.get(sym);//if there's no such sym, return null
		if (a == null)
			return null;
		else
			return a.type;
	}
	
	public VarInfo getVarInfo(Symbol sym){
		Binder a = dict.get(sym);
		if (a == null)
			return null;
		else 
			return a.varInfo;
		
	}
	
	public void downLevel(){
		File.out.println("DownLevel");
		while ((now != null) && (now.level == level)){
			if (now.last == null)
				dict.remove(now.sym);
			else
				dict.put(now.sym, now.last);
			now = now.pre;
		}
		level--;
	}
	
	public VarInfo push(Symbol sym, Type type){
		Binder a = dict.get(sym);
		File.out.println("Push "+ sym.toString());
		if (a != null){
			if (a.level == level)
				throw new Error("conflict definition/declaration");
		}
		Binder tmp = new Binder(now, a, level, sym, type);
		now = tmp;
		dict.put(sym, tmp);
		return tmp.getVarInfo();
	}
	
	public VarInfo push(Field field){
		return push(field.name(), field.type());
	}
	
	public VarInfo pushPara(Symbol sym, Type type){
		int tmpLevel = level+1;
		Binder a = dict.get(sym);
		File.out.println("Push "+ sym.toString());
		type.isPara = true;
		if (a != null){
			if (a.level == tmpLevel)
				throw new Error("conflict definition/declaration");
		}
		Binder tmp;
//		if (type instanceof Array){
//			tmp = new Binder(now, a, tmpLevel, sym, new Pointer(type));
//		}
//		else {
			tmp = new Binder(now, a, tmpLevel, sym, type);
//		}
		now = tmp;
		dict.put(sym, tmp);
		
		return tmp.varInfo;
	}
	
	public int getLevel(){
		
		return level;
	}
}
