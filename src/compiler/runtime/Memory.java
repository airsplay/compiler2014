package compiler.runtime;
import compiler.quad.*;

public class Memory implements Operand{
	public int offset;
	public Operand reg = null;
	
	@Override
	public String toString() {
		return offset + "(" + reg +")"; 
	}
	
	public Memory(){
		offset = 0;
	}
	
	public Memory(int a){
		offset = a;
	}
	
//	public Memory(Memory m, int a){
//		offset = m.offset + a;
//	}
	
	public Memory(Operand r, int a){
		reg = r;
		offset = a;
	}
	
	public Memory(Operand r){
		if (!( r instanceof Temp )){
			throw new Error("the Operand give to the Memory isn't Temp");
		}
		offset = 0;
		reg = r;
	}
	public void set(int i, Operand o){
		offset = i;
		reg = o;
	}
	public void add(int i){
		offset += i;
	}
}
