/**
 * 
 */
package compiler.runtime;

import java.util.LinkedList;
import java.util.List;

import compiler.quad.Operand;
import java.util.Iterator;

/**
 * @author tanhao
 *
 */
public final class Register implements Operand {
	static List<Register> availableList = new LinkedList<Register>();
	/**
	 * 
	 */
	static int cnt = 0;
	String tag;
	int label;
	public boolean available;
	public Register(String name, int num) {
		tag = name;
		label = num;
		this.available = true;
		this.availableList.add(this);
	}
	public Register(String name, int num, boolean b) {
		tag = name;
		label = num;
		this.available = b;
		if (this.available) this.availableList.add(this);
	}
	@Override
	public String toString(){
		return "$" + tag;
	}
	static public Iterator<Register> iteratorAvailable(){
		return availableList.iterator();
	}
}
