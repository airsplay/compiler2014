/**
 * 
 */
package compiler.runtime;

/**
 * @author tanhao
 *
 */
public interface RegisterConfig {
	static Register ZERO = new Register("0", 0, false);//forced
	static Register AT = new Register("at", 1, false);//forced
	static Register V0 = new Register("v0", 2, false);//semi-forced
	static Register V1 = new Register("v1", 3, false);//used
	static Register A0 = new Register("a0", 4, false);//semi-forced
	static Register A1 = new Register("a1", 5, false);//used
	static Register A2 = new Register("a2", 6, false);
	static Register A3 = new Register("a3", 7, false);
	static Register T0 = new Register("t0", 8);
	static Register T1 = new Register("t1", 9);
	static Register T2 = new Register("t2", 10);
	static Register T3 = new Register("t3", 11);
	static Register T4 = new Register("t4", 12);
	static Register T5 = new Register("t5", 13);
	static Register T6 = new Register("t6", 14);
	static Register T7 = new Register("t7", 15);
	static Register S0 = new Register("s0", 16);
	static Register S1 = new Register("s1", 17);
	static Register S2 = new Register("s2", 18);
	static Register S3 = new Register("s3", 19);
	static Register S4 = new Register("s4", 20);
	static Register S5 = new Register("s5", 21);
	static Register S6 = new Register("s6", 22);
	static Register S7 = new Register("s7", 23);
	static Register T8 = new Register("t8", 24);
	static Register T9 = new Register("t9", 25);
	static Register K0 = new Register("k0", 26, false);//forced
	static Register K1 = new Register("k1", 27, false);//forced
	static Register GP = new Register("gp", 28, false);//used
	static Register SP = new Register("sp", 29, false);//forced
	static Register S8 = new Register("s8", 30, false);//used
	static Register RA = new Register("ra", 31, false);//forced
	static Register FP = S8;
	static int registerSum = 20;
	static int SIZE_OF_REGISTER = 4;
}
