package compiler.runtime;
import compiler.type.Type;
import compiler.quad.Operand;
import compiler.runtime.Register;

public class VarInfo {
	public Type type;
	public int level;
	public Operand mem;
	public Register register;
	
	
	public VarInfo(Type t){
		type = t;
		mem = new Memory();
	}
	
	public VarInfo(Type t, int l){
		type = t;
		mem = new Memory();
		level = l;
	}
	
	
//	public VarInfo(Type t, Memory m){
//		type = t;
//		mem = m;
//		level = -1;
//	}
	
}
