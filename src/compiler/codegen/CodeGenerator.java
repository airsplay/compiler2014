/*
 * tag:
 *  critical:must change for running, otherwise cause semantic error
 */

package compiler.codegen;
import compiler.ast.*;
import compiler.main.File;
import compiler.quad.*;
import compiler.type.*;
import compiler.symbol.*;
import compiler.runtime.*;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.ListIterator;


public class CodeGenerator implements Intermediate, RegisterConfig, MipsConfig{
//	PrintStream System.out;
	
	public CodeGenerator(PrintStream p){
	
        
        //System.out = p;
		File.out.println("-------------------------CodeGenerator---------------------");
		Temp.sum = Temp.cnt;
//		testSeen();
		
		
		
		allocate_arg_data_memory();
		deleteDuplicate();		//Optimizer;
		printInter();
		
		
		registerAllocate();		//Allocate
		
		function_save_register();	//Allocate
		function_return_memory();	//Allocate
		
		intermediate_expand();		//final Generate
		expand_illegal_instruction();	//展开无法在mips中表达的语义如&&和||
		
		printInter();
		
		File.out.println("----------------------------------------finalCode-------------------------------------");
		set_mips_config();
		print_code_static();
		print_static();
		print_code();
		StringBuffer sb;
		//allocate the Memory;
		//Memory Broadcast;
		//
	}
	private void Loop(){
		ListIterator<Quad> iter = intermediateCode.listIterator(intermediateCode.size());
		while (iter.hasPrevious()){
			Quad q = iter.previous();
			
		}
	}
	private void expand_illegal_instruction(){
		ListIterator<Quad> iter = intermediateCode.listIterator();
		while (iter.hasNext()){
			Quad q = iter.next();
			if (q instanceof Store){
				Store s = (Store) q;
				if (s.arg1 instanceof Immediate){
					int i = ((Immediate)s.arg1).toInt();
					if (i == 0)
						q.arg1 = ZERO;
					else {
						iter.remove();
						Temp t = new Temp();
						iter.add(new Load(S8, s.arg1));//load from Immediate
						iter.add(new Store(S8, s.arg2, s.type));
					}
				}
//				if (s.arg2 instanceof Register) {
//					
//				}
			}
//			else if (q instanceof OpQuad){
//				OpQuad o = (OpQuad) q;
//				if (o.op == "||"){
//					iter.remove();
//					Temp t = new Temp();
//					iter.add(new OpQuad(t, q.arg1, "|", q.arg2));
//					iter.add(new OpQuad(q.result, ZERO, "!=", t));
//				}
//				else if (o.op == "&&"){
//					iter.remove();
			
//					Temp t = new Temp();
//					iter.add(new OpQuad(t, q.arg1, "&", q.arg2));
//					iter.add(new OpQuad(q.result, ZERO, "!=", t));
//				}
//			}
		}
	}
	private void loop1(){
		Iterator< Quad > iter = intermediateCode.iterator();
		while (iter.hasNext()){
			Quad q = iter.next();
		}
	}
	
	private void set_mips_config() {
		
		opMap.put("+", "add");
		opMap.put("-", "sub");
		opMap.put("*", "mul");
		opMap.put("/", "div");
		opMap.put("%", "rem");
		opMap.put("^", "xor");
		opMap.put("<<", "sll");
		opMap.put(">>", "srl");
		
		opMap.put("&&", "and");//critical
		opMap.put("||", "or");//critical
		opMap.put("&", "and");
		opMap.put("|", "or");
		opMap.put("!|", "nor");
		
		opMap.put("==", "seq");
		opMap.put(">=", "sge");
		opMap.put(">", "sgt");
		opMap.put("<=", "sle");
		opMap.put("<", 	"slt");
		opMap.put("!=", "sne");
		
	}
	private void print_code_static(){
		Iterator<String> iter = codeStatic.iterator();
		while (iter.hasNext()){
			write(iter.next());
		}
	}
	private void print_static() {
		write(".data");
		Iterator<String> iter = pseudoStatic.iterator();
		while (iter.hasNext()){
			write(iter.next());
		}
	}
	
	private void write(String str, Quad q){
		String a = "", b = "", c = "";
		if (q.result != null) {
			a = q.result.toString();
		}
		if (q.arg1 != null) 
			b = q.arg1.toString();
		if (q.arg2 != null) {
			if (q.arg2 instanceof Immediate){
				c = "" + ((Immediate)q.arg2).toInt();
			}	
			else c = q.arg2.toString();
		}
		System.out.printf("\t%s %s %s %s\n", str, a,b,c);
	}
	private void write(String str){
		System.out.println( str);
	}
/*	move：
		Register：不同Register之间的传递。
		Immediate：将值送到Register中
	LoadAddr：得到地址。
		Memory：得到Memory的地址
		Label：得到Label的地址	
	Load：得到值
		Memory：直接得到Memory处的值。
		Label：得到Label的值。
		Immediate：将值送到Register中。
		Register：得到Register所指向地址的值*/
		
	private void write_mips(Quad q){
		if (q instanceof Bez){
			if (q.arg1 instanceof Immediate){
				int a = ((Immediate)q.arg1).toInt();
				if (a == 0){
					write("\tj " + q.arg2);
				}
				else {
				}
			}
			else write("beqz", q);
		}
		else if (q instanceof Jal){
			write("jal", q);
		}
		else if (q instanceof Label) {
			write(q.toString() + ":");
		}
		else if (q instanceof Load) {
			Load l = (Load) q;
			if (q.result == null) return;
			if (q.arg1 instanceof Immediate){
				write("\tli " + q.result + " " + ((Immediate)q.arg1).toInt());
//				write("li", q);
			}
			else {
				String instruction;
				if (l.type instanceof Char) instruction = "lb";
				else instruction = "lw";
				if (q.arg1 instanceof Register) {
					write("\t" + instruction +" " + q.result + " " + 0 + "(" + q.arg1 + ")");
				}
				else if ((q.arg1 instanceof Label) || (q.arg1 instanceof Memory)){
					write(instruction, q);
				}
				else throw new Error("illegal Operand in Load instruction with " + q.arg1.toString());
			}
			
		}
		else if (q instanceof Store) {
			Store s = (Store) q;
			String instruction;
			if (q.arg1 == null) return;
			if (s.type instanceof Char) instruction = "sb";
				else instruction = "sw";
			
//			if (s.type instanceof Char){
			if (s.arg2 instanceof Register) {
				write("\t" + instruction + " " + s.arg1 + " " + 0 + "(" + s.arg2 + ")");
			}
			else write(instruction , q);
//			}
//			else {
//				if (s.arg2 instanceof Register) {
//					write("\tsw " + s.arg1 + " " + 0 + "(" + s.arg2 + ")");
//				}
//				else write("sw", q);
//			}
		}
		else if (q instanceof OpQuad) {
//			write()
			if (!opMap.containsKey(((OpQuad)q).op))
				throw new Error("unhandled binary op");
			write(opMap.get(((OpQuad)q).op), q);
		}
		else if (q instanceof LoadAddr) {
			if (q.arg1 instanceof Register){
				write("move", q);
			}
			else if (q.arg1 instanceof Memory) {
				write("\taddu " + q.result + " " +
							((Memory)q.arg1).reg + " " + ((Memory)q.arg1).offset);
			}
			else if (q.arg1 instanceof Label) {
				write("la", q);
			}
			else if (q.arg1 instanceof Immediate){
				write("li", q);
			}
			else throw new Error("illegal Operand in LoadAddr instruction with " + q.arg1.toString());
		}
		else if (q instanceof FunctionLabel) {
			write(".text");
			write((FunctionLabel)q + ":");
		}
		else if (q instanceof Jump){
			write("j", q);
		}
		else if (q instanceof FunctionEnd){
			
		}
		else if (q instanceof FunctionReturn){
			
		}
		else if (q instanceof Syscall){
			write("syscall");
		}
		else if (q instanceof Formalation){
			write(q.toString());
		}
		else if (q instanceof Instruction){
			write(q.toString());
		}
		else if (q instanceof Bne) {
			
			write("\tbne " + q.arg1 + " " + q.arg2 + " " + ((Bne)q).label);
		}
		else if (q instanceof Beq) {
			write("\tbeq " + q.arg1 + " " + q.arg2 + " " + ((Beq)q).label);
		}
		else if (q instanceof ExplicitSet) {
			ExplicitSet l = (ExplicitSet) q;
			
			if (q.arg2 instanceof Immediate){
				write("\tli " + q.arg1 + " " + ((Immediate)q.arg2).toInt());
//				write("li", q);
			}
			else {
				String instruction;
//				if (l.type instanceof Char) instruction = "lb";
//				else 
				
				instruction = "lw";
				if (q.arg2 instanceof Register) {
					write("\t" + instruction +" " + q.result + " " + 0 + "(" + q.arg2 + ")");
				}
				else if ((q.arg2 instanceof Label) || (q.arg2 instanceof Memory)){
					write(instruction, q);
				}
				else throw new Error("illegal Operand in ExplicitSet instruction with " + q.arg2.toString());
			}
		}
		else if (q instanceof Move){
			if (q.arg1 instanceof Register){
				write("move", q);
			}
			else if (q.arg1 instanceof Immediate){
				write("li", q);
			}
			else throw new Error("illegal Operand in move instruction with " + q.arg1.toString());
		}
		else if (q instanceof Memcpy){
//			throw new Error("unhandled memcpy");
		}
		//else throw new Error("Unhandled Quad");
	}
	private void print_code(){
		Iterator< Quad > iter = intermediateCode.iterator();
		while (iter.hasNext()){
			Quad q = iter.next();
			write_mips(q);
		}
	}
	private void add(Quad q){
		intermediateCode.add(q);
//		if (q instanceof Label)
//		File.out.println(q);
//		else 
//			File.out.println("\t"+q);
	}
	
	
	private void intermediate_expand(){
		ListIterator<Quad> iter = intermediateCode.listIterator();
		while (iter.hasNext()){
			Quad q = iter.next();
			if (q instanceof FunctionLabel){
				
				Function funcType = ((FunctionLabel) q).func;
				if (funcType.name == "main"){
					iter.add(new OpQuad(SP, SP, "-", funcType.memSize));
				}
				funcType.memRA = funcType.allocateReg();
				iter.add(new Store(RA, funcType.memRA));
				Iterator<VarInfo> iterLoopReg = funcType.loopRegList.iterator();
				while (iterLoopReg.hasNext()){
					VarInfo v = iterLoopReg.next();
					iter.add(new Load(v.register, v.mem));
				}
				
//				else {
					//store the Register File
//					Iterator<Memory> iterMem = funcType.regMemList.iterator();
//					Iterator<Register> iterReg = funcType.regList.iterator();
//					while (iterReg.hasNext()){
//						Memory mem = iterMem.next();
//						Register reg = iterReg.next();
//						iter.add(new Store(reg, mem));
//					}
//				}
			}
			else if (q instanceof FunctionEnd){
				Function funcType = ((FunctionEnd) q).func;
				//Memory mem = funcType.allocateReg();
				Iterator<VarInfo> iterLoopReg = funcType.loopRegList.iterator();
				while (iterLoopReg.hasNext()){
					VarInfo v = iterLoopReg.next();
					iter.add(new Store(v.register, v.mem));
				}
				iter.add(new Load(RA, funcType.memRA));
//				if (funcType.name != "main"){
					//write back Register
//					Iterator<Memory> iterMem = funcType.regMemList.iterator();
//					Iterator<Register> iterReg = funcType.regList.iterator();
//					while (iterReg.hasNext()){
//						Memory mem = iterMem.next();
//						Register reg = iterReg.next();
//						iter.add(new Load(reg, mem));//Load a Register's value from saved.So can't be Char
//					}
//				}
				
				
				if (funcType.name == "main"){	//add the SP
					iter.add(new OpQuad(SP, SP, "+", funcType.memSize));
				}
				//return 
				iter.add(new Jump(RA));
			}
			else if (q instanceof FunctionReturn){
				Function funcType = ((FunctionReturn) q).func;
				//Memory mem = funcType.allocateReg();
				Iterator<VarInfo> iterLoopReg = funcType.loopRegList.iterator();
				while (iterLoopReg.hasNext()){
					VarInfo v = iterLoopReg.next();
					iter.add(new Store(v.register, v.mem));
				}
				iter.add(new Load(RA, funcType.memRA));
				//Prepare the return address
				if (funcType.returnVarInfo.type instanceof Struct){
					iter.add(new Memcpy(funcType.returnVarInfo.type.size(), q.arg1, funcType.returnVarInfo.mem));//check
				}
				else {
					iter.add(new Store(q.arg1, funcType.returnVarInfo.mem, funcType.returnVarInfo.type));
				}
				if (funcType.name == "main"){	//add the SP
					iter.add(new OpQuad(SP, SP, "+", funcType.memSize));
				}
				iter.add(new Jump(RA));
			}
			else if (q instanceof Jal){
			}
			else if (q instanceof Memcpy){
				//V0 V1 A0
				iter.add(new Formalation("start Memcpy----------------------"));
				Memcpy in = (Memcpy) q;
				iter.add(new LoadAddr(V1, q.arg1));
				iter.add(new OpQuad(V0, V1, "+", new Immediate(in.size)));
				iter.add(new LoadAddr(A0, q.arg2));
				
				
				Label labelEnd = new Label();
				Label labelIter = new Label();

				iter.add(labelIter);
				iter.add(new Beq(V1, V0, labelEnd));
				iter.add(new Load(S8, V1));
				iter.add(new Store(S8, A0));
				iter.add(new OpQuad(V1, V1, "+", new Immediate(4) ));
				iter.add(new OpQuad(A0, A0, "+", new Immediate(4) ));
				
				iter.add(new Jump(labelIter));
				iter.add(labelEnd);
				iter.add(new Formalation("End Memcpy-----------------------"));
			}
		}	
	}
	
	private boolean ignore(Register reg){
		if ((reg.available) || (reg == A2) || (reg == A3))
			return false;
		return true;
	}
	private void function_save_register(){
		Set<Register> set = new HashSet<Register>();
		ListIterator<Quad> iter = intermediateCode.listIterator(intermediateCode.size());
		while (iter.hasPrevious()){
			Quad q = iter.previous();
			if (q instanceof FunctionLabel){
				set.clear();
//				set.add(RA);
			}
			else if (q instanceof FunctionEnd){
				set.clear();
//				set.add(RA);
			}
			else if (q instanceof Jal){
				Function funcType = ((Jal)q).funcType;
				
				Iterator<Memory> iterMem = funcType.regMemList.iterator();
				Iterator<Register> iterReg = set.iterator();
				boolean appended = false;	
				//ListIterator<Quad> tmpIter = iter.;
				int cnt = 0;
				iter.next();
				while (iterReg.hasNext()){
					Register reg = iterReg.next();
					if (ignore(reg)) continue;
					Memory mem;
					if ((!appended) && iterMem.hasNext()){
						mem = iterMem.next();
					}
					else {
						appended = true;
						mem = funcType.allocateReg();
					}
					cnt++;
					iter.add(new Load(reg, mem));//Load saved Register,can't be Char 
				}
				for (int i = 1; i <= cnt; i++) iter.previous();
				iter.previous();
				
				iterMem = funcType.regMemList.iterator();
				iterReg = set.iterator();
				while (iterReg.hasNext()){
					Register reg = iterReg.next();
					if (ignore(reg)) continue;
					Memory mem = iterMem.next();
					iter.add(new Store(reg, mem));
				}
				
				for (int i = 1; i <= cnt; i++) iter.previous();
				
				
			}
			else {
				set_close(set, q.result);
				set_open(set, q.arg1);
				set_open(set, q.arg2);
			}
		}
		
		
//		Set<Register> set = new HashSet<Register>();
//		
//		Iterator< Quad > iter = intermediateCode.iterator();
//		Function nowFunc = null;
//		while (iter.hasNext()){
//			
//			Quad q = iter.next();
//			if (q instanceof FunctionLabel){
//				set.clear();
//				nowFunc = ((FunctionLabel) q).func;
//				nowFunc.addSavedReg(RA);
//			}
//			else if (q instanceof FunctionEnd){
//				nowFunc = null;
//			}
//			else if ((nowFunc != null) && (q.result != null) && !(set.contains((Register) q.result )) && (((Register)q.result).available)){
//				nowFunc.addSavedReg( (Register) q.result );
//				set.add((Register) q.result);
//			}
//		}
	}
	private void set_open(Set<Register> set, Operand a){
		if (a instanceof Register){
			set.add((Register)a);
		}
	}
	private void set_close(Set<Register> set, Operand a){
		if (a instanceof Register){
			set.remove((Register)a);
		}
	}
	
	private void function_return_memory/*and put the function_end_align*/(){
		Iterator< Function > iter = lFunc.iterator();
		while (iter.hasNext()){
			Function func = iter.next( );
			func.allocate_arg(func.returnVarInfo);
			func.align();
		}
		
	}
	
	private void testSeen(){
		int sum = Temp.cnt;
		pr(sum);
		boolean[] seen = new boolean[sum];
		for (int i = 0; i < sum; i++)
			seen[i] = false;
		Iterator<Quad> iter1 = intermediateCode.iterator();
		while (iter1.hasNext()){
			Quad q = iter1.next();
			
			int a = getLabel(q.arg1);
			int b = getLabel(q.arg2);
			int c = getLabel(q.result);
			if (a != -1) seen[a] = true;
			if (b != -1) seen[b] = true;
			if (c != -1) seen[c] = true;
		}
		pr("notSeen:");
		for (int i = 0; i < sum; i++)
			if (!seen[i]) pr(i);
	}
	private void allocate_func_list(Function func, List<VarInfo> lVarInfo){
		Iterator< VarInfo > iter = lVarInfo.iterator();
		while (iter.hasNext()){
			func.allocate(iter.next());
		}
	}
	private void allocate_func_arg_list(Function func, List<VarInfo> lVarInfo){
		Iterator< VarInfo > iter = lVarInfo.iterator();
		while (iter.hasNext()){
			func.allocate_arg(iter.next());
		}
	}
	private void allocate_arg_data_memory(){
		Iterator< Function > iter = lFunc.iterator();
		while (iter.hasNext()){
			Function func = iter.next( );
			allocate_func_arg_list( func, func.argList );
			allocate_func_list( func, func.dataList );
		}
	}
//	private void d
	private void deleteDuplicate() {
		File.out.println("before deleteDuplicate:" +intermediateCode.size());
		pr("the Temp' sum is");
		pr(Temp.sum);
		int sum = Temp.sum;
		boolean[] used = new boolean[sum];
		for (int i = 0; i < sum; i++)
			used[i] = false;
		ListIterator<Quad> iter = intermediateCode.listIterator(intermediateCode.size());
		while (iter.hasPrevious()){
			Quad q = iter.previous();
			int c = getLabel(q.result);
			if (c != -1){
				if (!used[c]) {
					iter.remove();
					sum--;
					continue;
				}
			}
			int a = getLabel(q.arg1);
			int b = getLabel(q.arg2);
			if (a != -1){
				used[a] = true;
			}
			if (b != -1){
				used[b] = true;
			}
		}
		pr("after deleteDuplicate:"+ intermediateCode.size());
//		Temp.sum = sum;
		pr("the Temp' sum is");
		pr(Temp.sum);
	}
	static private void printInter(){
		Iterator<Quad> iter1 = intermediateCode.iterator();
		while (iter1.hasNext()){
			Quad q = iter1.next();
			if (!((q instanceof Label) || (q instanceof FunctionLabel)))
				File.out.print("\t");
			File.out.println(q);
		}
	}
	private int getLabel(Object o){
		if (o instanceof Temp){
			if (((Temp)o).reserved) return -1;
			return ((Temp) o).label;
		}
		return -1;
	}
	private int tryAllocate(Quad q, boolean[] allocated){
		int a = getLabel(q.arg1);
		int b = getLabel(q.arg2);
		if ((a != -1) && (!allocated[a]) && (lastUsed[a] == q)) return a;
		if ((b != -1) && (!allocated[b]) && (lastUsed[b] == q)) return b;
		return -1;
	}
	private boolean endAllocate(Quad q, int x) {
		int a = getLabel(q.result);
		if ((a != -1) && (a == x)) return true;
		return false;
	}

	Quad[] lastUsed = new Quad[Temp.cnt];
	
	private void registerAllocate(){
		int sum = Temp.cnt;
		
		for (int i = 0; i < sum; i++) lastUsed[i] = null;
		Iterator<Quad> iter1 = intermediateCode.iterator();
		while (iter1.hasNext()){
			Quad q = iter1.next();
			int a = getLabel(q.arg1);
			int b = getLabel(q.arg2);
			if ((a != -1)) lastUsed[a] = q;
			if ((b != -1)) lastUsed[b] = q;
		}
		
		boolean[] allocated = new boolean[sum];
		Register[] map = new Register[sum];
		for (int i = 0; i < sum; i++)
			map[i] = null;
		for (int i = 0; i < sum; i++)
			allocated[i] = false;
		int nowReg = -1;
		int optiReg = 0;
		
		Iterator<Register> iterReg = Register.iteratorAvailable();
		while (iterReg.hasNext()){
			Register realReg = iterReg.next();
			ListIterator<Quad> iter = intermediateCode.listIterator(intermediateCode.size());
			while (iter.hasPrevious()){
				Quad q = iter.previous();
//				if (q instanceof Ex
				if (nowReg == -1) nowReg = tryAllocate(q, allocated);
				else if (endAllocate(q, nowReg)){
					map[nowReg] = realReg;
					allocated[nowReg] = true;
					optiReg++;
//					pr(nowReg);
					nowReg = -1;
				}
			}
			if (optiReg == Temp.sum)
				break;
		}
		
		
		Iterator<Quad> iter = this.intermediateCode.iterator();
		while (iter.hasNext()){
			Quad q = iter.next();
			if (q.arg1 instanceof Temp){
				File.out.println(q.arg1.toString());
				q.arg1 = map[getLabel(q.arg1)];
				if (q.arg1 == null) throw new Error("Can't allocate all in Register");
			}
			if (q.arg2 instanceof Temp){
				File.out.println(q.arg2.toString());
				q.arg2 = map[getLabel(q.arg2)];
				if (q.arg2 == null) throw new Error("Can't allocate all in Register");
			}
			if (q.result instanceof Temp){
				File.out.println(q.result.toString());
				q.result = map[getLabel(q.result)];
				if (q.result == null) throw new Error("Can't allocate all in Register");
			}
		}
		
		pr("the Temp sum :" + Temp.sum);
		pr("the Temp allocated :" + optiReg);
	}
	static private void pr(int x){
		File.out.println(x);
	}
	static private void pr(String s){
		File.out.println(s);
	}
}
