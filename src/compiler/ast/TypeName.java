package compiler.ast;

public class TypeName{

	public TypeSpecifier typeSpec;
	public Stars stars;

	public TypeName(TypeSpecifier argtypeSpec, Stars argstars){
		typeSpec = argtypeSpec;
		stars = argstars;
	}

	public TypeName(){
		typeSpec = null;
		stars = null;
	}

}