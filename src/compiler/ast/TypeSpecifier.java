package compiler.ast;
import compiler.type.Type;
public class TypeSpecifier{

	public TypeSpecifier.OpType op;
	public compiler.symbol.Symbol sym;
	public Members memList;
//	public Type type;
	
	public TypeSpecifier(TypeSpecifier.OpType argop, compiler.symbol.Symbol argsym, Members argmemList){
		op = argop;
		sym = argsym;
		memList = argmemList;
	}

	public TypeSpecifier(){
		op = null;
		sym = null;
		memList = null;
	}

	public static enum OpType {
		VOID, CHAR, INT, STRUCT, UNION, ENDING
	}
}