package compiler.ast;

public class CharConstant extends Constant{

	public char ch;

	public CharConstant(char argch){
		ch = argch;
	}

	public CharConstant(){
		ch = ' ';
	}

}