package compiler.ast;

public class FunctionDeclarator extends Declarator{

	public PlainDeclarator plainDor;
	public Parameters paraList;

	public FunctionDeclarator(PlainDeclarator argplainDor, Parameters argparaList){
		plainDor = argplainDor;
		paraList = argparaList;
	}

	public FunctionDeclarator(){
		plainDor = null;
		paraList = null;
	}

}