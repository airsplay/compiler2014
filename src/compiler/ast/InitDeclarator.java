package compiler.ast;
import compiler.type.Type;
import compiler.symbol.Symbol;
import compiler.runtime.VarInfo;

public class InitDeclarator{

	public Declarator dor;
	public Initializer init;
	public Type type;
	public Symbol sym;
	public VarInfo varInfo;
	
	public InitDeclarator(Declarator argdor, Initializer arginit){
		dor = argdor;
		init = arginit;
	}

	public InitDeclarator(){
		dor = null;
		init = null;
	}

}