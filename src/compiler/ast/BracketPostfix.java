package compiler.ast;

public class BracketPostfix extends Postfix{

	public Expression exp;

	public BracketPostfix(Expression argexp){
		exp = argexp;
	}

	public BracketPostfix(){
		exp = null;
	}

}