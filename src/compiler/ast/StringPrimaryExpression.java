package compiler.ast;

public class StringPrimaryExpression extends PrimaryExpression{

	public String str;

	public StringPrimaryExpression(String argstr){
		str = argstr;
	}

	public StringPrimaryExpression(){
		str = null;
	}

}