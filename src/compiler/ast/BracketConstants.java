package compiler.ast;

public class BracketConstants{

	public Expression consExp;
	public BracketConstants bracConList;

	public BracketConstants(Expression argconsExp, BracketConstants argbracConList){
		consExp = argconsExp;
		bracConList = argbracConList;
	}

	public BracketConstants(){
		consExp = null;
		bracConList = null;
	}

}