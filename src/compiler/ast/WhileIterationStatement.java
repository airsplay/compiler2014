package compiler.ast;

public class WhileIterationStatement extends IterationStatement{

	public Expression exp;
	public Statement stat;

	public WhileIterationStatement(Expression argexp, Statement argstat){
		exp = argexp;
		stat = argstat;
	}

	public WhileIterationStatement(){
		exp = null;
		stat = null;
	}

}