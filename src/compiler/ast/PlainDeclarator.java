package compiler.ast;

public class PlainDeclarator{

	public Stars stars;
	public compiler.symbol.Symbol sym;

	public PlainDeclarator(Stars argstars, compiler.symbol.Symbol argsym){
		stars = argstars;
		sym = argsym;
	}

	public PlainDeclarator(){
		stars = null;
		sym = null;
	}

}