package compiler.ast;

public class BinaryExpression extends Expression{

	public Expression lOprand;
	public BinaryExpression.OpType op;
	public Expression rOprand;

	public BinaryExpression(Expression arglOprand, BinaryExpression.OpType argop, Expression argrOprand){
		lOprand = arglOprand;
		op = argop;
		rOprand = argrOprand;
	}

	public BinaryExpression(){
		lOprand = null;
		op = null;
		rOprand = null;
	}

	public static enum OpType {
		COMMA, ASSIGN, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, ADD_ASSIGN, SUB_ASSIGN, SHL_ASSIGN, SHR_ASSIGN, AND_ASSIGN, XOR_ASSIGN, OR_ASSIGN, EQ, NE, LT, GT, LE, GE, SHL, SHR, PLUS, MINUS, LOGOR, LOGAND, XOR, AND, OR, MUL, DIV, MOD, ENDING
	}
}