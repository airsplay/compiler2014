package compiler.ast;

public class FunctionProgram extends Program{

	public FunctionDefinition func;
	public Program prog;

	public FunctionProgram(FunctionDefinition argfunc, Program argprog){
		func = argfunc;
		prog = argprog;
	}

	public FunctionProgram(){
		func = null;
		prog = null;
	}

}