package compiler.ast;

public class ConstantExpression extends Expression {

	public Expression exp;

	public ConstantExpression(Expression argexp){
		exp = argexp;
	}

	public ConstantExpression(){
		exp = null;
	}

}