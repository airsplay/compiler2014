package compiler.ast;

public class Declarations{

	public Declaration don;
	public Declarations donList;

	public Declarations(Declaration argdon, Declarations argdonList){
		don = argdon;
		donList = argdonList;
	}

	public Declarations(){
		don = null;
		donList = null;
	}

}