package compiler.ast;

public class PtrPostfix extends Postfix{

	public compiler.symbol.Symbol sym;

	public PtrPostfix(compiler.symbol.Symbol argsym){
		sym = argsym;
	}

	public PtrPostfix(){
		sym = null;
	}

}