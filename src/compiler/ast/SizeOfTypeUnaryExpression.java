package compiler.ast;

public class SizeOfTypeUnaryExpression extends UnaryExpression{

	public TypeName typeName;

	public SizeOfTypeUnaryExpression(TypeName argtypeName){
		typeName = argtypeName;
	}

	public SizeOfTypeUnaryExpression(){
		typeName = null;
	}

}