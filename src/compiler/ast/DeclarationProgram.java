package compiler.ast;

public class DeclarationProgram extends Program{

	public Declaration decl;
	public Program prog;

	public DeclarationProgram(Declaration argdecl, Program argprog){
		decl = argdecl;
		prog = argprog;
	}

	public DeclarationProgram(){
		decl = null;
		prog = null;
	}

}