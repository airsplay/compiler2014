package compiler.ast;

public class DotPostfix extends Postfix{

	public compiler.symbol.Symbol sym;

	public DotPostfix(compiler.symbol.Symbol argsym){
		sym = argsym;
	}

	public DotPostfix(){
		sym = null;
	}

}