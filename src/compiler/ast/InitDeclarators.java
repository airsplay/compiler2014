package compiler.ast;

public class InitDeclarators{

	public InitDeclarator initDor;
	public InitDeclarators initDors;

	public InitDeclarators(InitDeclarator arginitDor, InitDeclarators arginitDors){
		initDor = arginitDor;
		initDors = arginitDors;
	}

	public InitDeclarators(){
		initDor = null;
		initDors = null;
	}

}