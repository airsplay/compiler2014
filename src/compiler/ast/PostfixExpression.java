package compiler.ast;

public class PostfixExpression extends UnaryExpression{

	public Expression priExp;
	public Postfixs postfixs;

	public PostfixExpression(Expression argpriExp, Postfixs argpostfixs){
		priExp = argpriExp;
		postfixs = argpostfixs;
	}

	public PostfixExpression(){
		priExp = null;
		postfixs = null;
	}

}