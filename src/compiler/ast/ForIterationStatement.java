package compiler.ast;
import compiler.runtime.Register;
public class ForIterationStatement extends IterationStatement{

	public Expression exp1;
	public Expression exp2;
	public Expression exp3;
	public Statement stat;
	public Register register;

	public ForIterationStatement(Expression argexp1, Expression argexp2, Expression argexp3, Statement argstat){
		exp1 = argexp1;
		exp2 = argexp2;
		exp3 = argexp3;
		stat = argstat;
	}

	public ForIterationStatement(){
		exp1 = null;
		exp2 = null;
		exp3 = null;
		stat = null;
	}

}