package compiler.ast;

public class ParenPostfix extends Postfix{

	public Arguments args;

	public ParenPostfix(Arguments argargs){
		args = argargs;
	}

	public ParenPostfix(){
		args = null;
	}

}