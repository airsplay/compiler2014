package compiler.ast;

public class NumConstant extends Constant{

	public int value;

	public NumConstant(int argvalue){
		value = argvalue;
	}

	public NumConstant(){
		value = 0;
	}

}