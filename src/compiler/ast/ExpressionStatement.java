package compiler.ast;

public class ExpressionStatement extends Statement{

	public Expression exp;

	public ExpressionStatement(Expression argexp){
		exp = argexp;
	}

	public ExpressionStatement(){
		exp = null;
	}

}