package compiler.ast;

public class Initializers extends Initializer{

	public Initializer init;
	public Initializers initList;

	public Initializers(Initializer arginit, Initializers arginitList){
		init = arginit;
		initList = arginitList;
	}

	public Initializers(){
		init = null;
		initList = null;
	}

}