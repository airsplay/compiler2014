package compiler.ast;

public class CompoundStatement extends Statement{

	public Declarations donList;
	public Statements statList;

	public CompoundStatement(Declarations argdonList, Statements argstatList){
		donList = argdonList;
		statList = argstatList;
	}

	public CompoundStatement(){
		donList = null;
		statList = null;
	}

}