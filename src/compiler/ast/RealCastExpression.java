package compiler.ast;

public class RealCastExpression extends CastExpression{

	public TypeName typeName;
	public Expression castExp;

	public RealCastExpression(TypeName argtypeName, Expression argcastExp){
		typeName = argtypeName;
		castExp = argcastExp;
	}

	public RealCastExpression(){
		typeName = null;
		castExp = null;
	}

}