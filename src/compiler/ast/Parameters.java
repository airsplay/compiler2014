package compiler.ast;

public class Parameters{

	public PlainDeclaration plainDon;
	public Parameters paraList;

	public Parameters(PlainDeclaration argplainDon, Parameters argparaList){
		plainDon = argplainDon;
		paraList = argparaList;
	}

	public Parameters(){
		plainDon = null;
		paraList = null;
	}

}