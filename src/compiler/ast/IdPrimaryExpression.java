package compiler.ast;

import compiler.runtime.VarInfo;
import compiler.runtime.Register;
public class IdPrimaryExpression extends PrimaryExpression{

	public compiler.symbol.Symbol sym;
	public VarInfo varInfo;
	public Register register;

	public IdPrimaryExpression(compiler.symbol.Symbol argsym){
		sym = argsym;
	}

	public IdPrimaryExpression(){
		sym = null;
	}

}