package compiler.ast;
import compiler.quad.*;
import compiler.type.Type;
public class Arguments{

	public Expression binExp;
	public Arguments args;
	public Operand rand;
	public Type type;
	
	public Arguments(Expression argbinExp, Arguments argargs){
		binExp = argbinExp;
		args = argargs;
	}

	public Arguments(){
		binExp = null;
		args = null;
	}

}