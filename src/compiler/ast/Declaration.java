package compiler.ast;

public class Declaration{

	public TypeSpecifier typeSpec;
	public InitDeclarators initDors;

	public Declaration(TypeSpecifier argtypeSpec, InitDeclarators arginitDors){
		typeSpec = argtypeSpec;
		initDors = arginitDors;
	}

	public Declaration(){
		typeSpec = null;
		initDors = null;
	}

}