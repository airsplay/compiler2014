package compiler.ast;

public class ConstantPrimaryExpression extends PrimaryExpression{

	public Constant cons;

	public ConstantPrimaryExpression(Constant argcons){
		cons = argcons;
	}

	public ConstantPrimaryExpression(){
		cons = null;
	}

}