package compiler.ast;
import compiler.type.Type;
import compiler.quad.Label;
import compiler.quad.Operand;
public class Expression{
	boolean lValue = false;
	public Type type = null;
	public Label label = null;
	public Operand rand;
	
	public Expression(){
	}
	
	public void setType(Type obj){
		type = obj;
	}
//	public void setFinalType(Type obj){
//		finalType = obj;
//	}
	
	public boolean lValue(){
		return lValue;
	}
	
	public void allowlValue(){
		lValue = true;
	}
}