package compiler.ast;

public class PlainDeclaration{

	public TypeSpecifier typeSpec;
	public Declarator dor;

	public PlainDeclaration(TypeSpecifier argtypeSpec, Declarator argdor){
		typeSpec = argtypeSpec;
		dor = argdor;
	}

	public PlainDeclaration(){
		typeSpec = null;
		dor = null;
	}

}