package compiler.ast;

public class UnaryExpression extends CastExpression{

	public UnaryExpression.OpType op;
	public Expression exp;

	public UnaryExpression(UnaryExpression.OpType argop, Expression argexp){
		op = argop;
		exp = argexp;
	}

	public UnaryExpression(){
		op = null;
		exp = null;
	}

	public static enum OpType {
		AND, STAR, PLUS, MINUS, NOT, LOGNOT, INC, SIZEOF, DEC, ENDING
	}
}