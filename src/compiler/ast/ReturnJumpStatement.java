package compiler.ast;

public class ReturnJumpStatement extends JumpStatement{

	public Expression exp;

	public ReturnJumpStatement(Expression argexp){
		exp = argexp;
	}

	public ReturnJumpStatement(){
		exp = null;
	}

}