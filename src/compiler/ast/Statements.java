package compiler.ast;

public class Statements{

	public Statement stat;
	public Statements statList;

	public Statements(Statement argstat, Statements argstatList){
		stat = argstat;
		statList = argstatList;
	}

	public Statements(){
		stat = null;
		statList = null;
	}

}