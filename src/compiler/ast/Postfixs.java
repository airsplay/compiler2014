package compiler.ast;
import compiler.quad.Operand;
public class Postfixs extends Expression {
	public Operand initRand;
//	boolean lValue = false;
	
	public boolean lValue(){
		return lValue;
	}
	
	public void allowlValue(){
		lValue = true;
	}
	
	public Postfix postfix;
	public Postfixs postfixs;

	public Postfixs(Postfix argpostfix, Postfixs argpostfixs){
		postfix = argpostfix;
		postfixs = argpostfixs;
	}

	public Postfixs(){
		postfix = null;
		postfixs = null;
	}

}