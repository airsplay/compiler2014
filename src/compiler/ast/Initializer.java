package compiler.ast;
import compiler.type.Type;
public class Initializer {
	public Type type;
	public Expression exp;

	public Initializer(Expression argexp){
		exp = argexp;
	}

	public Initializer(){
		exp = null;
	}

}