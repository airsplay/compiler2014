package compiler.ast;

public class DataDeclarator extends Declarator{

	public PlainDeclarator plainDor;
	public BracketConstants bracConList;

	public DataDeclarator(PlainDeclarator argplainDor, BracketConstants argbracConList){
		plainDor = argplainDor;
		bracConList = argbracConList;
	}

	public DataDeclarator(){
		plainDor = null;
		bracConList = null;
	}

}