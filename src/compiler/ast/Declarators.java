package compiler.ast;

public class Declarators{

	public Declarator dor;
	public Declarators dorList;

	public Declarators(Declarator argdor, Declarators argdorList){
		dor = argdor;
		dorList = argdorList;
	}

	public Declarators(){
		dor = null;
		dorList = null;
	}

}