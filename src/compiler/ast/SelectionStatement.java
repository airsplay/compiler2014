package compiler.ast;

public class SelectionStatement extends Statement{

	public Expression exp;
	public Statement ifStat;
	public Statement elseStat;

	public SelectionStatement(Expression argexp, Statement argifStat, Statement argelseStat){
		exp = argexp;
		ifStat = argifStat;
		elseStat = argelseStat;
	}

	public SelectionStatement(){
		exp = null;
		ifStat = null;
		elseStat = null;
	}

}