package compiler.ast;

public class Members{

	public TypeSpecifier typeSpec;
	public Declarators dorList;
	public Members memList;

	public Members(TypeSpecifier argtypeSpec, Declarators argdorList, Members argmemList){
		typeSpec = argtypeSpec;
		dorList = argdorList;
		memList = argmemList;
	}

	public Members(){
		typeSpec = null;
		dorList = null;
		memList = null;
	}

}