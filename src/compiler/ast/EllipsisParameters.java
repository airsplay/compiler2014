package compiler.ast;

public class EllipsisParameters extends Parameters{

	public PlainDeclaration plainDon;
	public Parameters paraList;

	public EllipsisParameters(PlainDeclaration argplainDon, Parameters argparaList){
		plainDon = argplainDon;
		paraList = argparaList;
	}

	public EllipsisParameters(){
		plainDon = null;
		paraList = null;
	}

}