package compiler.ast;

import compiler.type.Type;
public class FunctionDefinition{

	public TypeSpecifier typeSpec;
	public PlainDeclarator plainDor;
	public Parameters paraList;
	public CompoundStatement compStmt;
	public Type type;

	public FunctionDefinition(TypeSpecifier argtypeSpec, PlainDeclarator argplainDor, Parameters argparaList, CompoundStatement argcompStmt){
		typeSpec = argtypeSpec;
		plainDor = argplainDor;
		paraList = argparaList;
		compStmt = argcompStmt;
	}

	public FunctionDefinition(){
		typeSpec = null;
		plainDor = null;
		paraList = null;
		compStmt = null;
	}

}