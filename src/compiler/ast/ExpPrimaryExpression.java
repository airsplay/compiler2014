package compiler.ast;

public class ExpPrimaryExpression extends PrimaryExpression{

	public Expression exp;

	public ExpPrimaryExpression(Expression argexp){
		exp = argexp;
	}

	public ExpPrimaryExpression(){
		exp = null;
	}

}