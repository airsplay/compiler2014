package compiler.quad;


public class Memcpy extends Quad {
	public int size;
	public Memcpy(int s, Operand a, Operand b){
		super(a,b);
		this.size = s;
	}
	
	@Override
	public String toString() {
		return "Memcpy " + super.toString() + size;
	}
	
}
