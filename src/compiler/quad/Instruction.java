package compiler.quad;

public class Instruction extends Quad {
	String str;
	public Instruction(String s){
		str = s;
	}
	
	@Override
	public String toString(){
		return str + "\n";
	}
}
