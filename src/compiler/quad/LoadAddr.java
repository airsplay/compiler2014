package compiler.quad;

public class LoadAddr extends Quad {
	//if arg1 is Temp, simplely move it; else calculate and store
	@Override
	public String toString() {
		return "LoadA " + super.result + " " + super.arg1;
	}
	
	public LoadAddr(Operand a, Operand b){
		super(a, b, null);
	}
}
