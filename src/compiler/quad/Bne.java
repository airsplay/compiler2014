package compiler.quad;

public class Bne extends Quad {
	public Label label;
	
	public Bne(Operand a, Operand b, Label l) {
		super(a, b);
		label = l;
	}

	@Override
	public String toString(){
		return "Bne " + arg1 + " " + arg2 + " " + label;
	}

}
