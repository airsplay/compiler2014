package compiler.quad;

public class Jump extends Quad {
	public Jump(Operand lab){
		super(null, lab);
	}
	
	public String toString(){
		return "Jump "+super.toString();
	}
}
