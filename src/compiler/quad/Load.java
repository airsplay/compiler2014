package compiler.quad;
import compiler.type.Type;
import compiler.type.Int;
public class Load extends Quad {
	public Type type;
	@Override
	public String toString() {
		return "Load " + super.toString();
	}

	public Load(Operand a, Operand b) {
		super(a, b, null);
		type = new Int();
	}
	
	public Load(Operand a, Operand b, Type t){
		super(a, b, null);
		type = t;
	}
}
