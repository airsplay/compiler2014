package compiler.quad;

import java.util.List;
import java.util.LinkedList;

import compiler.type.*;
public interface Intermediate {
	List<String> pseudoStatic = new LinkedList<String>();
	List<String> codeStatic = new LinkedList<String>();
	List<Quad> intermediateCode = new LinkedList<Quad>();
	List<Function> lFunc = new LinkedList<Function>();
	
//	public static Temp SP = new Temp(true);
//	public static Temp FP = new Temp(true);
//	public static Temp ZERO = new Temp(true);
//	public static Temp Num31 = new Temp(true);
	
	final public static int realRegisterSum = 20;
}
