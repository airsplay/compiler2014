package compiler.quad;

public class Bez extends Quad {
	public Bez(Operand a, Operand lab){
		super(a, lab);
	}
	public String toString(){
		return ("Bez "+super.toString());
	}
}
