package compiler.quad;

public final class Label extends Quad implements Operand{
	static int cnt = 0;
//	int i;
	String str;
	public Label(){
		str = "$$L" + cnt ;
		cnt++;
	}
	public Label(String s){
		this.str = s ;
	}
	public String toString() {
		return (str);
				
	}
}
