package compiler.quad;
import compiler.type.Function;
public class FunctionLabel extends Quad implements Operand{
	public Function func;
	public FunctionLabel(Function f) {
		super(null, null, null);
		func = f;
		
	}
	
	@Override
	public String toString() {
		if (func.name == "main")
			return func.name;
		else return "$$" + func.name;	
	}
}
