package compiler.quad;

public class Move extends Quad {
	public Move (Operand a, Operand b){
		super(a, b, null);
	}
	
	@Override
	public String toString(){
		return "Move " + super.toString();
	}
}
