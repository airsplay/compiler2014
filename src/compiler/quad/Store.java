package compiler.quad;
import compiler.type.Type;
import compiler.type.Int;
public class Store extends Quad {
	public Type type;
	@Override
	public String toString() {
		return "Store " + super.toString();
	}
	
	public Store(Operand a, Operand b){
		super(a, b);
		type = new Int();
	}
	
	public Store(Operand a, Operand b, Type t){
		super(a, b);
		type = t;
	}
}
