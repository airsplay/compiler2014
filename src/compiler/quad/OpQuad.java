package compiler.quad;

public class OpQuad extends Quad{
	public String op;
	public OpQuad(Operand res, Operand a, String sOp, Operand b){
		super(res,a,b);
		this.op = sOp;
	}
	
	@Override
	public String toString() {
		return "Op " + result + " " + arg1 + " " + op + " " + arg2;
	}
}
