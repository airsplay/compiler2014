package compiler.quad;
import compiler.quad.Operand;

public class Quad {
	public Operand result;
	public Operand arg1,arg2;
	
	public Quad(){
		result = arg1 = arg2 = null;
	}
	public Quad(Operand res, Operand a1, Operand a2){
		result = res;
		arg1 = a1;
		arg2 = a2;
	}
	public Quad(Operand a1, Operand a2){
		result = null;
		arg1 = a1;
		arg2 = a2;
	}
	public String toString(){
		String tmp = new String();
		if (result != null){
			tmp += (result.toString()+" ");
		}
		if (arg1 != null){
			tmp += (arg1.toString()+" ");
		}
		if (arg2 != null){
			tmp += (arg2.toString()+" ");
		}
		return tmp;
	}
}
