package compiler.quad;
import compiler.type.Function;
public class FunctionEnd extends Quad {
	public Function func;
	public FunctionEnd(Function f){

		super(null, null, null);
		func = f;
	}
	
	@Override
	public String toString(){
		return "End of " + func.name;
	}
}
