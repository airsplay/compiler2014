package compiler.quad;

public class Beq extends Quad {
	public Label label;
	

	public Beq(Operand a, Operand b, Label l) {
		super(a, b);
		label = l;
	}

	@Override
	public String toString(){
		return "Beq " + arg1 + " " + arg2 + " " + label;
	}

}
