package compiler.quad;
import compiler.type.Function;

public class Jal extends Quad {
	public Function funcType;
	public Jal(FunctionLabel label, Function func){
		super(label, null);
		funcType = func;
	}
	
	@Override
	public String toString(){
		return "Jal " + super.toString();
	}
}
