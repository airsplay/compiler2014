package compiler.quad;
import compiler.type.Function;
public class FunctionReturn extends Quad {
	public Function func;
	public FunctionReturn(Function f, Operand o){
		super(o, null);
		func = f;
	}
	
	@Override
	public String toString() {
		return "Return " + func.name + " " + arg1;
	}
}
