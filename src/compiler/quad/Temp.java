package compiler.quad;

import compiler.runtime.Memory;
import compiler.runtime.Register;

public class Temp implements Operand{
	public static int sum = 0;
	
	public static int cnt = 0;
	public int label;
	public Operand mem;
	public boolean reserved = false;
	public String tag = null;
	public Register real;
	
	public Temp(){
		label = (cnt ++);
	}
	
	public Temp(boolean b){
		label = (cnt ++);
		reserved = b;
	}
	
	@Override
	public boolean equals(Object res){
		if (res == null) return false;
		if (! (res instanceof Temp)) return false;
		if (label == ((Temp)res).label) return true;
		else return false;
	}
	
	public void setMemory(Operand op){
		if (op instanceof Temp){
			mem = new Memory(op);
		}
		mem = op;
	}
	
	@Override
	public String toString() {
		return "R" + label; 
	}
}
