package compiler.quad;

public class Formalation extends Quad {
	String str;
	public Formalation () {
		str = "";
	}
	public Formalation (String s){
		str = s;
	}
	@Override
	public String toString(){
		return "#" + str;
	}
}
