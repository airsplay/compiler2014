package compiler.quad;

public class ExplicitSet extends Quad {
	
	public ExplicitSet(Operand a, Operand b){
		super(a, b);
	}
	
	@Override
	public String toString(){
		return "Set " + super.toString();
	}
}
