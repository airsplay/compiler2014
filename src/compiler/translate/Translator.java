/*Don't push the static argument in Loop
 * don't lw without arg1 
* tags:
 * todo
 * check
 * mem : serve for the "offset(register)" memory style
 * mention : Don't need to be changed now. But if you want to modify the structure. mention this
 * codeGen : The Question remained to be solved when generate the MIPS CODE
 * caution : It has very low possibility to be wrong 
 * delete  : the code can deleted after the test of correctness
 * critical: the code must be changed
 */
package compiler.translate;
import compiler.ast.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;
import compiler.symbol.*;
import compiler.main.File;
import compiler.quad.*;
import compiler.runtime.*;
import compiler.type.*;
import java.util.Set;
public class Translator implements Intermediate, RegisterConfig{
//	Table table = new Table();
//	Temp sp;
//	Temp fp;
//	Temp ZERO;
	Set<String> a = new HashSet<String>();
	Function nowFunc;//todo Add Data to Function
	Label	printfSpaceLabel;
	Label	printfTempLabel;
	compiler.ast.ForIterationStatement nowFor = null;
//	List<Quad> intermediateCode = new LinkedList<Quad>();
	public Translator(Program prog){
		
		PRINT("-----------------------------Translate---------------------");
//		ZERO = new Temp();
//		sp = new Temp();
//		fp = new Temp();
		
		printfSpaceLabel = new Label();
//		pseudoStatic.add(".data");
		pseudoStatic.add(".align 2");
		pseudoStatic.add(printfSpaceLabel	+	":");
		pseudoStatic.add(".space 1024");
		printfTempLabel = new Label();
		pseudoStatic.add(".align 2");
		pseudoStatic.add(printfTempLabel  + ":");
		pseudoStatic.add(".space 4");
		
		translate(prog);
		/*PrintfRegisterConfig:
		 * 3:used to find the actual field
		 * 5;used for temporory
		 * 30:the pointer which point to the String
		 * 2/4:the Syscall Register. & used to temproy
		 */
		
		String code = 
				".text\n"+
				"$printf:\n"+
				"move	$4	$30\n" +
				"$label_printf_Iter:\n" +
				"	lb	$2	0($30)\n" +
				"	ori	$5	$0	37\n"+
				"	beq	$5	$2	$label_printf_special_out\n"+
				"	bne	$0	$2	$label_printf_scan_next\n"+
				"		ori	$2	$0	4\n"+
				"		syscall\n"+
				"		j $label_printf_end\n"+
				"$label_printf_scan_next:\n"+
				"	add	$30	$30	1\n"+
				"	j $label_printf_Iter\n"+
				"$label_printf_end:\n"+
				"j	$31";
		String code1 = 
				"$label_printf_special_out:\n"+
				"	beq	$30	$4	$label_printf_check_char\n"+
				"	lb	$5	0($30)\n"+
					
				"	sb	$0	0($30)\n"+
				"	ori	$2	$0	4\n"+
				"	syscall\n"+
					
				"	sb	$5	0($30)\n"+
					
				"$label_printf_check_char:\n"+
				"	lb	$4	1($30)\n"+
				"	$label_printf_d:	\n"+
				"	ori	$2	$0	'd'\n"+
				"		bne	$2	$4	$label_printf_c\n"+
				"			lw	$4	0($3)\n"+
				"			ori	$2	$0	1\n"+
				"			syscall\n"+
				"			add $3	$3	4\n"+
				"			add	$30	$30	2\n"+
				"			move	$4	$30\n"+
				"			j	$label_printf_Iter\n"+
				"	$label_printf_c:\n"+
				"	ori	$2	$0	'c'\n"+
				"		bne	$2	$4	$label_printf_s\n"+
				"			la	$4	" +printfTempLabel + "\n"+
				"			lw	$2	0($3)\n"+
				"			sb	$2	0($4)\n"+
				"			ori	$2	$0	4\n"+
				"			syscall\n"+
				"			add	$4	$30	2\n"+
				"			add	$30	$30	2\n"+
				"			add $3	$3	4\n"+
				"			j	$label_printf_Iter\n"+
				"	$label_printf_s:	\n"+
				"	ori	$2	$0	's'\n"+
				"		bne	$2	$4	$label_printf_04d\n"+
				"			lw	$4	0($3)\n"+
				"			ori	$2	$0	4\n"+
				"			syscall\n"+
				"			add $3	$3	4\n"+
				"			add	$4	$30	2\n"+
				"			add	$30	$30	2\n"+
				"			move	$4	$30\n"+
				"			j	$label_printf_Iter\n"+
				"	$label_printf_04d:	\n"+
				"		ori	$2	$0	1	\n"+
					
				"		lw	$4	0($3)\n"+
				"		bge $4 	1000 $$$label_printf_num\n"+
				"		li	$4	0\n"+
				"		syscall\n"+
					
				"		lw	$4	0($3)\n"+
				"		bge $4 	100 $$$label_printf_num\n"+
				"		li	$4	0\n"+
				"		syscall\n"+
					
				"		lw	$4	0($3)\n"+
				"		bge $4 	10 $$$label_printf_num\n"+
				"		li	$4	0\n"+
				"		syscall\n"+
					
				"		lw	$4	0($3)\n"+
				"		bge $4 	1 $$$label_printf_num\n"+
				"		li	$4	0\n"+
				"		syscall\n"+
					
				"		$$$label_printf_num:\n"+
				"		lw	$4	0($3)\n"+
				"		syscall\n"+
							
						
				"			add $3	$3	4\n"+
				"			add	$30	$30	4\n"+
				"			move	$4	$30\n"+
				"			j	$label_printf_Iter\n";
		codeStatic.add(code);
		codeStatic.add(code1);
//		pseudoStatic.add(".data");
//		PRINT(new Integer(lQuad.size()));
//		Iterator<Quad> iter = lQuad.iterator();
//		while (iter.hasNext()){
//			File.out.println(iter.next().toString());
//		}
	}
	
	private void translate(Program prog) /* throws */ {
		if (prog == null) {
			return;
		}
		if (prog instanceof DeclarationProgram) {
			translate((DeclarationProgram) prog);
			return;
		}
		else if (prog instanceof FunctionProgram) {
			translate((FunctionProgram) prog);
			return;
		}
	}

	private void translate(DeclarationProgram prog) /* throws */ {
		if (prog == null) {
			return;
		}
		translate(prog.decl);
		translate(prog.prog);
	}

	private void translate(FunctionProgram prog) /* throws */ {
		if (prog == null) {
			return;
		}
		translate(prog.func);
		translate(prog.prog);
	}

	private void translate(Declaration don) /* throws */ {
		if (don == null) {
			return;
		}
		translate(don.typeSpec);
		translate(don.initDors);
	}

	private void translate(FunctionDefinition func) /* throws */ {
		if (func == null) {
			return;
		}
		Function funcType = (Function)func.type;
		lFunc.add(funcType);
//		funcType.label = new Label(funcType.name);
//		add(funcType.label);
//		translate(func.typeSpec);
//		translate(func.plainDor);
//		translate(func.paraList);
		funcType.label = new FunctionLabel(funcType);
		add(funcType.label);
		
		nowFunc = funcType;
		translate(func.compStmt, null, null);
		
//		add(new OpQuad(SP, SP, "+", funcType.memSize));
		add(new FunctionEnd(funcType));
		nowFunc = null;
	}

	private void translate(Parameters paraList) /* throws */ {
		if (paraList == null) {
			return;
		}
		translate(paraList.plainDon);
		translate(paraList.paraList);
	}

	private void translate(Declarators dorList) /* throws */ {
		if (dorList == null) {
			return;
		}
		translate(dorList.dor);
		translate(dorList.dorList);
	}

	private void translate(InitDeclarators initDors) /* throws */ {
		if (initDors == null) {
			return;
		}
		
		translate(initDors.initDor);
		translate(initDors.initDors);
	}

	private void translate(InitDeclarator initDor) /* throws */ {
		if (initDor == null) {
			return;
		}
//		table.push(initDor.sym, initDor.type);
		//codeGen: put the initializer of big Struct or Array or String in Static & use Memcpy
		VarInfo varInfo = initDor.varInfo;
		Type type = varInfo.type;
		if (nowFunc != null){				//in Stack
			if (initDor.init != null){
				Operand o;
				if (varInfo.register == null)
					o = load(type, varInfo.mem);
				else o = varInfo.register;
				translate(initDor.init, type, o);
			}
		}
		else {								//in Static
			varInfo.register = null;
			Label label = new Label();
			static_travel(initDor.init);
			pseudoStatic.add(".align 2");
			pseudoStatic.add(label+":");
			static_allocate(initDor.init, type, type.size());
			varInfo.mem = label;
		}
	}
	private void static_travel(Initializer init ){
		if (init == null){
			return;
		}
		translate(init.exp);
	}
	private void static_allocate(Initializer init, Type type, int remain) {
		if (init == null){
			pseudoStatic.add(".space " + remain);
			return;
		}
		if (init instanceof Initializers){
			static_allocate((Initializers) init, type, remain);
		}
		if (init.exp.rand instanceof Immediate){
			pseudoStatic.add(".word " + (Immediate) init.exp.rand);
			remain -= 4;
		}
		else if (init.exp.label != null){
			pseudoStatic.add(".word " + init.exp.label);
			remain -= 4;
		}
		if (remain != 0) pseudoStatic.add(".space " + remain);
	}
	
	private void static_allocate(Initializers init, Type type, int remain) {
		if (init == null) return;
		pseudoStatic.add(".space " + remain);
		
	}
	private void translate(Initializer init, Type type, Operand rand) /* throws */ {//codeGen: the InitializerList
		if (init == null) {
			return;
		}
		if (init instanceof Initializers) {
			translate((Initializers) init, type, rand);
			return;
		}
//		PRINT("Initializer");
		translate(init.exp);
//		PRINT(init.exp.rand);
		addOpType(rand, type, "=", init.exp.rand, init.exp.type); 
	}

	private void translate(Initializers initList, Type type, Operand rand) /* throws */ {
		if (initList == null) {
			return;
		}
		
//		PRINT("InitializerList");
		translate(initList.init, type ,rand);
//		translate(initList.initList, type, rand);
	}

	private void translate(TypeSpecifier typeSpec) /* throws */ {
		if (typeSpec == null) {
			return;
		}
//		translate(typeSpec.op);
//		translate(typeSpec.sym);
		translate(typeSpec.memList);
	}

	private void translate(Members memList) /* throws */ {
		if (memList == null) {
			return;
		}
		translate(memList.typeSpec);
		translate(memList.dorList);
		translate(memList.memList);
	}

	private void translate(PlainDeclaration plainDon) /* throws */ {
		if (plainDon == null) {
			return;
		}
		translate(plainDon.typeSpec);
		translate(plainDon.dor);
	}

	private void translate(Declarator dor) /* throws */ {
		if (dor == null) {
			return;
		}
		if (dor instanceof FunctionDeclarator) {
			translate((FunctionDeclarator) dor);
			return;
		}
		else if (dor instanceof DataDeclarator) {
			translate((DataDeclarator) dor);
			return;
		}
	}

	private void translate(FunctionDeclarator funDor) /* throws */ {
		if (funDor == null) {
			return;
		}
		translate(funDor.plainDor);
		translate(funDor.paraList);
	}

	private void translate(DataDeclarator dataDor) /* throws */ {
		if (dataDor == null) {
			return;
		}
		translate(dataDor.plainDor);
		translate(dataDor.bracConList);
	}

	private void translate(BracketConstants bracConList) /* throws */ {
		if (bracConList == null) {
			return;
		}
		translate(bracConList.consExp);
		translate(bracConList.bracConList);
	}

	private void translate(PlainDeclarator plainDor) /* throws */ {
		if (plainDor == null) {
			return;
		}
		translate(plainDor.stars);
//		translate(plainDor.sym);
	}
	
	
//start-------------------------------statement---------------------------//
//	private void translate(Statement stat) /* throws */ {
//		if (stat == null) {
//			return;
//		}
//		if (stat instanceof ExpressionStatement) {
//			translate((ExpressionStatement) stat);
//			return;
//		}
//		else if (stat instanceof CompoundStatement) {
//			translate((CompoundStatement) stat, null, null);
//			return;
//		}
//		else if (stat instanceof SelectionStatement) {
//			translate((SelectionStatement) stat);
//			return;
//		}
//		else if (stat instanceof IterationStatement) {
//			translate((IterationStatement) stat);
//			return;
//		}
//		else if (stat instanceof ReturnJumpStatement){
//			translate((ReturnJumpStatement) stat);
//			return;
//		}
//	}
//	
	private void translate(Statement stat,  Label labelStart, Label labelEnd){
		PRINT(" ");
		add(new Formalation());
		if (stat == null){
			return;
		}
		else if (stat instanceof ExpressionStatement) {
			translate((ExpressionStatement) stat);
			return;
		}
		else if (stat instanceof SelectionStatement) {
			translate((SelectionStatement) stat, labelStart, labelEnd);
			return;
		}
		else if (stat instanceof IterationStatement) {
			translate((IterationStatement) stat);
			return;
		}
		else if (stat instanceof ReturnJumpStatement){
			translate((ReturnJumpStatement) stat);
			return;
		}
		else if (stat instanceof CompoundStatement){
			translate((CompoundStatement) stat, labelStart, labelEnd);
		}
		else if (stat instanceof ContinueJumpStatement){//cope Continue
			add(new Jump(labelStart));
		}
		else if (stat instanceof BreakJumpStatement){//cope break
//			File.out.println(labelEnd.toString());
			add(new Jump(labelEnd));
		}
	}
	
	private void translate(ExpressionStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		translate(stat.exp);
	}

	private void translate(CompoundStatement compStmt,  Label labelStart, Label labelEnd) /* throws */ {
//		table.upLevel();
		if (compStmt == null) {
			return;
		}
		translate(compStmt.donList);
		translate(compStmt.statList,  labelStart, labelEnd);
//		table.downLevel();
	}

	private void translate(Declarations donList) /* throws */ {
		if (donList == null) {
			return;
		}
		translate(donList.don);
		translate(donList.donList);
	}

	private void translate(Statements statList ,  Label labelStart, Label labelEnd) /* throws */ {
		if (statList == null) {
			return;
		}
		translate(statList.stat, labelStart, labelEnd);
		translate(statList.statList, labelStart, labelEnd);
	}

	private void add(Quad q){
		intermediateCode.add(q);
		if (q instanceof Label)
		File.out.println(q);
		else 
			File.out.println("\t"+q);
	}
	
	private void translate(SelectionStatement stat, Label labelStart, Label labelE) /* throws */ {
		if (stat == null) {
			return;
		}
		Label labelElse = new Label();
		Label labelEnd = new Label();
		Label labelThen = new Label();
		
		translate(stat.exp);  //cope If
		Operand a = stat.exp.rand;
		add(new Bez(a, labelElse));
		//if (stat )
		translate(stat.ifStat, labelStart, labelE);
		add(new Jump(labelEnd));
		add(labelElse);
		translate(stat.elseStat, labelStart, labelE);
		add(labelEnd);
	}

	private void translate(IterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		if (stat instanceof ForIterationStatement) {
			translate((ForIterationStatement) stat);
			return;
		}
		else if (stat instanceof WhileIterationStatement) {
			translate((WhileIterationStatement) stat);
			return;
		}
	}

	private void translate(ForIterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		Label labelStart = new Label();
		Label labelNext = new Label(); 
		Label labelEnd = new Label();
		
		translate(stat.exp1);
		add(labelStart);
		
		translate(stat.exp2);//todo Change Operand a to 注释型。以及附近的a都要改
		Operand a = stat.exp2.rand;
		
		add(new Bez(a, labelEnd));
		
		translate(stat.stat, labelNext, labelEnd);
		
		add(labelNext);
		nowFor = (ForIterationStatement) stat;
		translate(stat.exp3);
		
		add(new Jump(labelStart));
		add(new Quad(null, nowFor.register, null));
		add(labelEnd);
		
		nowFor = null;
	}

	private void translate(WhileIterationStatement stat) /* throws */ {
		if (stat == null) {
			return;
		}
		
		Label labelStart = new Label();
		Label labelEnd = new Label();
		
		add(labelStart);
		translate(stat.exp);
		Operand a = stat.exp.rand;
		add(new Bez(a, labelEnd));
		translate(stat.stat, labelStart, labelEnd);
		add(new Jump(labelStart));
		add(labelEnd);
		
	}

//	
//	private void translate(JumpStatement stat) /* throws */ {
//		if (stat == null) {
//			return;
//		}
//		if (stat instanceof ContinueJumpStatement) {
//			translate((ContinueJumpStatement) stat);
//			return;
//		}
//		else if (stat instanceof BreakJumpStatement) {
//			translate((BreakJumpStatement) stat);
//			return;
//		}
//		else if (stat instanceof ReturnJumpStatement) {
//			translate((ReturnJumpStatement) stat);
//			return;
//		}
//	}
//
//	private void translate(ContinueJumpStatement stat) /* throws */ {
//		if (stat == null) {
//			return;
//		}
//	}
//
//	private void translate(BreakJumpStatement stat) /* throws */ {
//		if (stat == null) {
//			return;
//		}
//	}

	private void translate(ReturnJumpStatement stat) /* throws */ {
		if (stat == null) {	
			return;
		}
		translate(stat.exp);
		if (stat.exp != null) //or changed to nowFunc.returnType !instanceof Void
			add(new FunctionReturn(nowFunc, stat.exp.rand));
	}

	private void translate(Expression exp){
		translate(exp, null, null);
	}
	private void translate(Expression exp, Label trueLabel, Label falseLabel) /* throws */ {
		if (exp == null) {
			return;
		}
		
		if (exp.type instanceof Immediate){
			if (((Immediate)exp.type).type() instanceof Pointer){
				Label l = new Label();
				Intermediate.pseudoStatic.add(".align 2");
				Intermediate.pseudoStatic.add(l+":");
				String s = (String) ((Immediate)exp.type).content();
				
				for (int i = 0; i < s.length(); i++){
					File.out.println(s.charAt(i));
					
				}
				s.replace("\\n", "aswdf");
				
				Intermediate.pseudoStatic.add
					(".asciiz \"" + s +"\\000\"");
				Temp t = new Temp();
				add(new LoadAddr(t, l));
				exp.rand = t;
				exp.label = l;
			}
			else exp.rand = (Immediate) exp.type;
//			return exp.rand;
		}
		else if (exp instanceof BinaryExpression) {
			translate((BinaryExpression) exp, trueLabel, falseLabel);
//			File.out.println("binExp" + exp.rand);
//			return null;
		}
		else if (exp instanceof CastExpression) {
			translate((CastExpression) exp);
//			File.out.println("castExp" + exp.rand);
//			return null;
		}
		else if (exp instanceof PrimaryExpression) {
			if (((PrimaryExpression)exp).type instanceof Function)
				return;
			translate((PrimaryExpression) exp);
//			File.out.println("priExp" + exp.rand);
//			return null;
		}
		else if (exp instanceof ConstantExpression){
//			File.out.println("ConstantExp" + exp.rand);
//			return (Immediate) exp.type;
		}
		
//		return null;
	}
	
	private Operand imme2res(Operand a){
		if (a instanceof Immediate){
			Temp r = new Temp();
			add(new Load(r, a));
			return r;
		}
		else {
			return a;
		}
	}
	private Operand mem2res(Operand a){//delete
		
		if (a instanceof Memory){
			Temp r = new Temp();
			add(new Load(r, a));

			throw new Error("get a Memory Operand in a BinaryExpression");
//			return r;
		}
		else {
			return a;
		}
	}
	private Operand addOp(Operand a, String op, Operand b){
//		if (op == "-"){
//			
//		}
//		else if (op == "+"){
//			
//		}
//		if (op == "="){//不是struct的赋值。只有一位。用于+=这种不会是Struct的情况。
//			add(new Store(b, ((Temp)a).mem));
//			return a;
//		}
//		else {
			Temp returnRes = new Temp();
			a = mem2res(imme2res(a));
			b = mem2res(b);
			add(new OpQuad(returnRes, a, op, b));
			return returnRes;
//		}
//		return null;//delete
	}
	public void addMemcpy(Type type, Operand a, Operand b){//codeGen
		add(new Memcpy(type.size(), b, a));
	}
	public boolean isValue(Type type){
		if ((type instanceof Int) || (type instanceof Char)) return true;
		if (type instanceof Immediate){
			if ((((Immediate)type).type() instanceof Int) || (((Immediate)type).type() instanceof Char)){
				return true;
			}
			else return false;
		}
		return false;
	}
	public boolean isPointer(Type type){//mention: lose Function pointer
		if ((type instanceof Array) || (type instanceof Pointer)) {
			return true;
		}
		return false;
	}
//	public boolean isStruct(Type type){
//		if (type instanceof Struct) return true;
//		return false;
//	}
	public Type elementType(Type type){
		if (type instanceof Pointer){
			return ((Pointer)type).elementType();
		}
		else if (type instanceof Array){
			return ((Array)type).elementType();
		}
		else throw new Error("Want one's element type, but the one is not Pointer or Array");
			
	}
	private Operand addOpType(Operand lRand, Type lType, String op, Operand rRand, Type rType){//需要type信息的处理。
		if (op == "="){
//			PRINT(a.toString());
			if (lType instanceof Name) {
				add(new Memcpy(lType.size(), rRand, lRand));
//				addMemcpy(lType, rRand, lRand);
			}
			else //add(new Store(rRand, ((Temp)lRand).mem, lType));
				storeVar(rRand, lRand, lType);
			return rRand;
		}
		else if (op == "+"){
			
			if (isValue(lType)) {
				if (isValue(rType)){
					return addOp(lRand, op ,rRand);
				}	
				else if	(isPointer(rType)) {
//					addOp(rType, );
					Operand o = addOp(lRand, "*", new Immediate(rType.size()));
					return addOp(rRand, op, o);
				}
				else {
					throw new Error("Add a int with a Function?");
				}
			}
			else if (isPointer(lType) && isValue(rType)){
				Operand o = addOp(rRand, "*", new Immediate(lType.size()));
				return addOp(lRand, op, o);
			}
			else throw new Error("not match +");
		}
		else if (op == "-"){
			if (isValue(lType)){
				if (isValue(rType)){
					return addOp(lRand, "-", rRand);
				}
			}
			else if (isPointer(lType)){
				if (isValue(rType)){
					Operand o = addOp(rRand, "*", new Immediate(lType.size()));
					return addOp(lRand, op, o);
				}
				else if (isPointer(rType)){
					Type type = elementType(lType);
					Operand o = addOp(lRand, "-", rRand);
					return addOp(o, "/", new Immediate(type.size()));
				}
			}
		}
		return null;
	}
	private void translate(BinaryExpression binExp) {
		translate(binExp, null, null);
	}
	private void translate(BinaryExpression binExp, Label trueLabel , Label falseLabel) /* throws */ {
		if (binExp == null) {
			return;
		}
		
		if (binExp.rOprand == null){	//unaryExpression, 只有左值有值。
			translate(binExp.lOprand);
			binExp.rand = binExp.lOprand.rand;
			return;
		}
		
		
//		Operand rRand = binExp.rOprand.rand;
//		translate(binExp.op);
		Operand tmp;
		switch(binExp.op){
		case LOGOR:{
			Temp temp = new Temp();
			Label ending = new Label();
			translate(binExp.lOprand);
			Type lType = binExp.lOprand.type;
			Operand lRand = binExp.lOprand.rand;
			add(new Load(temp, new Immediate(1)));
			add(new Bne(lRand, ZERO, ending));
			translate(binExp.rOprand);
			Operand rRand = binExp.rOprand.rand;
			add(new Bne(rRand, ZERO, ending));
			add(new ExplicitSet(temp, new Immediate(0)));
			add(ending);
			binExp.rand = temp;	break;
		}
		case LOGAND:{
			Temp temp = new Temp();
			Label ending = new Label();
			
			if (falseLabel == null){
				translate(binExp.lOprand, null, ending);
			}
			else {
				translate(binExp.lOprand, null, falseLabel);
			}
			Type lType = binExp.lOprand.type;
//			Type rType = binExp.rOprand.type;
			Operand lRand = binExp.lOprand.rand;
			add(new Move(temp, ZERO));
			if (falseLabel == null){
				
				add(new Beq(lRand, ZERO, ending));
			}
			else {
				add(new Beq(lRand, ZERO, falseLabel));
			}
			
			if (falseLabel == null){
				translate(binExp.rOprand, null, ending);
			}
			else {
				translate(binExp.rOprand, null, falseLabel);
			}
			Operand rRand = binExp.rOprand.rand;
			
			if (falseLabel == null){
				add(new Beq(rRand, ZERO, ending));
			}
			else {
				add(new Beq(rRand, ZERO, falseLabel));
			}
			
			add(new ExplicitSet(temp, new Immediate(1)));
			add(ending);
			binExp.rand = temp;	break;
		}
		default:{
			translate(binExp.lOprand);
			translate(binExp.rOprand);
			Type lType = binExp.lOprand.type;
			Operand lRand = binExp.lOprand.rand;
			Type rType = binExp.rOprand.type;
			Operand rRand = binExp.rOprand.rand;
			switch(binExp.op){
			case ASSIGN:
				binExp.rand = addOpType(lRand, lType,  "=" , rRand, rType);		break;
			case PLUS:
				binExp.rand = addOpType(lRand, lType,  "+", rRand, rType);		break;	
			case MINUS:
				binExp.rand = addOpType(lRand, lType, "-", rRand, rType);		break;
			case EQ:
				binExp.rand = addOp(lRand, "==", rRand);	break;
			case NE:
				binExp.rand = addOp(lRand, "!=", rRand);	break;
			case LT:
				binExp.rand = addOp(lRand, "<", rRand);		break;
			case GT:
				binExp.rand = addOp(lRand, ">", rRand);		break;
			case LE:
				binExp.rand = addOp(lRand, "<=", rRand);	break;
			case GE:
				binExp.rand = addOp(lRand, ">=", rRand);	break;
			
			
			case SHL:
				binExp.rand = addOp(lRand, "<<", rRand);	break;
			case SHR:
				binExp.rand = addOp(lRand, ">>", rRand);	break;
			case MUL:
				binExp.rand = addOp(lRand, "*", rRand);		break;
			case DIV:
				binExp.rand = addOp(lRand, "/", rRand);		break;
			case MOD:
				binExp.rand = addOp(lRand, "%", rRand);		break;
			case XOR:
				binExp.rand = addOp(lRand, "^", rRand);		break;
			case AND:
				binExp.rand = addOp(lRand, "&", rRand);		break;
			case OR:
				binExp.rand = addOp(lRand, "|", rRand);		break;
			case MUL_ASSIGN:
				tmp = addOp(lRand, "*", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case DIV_ASSIGN:
				tmp = addOp(lRand, "/", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case MOD_ASSIGN:
				tmp = addOp(lRand, "%", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case SHL_ASSIGN:
				tmp = addOp(lRand, "<<", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case SHR_ASSIGN:
				tmp = addOp(lRand, ">>", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case AND_ASSIGN:
				tmp = addOp(lRand, "&", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case OR_ASSIGN:
				tmp = addOp(lRand, "|", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;	
			case XOR_ASSIGN:
				tmp = addOp(lRand, "^", rRand);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case ADD_ASSIGN:
				tmp = addOpType(lRand, lType,  "+" , rRand, rType);
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;
			case SUB_ASSIGN:
				PRINT("SUB_ASSIGN");
				tmp = addOpType(lRand, lType,  "-" , rRand, rType);	
				binExp.rand = addOpType(lRand, lType,  "=",  tmp, lType);
				break;		
			case COMMA://COMMA取右值
				binExp.rand = rRand;
				break;
			default:
				throw new Error("unkown operation in Binary expression");
			}
		}
		}
		
		
		
	}

	private void translate(ConstantExpression consExp) /* throws */ {
		if (consExp == null) {
			return;
		}
		translate(consExp.exp);
	}

	private void translate(CastExpression castExp) /* throws */ {
		if (castExp == null) {
			return;
		}
		if (castExp instanceof RealCastExpression) {
			translate((RealCastExpression) castExp);
			return;
		}
		else if (castExp instanceof UnaryExpression) {
			translate((UnaryExpression) castExp);
			return;
		}
	}

	private void translate(RealCastExpression exp) /* throws */ {
		if (exp == null) {
			return;
		}
		translate(exp.typeName);
		translate(exp.castExp);
		exp.rand = exp.castExp.rand;
	}

	private void translate(TypeName typeName) /* throws */ {
		if (typeName == null) {
			return;
		}
		translate(typeName.typeSpec);
		translate(typeName.stars);
	}

	private void translate(Stars stars) /* throws */ {
		if (stars == null) {
			return;
		}
		translate(stars.stars);
	}

	private void translate(UnaryExpression exp) /* throws */ {
		if (exp == null) {
			return;
		}
		if (exp instanceof SizeOfTypeUnaryExpression) {
			translate((SizeOfTypeUnaryExpression) exp);
			return;
		}
		else if (exp instanceof PostfixExpression) {
			translate((PostfixExpression) exp);
			return;
		}
		
		translate(exp.exp);
		Operand rand = exp.exp.rand;
		Type type = exp.exp.type;
		//caution:possible the type is null.Attention!
//		type.lValue();
//		File.out.println(exp.exp);
		Operand r;
		switch (exp.op){
		case AND:
			r =  new Temp();
			add(new LoadAddr(r, ((Temp) rand).mem));//Load a Pointer from Memory. must be int-valued
			exp.rand = r;
			break;
		case STAR:
//			r = new Temp(); 
//			add(new Load(r, rand)); //Load from Temp
//			((Temp)r).setMemory(rand);
//			exp.rand = r;
			Type elementType = null;
			if (exp.exp.type instanceof Array){
				elementType =  ((Array) exp.exp.type).elementType();
			}
			else if (exp.exp.type instanceof Pointer){
				elementType = ((Pointer) exp.exp.type).elementType();
			}
			exp.rand = load(elementType, rand);
			break;
		case PLUS:
			exp.rand = rand;
			break;
		case MINUS:
			r = addOp(ZERO,"-",rand);
			exp.rand = r;
			break;

		case NOT:
			exp.rand = addOp(ZERO, "!|", rand);//codeGen
			break;

		case LOGNOT:
			exp.rand = addOp(ZERO, "==", rand);
			break;
		case SIZEOF:
			exp.rand = (Immediate) exp.type;
			break;
		case INC:{
			Operand valueRand = addOp(rand, "+", new Immediate(new Int(), new Integer(1)));
			//add(new Store(valueRand, ((Temp)rand).mem, type) );
			storeVar(valueRand, rand, type);
			exp.rand = valueRand;
			break;
		}
		case DEC:{
			Operand valueRand = addOp(rand, "-", new Immediate(new Int(), new Integer(1)));
			//add(new Store(valueRand, ((Temp)rand).mem, type) );
			storeVar(valueRand, rand, type);
			exp.rand = valueRand;
			break;
		}
		default:throw new Error("what the operation of UnaryExpression it is?");
		}
	}
	
	private void storeVar(Operand a, Operand b, Type type){
		if (b instanceof Register){
			add(new LoadAddr(b, a));
		}
		else {
			add(new Store(a, ((Temp)b).mem, type) );
		}
	}
	
	private void translate(SizeOfTypeUnaryExpression exp) /* throws */ {
		if (exp == null) {
			return;
		}
		exp.rand = (Immediate) exp.type;
	}

	private void translate(PostfixExpression exp) /* throws */ {//exp.rand:exp返回的rand；initRand:自上传下帮助翻译的rand
		if (exp == null) {
			return;
		}
		translate(exp.priExp);
		
		Operand rand = exp.priExp.rand;
		
		if (exp.postfixs == null){
			exp.rand = rand;
		}
		else {
			
			exp.postfixs.initRand = rand;
//			if (rand != null)
//				PRINT(rand);
			translate(exp.postfixs);
			exp.rand = exp.postfixs.rand;
//			PRINT(exp);
//			PRINT(exp.rand.toString());
		}
	}

	private void translate(Postfixs postfixs) /* throws */ {
		if (postfixs == null) {
			return;
		}
		postfixs.postfix.initRand = postfixs.initRand;
		
		translate(postfixs.postfix);
		
		if (postfixs.postfixs == null){
			postfixs.rand = postfixs.postfix.rand;
		}
		else {
			postfixs.postfixs.initRand = postfixs.postfix.rand;
			
			translate(postfixs.postfixs);
			postfixs.rand = postfixs.postfixs.rand;
		}
//		File.out.println("Postfixs Rand " + postfixs.rand);
	}
	
	private void translate(Postfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
		//Remark:postfix中的type是在处理之前的Type
		if (postfix instanceof BracketPostfix) {
			translate((BracketPostfix) postfix);
			return;
		}
		else if (postfix instanceof ParenPostfix) {
			translate((ParenPostfix) postfix);
			return;
		}
		else if (postfix instanceof DotPostfix) {
			translate((DotPostfix) postfix);
			return;
		}
		else if (postfix instanceof PtrPostfix) {
			translate((PtrPostfix) postfix);
			return;
		}
		else if (postfix instanceof IncPostfix) {
			translate((IncPostfix) postfix);
			return;
		}
		else if (postfix instanceof DecPostfix) {
			translate((DecPostfix) postfix);
			return;
		}
	}
	
	
	
	private void translate(BracketPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
//		PRINT(postfix.initRand.toString());
		translate(postfix.exp);
		Operand index = postfix.exp.rand;
		Type type = postfix.type;
		
		Type elementType;
//		File.out.println(type.toString());
		if (type instanceof Array){//只有数组或者指针可能用到下标
			elementType =  ((Array) type).elementType();
		}
		else {//if (type instanceof Pointer){
			elementType = ((Pointer) type).elementType();
		}
		int size = elementType.size();
		Operand offset;
		if (index instanceof Immediate){
			offset = new Immediate( ((Immediate)index).toInt() * size);
		}
		else {
			offset = addOp(index, "*", new Immediate(size));
		}
		//PRINT(postfix.initRand.toString());
		postfix.rand = load(elementType, addOp(postfix.initRand, "+", offset));
		
//		PRINT(postfix.rand.toString());
//		PRINT(elementType);
	}

	private void translate(ParenPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
//		store()
//		PRINT("WTF?");
		if (postfix.type instanceof SpecialFunction){
			String funcName = ((SpecialFunction)postfix.type).name;
			if (funcName == "printf"){
				translate(postfix.args);
				if (postfix.args.args == null){
					add(new LoadAddr(A0, postfix.args.rand));
					add(new Load(V0, new Immediate(4)));
					add(new Syscall());
					add(new Formalation());
				}
				else{
					add(new LoadAddr(V1, printfSpaceLabel));
					Arguments args = postfix.args.args;
					int cnt = 0;
					while (args != null){
						add(new Store(args.rand, new Memory(V1, cnt*4)));
						cnt++;
						args = args.args;
					}
					
					add(new LoadAddr(FP, postfix.args.rand));//读入字符串的第一个字符串。
					
//					add(new Store(RA, new Memory(V1, cnt*4)));
					add(new Instruction("jal $printf"));
//					add(new Load(RA, new Memory(V1, cnt*4)));
				}	
//					String code = 
//					"$printf:"+
//					"move	$4	$30\n" +
//					"$label_printf_Iter:\n" +
//					"	lb	$2	0($30)\n" +
//					"	beq	$5	$2	$label_printf_special_out\n"+
//					"	bne	$0	$2	$label_printf_scan_next\n"+
//					"		ori	$2	$0	4\n"+
//					"		syscall\n"+
//					"		j $label_printf_end\n"+
//					"$label_printf_scan_next:\n"+
//					"	add	$30	$30	1\n"+
//					"	j $label_printf_Iter\n"+
//					"$label_printf_end:\n";
				//cheat:
//				Operand b = postfix.args.args.rand;
//				if (b instanceof Immediate){
//					if (((Immediate) b).type() instanceof Char){
//						add(new Load(V0, new Immediate(11)));
//						add(new Load(A0, new Immediate(((Immediate) b).toInt())));
//						add(new Syscall());
//					}
//				}
//				else {
//					add(new LoadAddr(A0, b));
//					add(new Load(V0, new Immediate(1)));
//					add(new Syscall());
//				}
				
			}
			else if (funcName == "malloc"){
				PRINT("MALLOC");
				translate(postfix.args);
				Operand a = postfix.args.rand;
				add(new Load(V0, new Immediate(9)));
				add(new Move(A0, a));
				add(new Syscall());
				postfix.rand = V0;
//				PRINT(postfix.rand.toString());
			}
			
//			add(new Load(A0, new Immediate(10)));
//			add(new Load(V0, new Immediate(11)));
//			add(new Syscall());
		}
		else {
			Function func = (Function) postfix.type;
			translate(postfix.args);//get Expression
			
			add(new OpQuad(SP, SP, "-", func.memSize));
			
			storeArgument(postfix.args, func.argList.iterator());//Store the Arguments
			add(new Jal(func.label, func));
			
			if (!(func.returnVarInfo.type instanceof compiler.type.Void)){
//				Temp returnReg = new Temp();
//				add(new Load(returnReg, func.returnVarInfo.mem, func.returnVarInfo.type));
				Operand returnReg;
				if (func.returnVarInfo.type instanceof Name){
					returnReg = new Temp();
					add(new Load(returnReg, func.returnVarInfo.mem));
				}
				else {
					returnReg = load(func.returnVarInfo.type, func.returnVarInfo.mem);
				}
				postfix.rand = returnReg;
			}		
			
			add(new OpQuad(SP, SP, "+", func.memSize));
		}
	}
	private void translate(Arguments args) /* throws */ {
//		PRINT("asdf");
		if (args == null) {
			return;
		}
//		VarInfo varInfo = iter.next();
		translate(args.binExp);
		args.rand = args.binExp.rand;
		//add(new Store(rand, varInfo.mem));
		translate(args.args);
	}
	private void storeArgument(Arguments args, Iterator<VarInfo> iter){
		if (args == null) {
			return;
		}
		VarInfo varInfo = iter.next();
		if (varInfo.type instanceof Name){
			add(new Memcpy(varInfo.type.size(), args.rand, varInfo.mem));
		}
		else {
//			if (varInfo.mem instanceof Register){
//				add(new LoadAddr())
//			}
			//critical:	if the iterator variable passed by argument, Crash
			add(new Store(args.rand, varInfo.mem, varInfo.type));
			//storeVar(args.rand, varInfo.mem, varInfo.type);
		}
		storeArgument(args.args, iter);
	}

	private void translate(DotPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
		Type type = postfix.type;
		
		if (type instanceof Name){
			Record nameType = (Record) ((Name) type).type;
			int offset = nameType.find(postfix.sym);
			Type fieldType = nameType.getType(postfix.sym);
			postfix.rand = load(fieldType, addOp(postfix.initRand, "+", new Immediate(offset)));
		}
//		translate(postfix.sym);
	}

	private void translate(PtrPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
		Type type = postfix.type;
		
//		Type elementType = ((Pointer) type).elementType();
//		Operand rand = load(elementType, postfix.initRand);
		
//		Operand rand = new Temp();
//		ADD(NEW LOAD(RAND, POSTFIX.INITRAND));
		if (!(postfix.initRand instanceof Temp))
			throw new Error("the PtrPostfix get not Temp");
		 
		if (type instanceof Pointer){
			Record nameType = (Record) ((Name)((Pointer)type).elementType()).type;
			int offset = nameType.find(postfix.sym);
//			File.out.println(offset);
			Type fieldType = nameType.getType(postfix.sym);
			postfix.rand = load(fieldType, addOp(postfix.initRand, "+", new Immediate(offset)));
		}
//		translate(postfix.sym);
	}

	private void translate(IncPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
		Operand valueRand = addOp(postfix.initRand, "+", new Immediate(new Int(), new Integer(1)));
		//add(new Store(valueRand, ((Temp)postfix.initRand).mem, postfix.type));
		storeVar(valueRand, postfix.initRand, postfix.type);
		postfix.rand = postfix.initRand;	//mention: If use translate to handle l_value, Must delete the access of Memory
	}

	private void translate(DecPostfix postfix) /* throws */ {
		if (postfix == null) {
			return;
		}
		Operand valueRand = addOp(postfix.initRand, "-", new Immediate(new Int(), new Integer(1)));
		//add(new Store(valueRand, ((Temp)postfix.initRand).mem, postfix.type));
		storeVar(valueRand, postfix.initRand, postfix.type);
		postfix.rand = postfix.initRand;	//mention: If use translate to handle l_value, Must delete the access of Memory
	}





	private void translate(PrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return;
		}
		if (priExp instanceof IdPrimaryExpression) {
			translate((IdPrimaryExpression) priExp);
			return;
		}
		else if (priExp instanceof ConstantPrimaryExpression) {
			translate((ConstantPrimaryExpression) priExp);
			return;
		}
		else if (priExp instanceof StringPrimaryExpression) {
			translate((StringPrimaryExpression) priExp);
			return;
		}
		else if (priExp instanceof ExpPrimaryExpression) {
			translate((ExpPrimaryExpression) priExp);
			return;
		}
	}
	
	public Operand memoryOffset(Operand r, Operand s){// TODO 
		return null;
	}
	
	private Operand load(Type type, Operand o) {//load from Memory or Temp
		if ((type == null)) 
			throw new Error("null pointer in load Function of Type");
		else if (o == null){
			throw new Error("null pointer in load Function of Operand");
		}
		
		
		if (type instanceof Function) return null;
		
		Temp r = null;
		if (o instanceof Temp){
			r = (Temp) o;
		}
		else if (!((type instanceof Name) || (type instanceof Array))){
			r = new Temp();
			add(new LoadAddr(r, o)); 
		}
		Temp s = new Temp();
		
		if (type instanceof Name){
			add(new LoadAddr(s, o));
			r = s;
		}
		else if (type instanceof Array){
			if (type.isPara){
				add(new Load(s, o));
				r = s;
			}
			else {
				add(new LoadAddr(s, o));
				r = s;
			}
		}
		else if ((type instanceof Int) || (type instanceof Char) || (type instanceof Pointer)){
			add(new Load(s, o, type));
		}
		
//		if (o instanceof Memory) {
		s.setMemory(r);
//		}
		
		return s;
	}
	
	private void translate(IdPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return;
		}
//		translate(priExp.sym);
//		Temp r = new Temp();
//		add(new Load(r, priExp.varInfo.mem)); //mem:Not load the Memory,instead of pushing the memory directly in the final Temp
//		Temp s = new Temp();
//		
//		if ((priExp.type instanceof Name) || (priExp.type instanceof Array)){
//			add(new OpQuad(s, priExp.varInfo.mem.reg, "+", new Immediate(priExp.varInfo.mem.offset)));
//			s.setMemory(r);
//		}
//		else if ((priExp.type instanceof Int) || (priExp.type instanceof Char) || (priExp.type instanceof Pointer)){
//			add(new Load(s, priExp.varInfo.mem));
//			s.setMemory(r);
//		}
//		File.out.println(priExp.varInfo);
		//PRINT("IDprimaryExpression");
		if (priExp.varInfo.register != null){
//			PRINT("Symbol" + priExp.sym + ":");
//			PRINT(priExp.varInfo.register.toString());
//			if (nowFor != null)
//				nowFor.register = priExp.varInfo.register;
			priExp.rand = priExp.varInfo.register;
			//priExp.varInfo.mem = priExp.register;
		}
		else {
			
			priExp.rand = load(priExp.type, priExp.varInfo.mem);
			
		}
	}

	private void translate(ConstantPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return;
		}
//		translate(priExp.cons);
		priExp.rand = (Immediate) priExp.type;
	}

	private void translate(StringPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return;
		}
		Label l = new Label();
		Temp t = new Temp();
		add(new Load(t, l));
//	semantic(priExp.str);
		//codeGen: push the String in the static Memory
	}

	private void translate(ExpPrimaryExpression priExp) /* throws */ {
		if (priExp == null) {
			return;
		}
		translate(priExp.exp);
		priExp.rand = priExp.exp.rand;
	}

	private void translate(Constant cons) /* throws */ {
		if (cons == null) {
			return;
		}
		
	}

	private void PRINT(Object str){
		File.out.println(str);
	}

	private void PRINT(Type type){
		if (type instanceof Pointer){
			File.out.println("Pointer");
		}
		if (type instanceof Int){
			File.out.println("Int");
		}
		if (type instanceof Array){
			File.out.println("Array");
		}
		if (type instanceof Function){
			File.out.println("Function");
		}
		
		if (type instanceof Name){
			File.out.println("Name");
		}
//		if (type instanceof Int){
//			File.out.println("Int");
//		}
	}
}
