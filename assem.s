.text
$printf:
move	$4	$30
$label_printf_Iter:
	lb	$2	0($30)
	ori	$5	$0	37
	beq	$5	$2	$label_printf_special_out
	bne	$0	$2	$label_printf_scan_next
		ori	$2	$0	4
		syscall
		j $label_printf_end
$label_printf_scan_next:
	add	$30	$30	1
	j $label_printf_Iter
$label_printf_end:
j	$31
$label_printf_special_out:
	beq	$30	$4	$label_printf_check_char
	lb	$5	0($30)
	sb	$0	0($30)
	ori	$2	$0	4
	syscall
	sb	$5	0($30)
$label_printf_check_char:
	lb	$4	1($30)
	$label_printf_d:	
	ori	$2	$0	'd'
		bne	$2	$4	$label_printf_c
			lw	$4	0($3)
			ori	$2	$0	1
			syscall
			add $3	$3	4
			add	$30	$30	2
			move	$4	$30
			j	$label_printf_Iter
	$label_printf_c:
	ori	$2	$0	'c'
		bne	$2	$4	$label_printf_s
			la	$4	$$L1
			lw	$2	0($3)
			sb	$2	0($4)
			ori	$2	$0	4
			syscall
			add	$4	$30	2
			add	$30	$30	2
			add $3	$3	4
			j	$label_printf_Iter
	$label_printf_s:	
	ori	$2	$0	's'
		bne	$2	$4	$label_printf_04d
			lw	$4	0($3)
			ori	$2	$0	4
			syscall
			add $3	$3	4
			add	$4	$30	2
			add	$30	$30	2
			move	$4	$30
			j	$label_printf_Iter
	$label_printf_04d:	
		ori	$2	$0	1	
		lw	$4	0($3)
		bge $4 	1000 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	100 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	10 $$$label_printf_num
		li	$4	0
		syscall
		lw	$4	0($3)
		bge $4 	1 $$$label_printf_num
		li	$4	0
		syscall
		$$$label_printf_num:
		lw	$4	0($3)
		syscall
			add $3	$3	4
			add	$30	$30	4
			move	$4	$30
			j	$label_printf_Iter

.data
.align 2
$$L0:
.space 1024
.align 2
$$L1:
.space 4
.align 2
$$L2:
.word 2000
.align 2
$$L3:
.word 13579
.align 2
$$L4:
.word 12345
.align 2
$$L5:
.word 32768
.align 2
$$L6:
.word 86421
.align 2
$$L7:
.space 4
.align 2
$$L8:
.space 4
.align 2
$$L41:
.asciiz "%d\n\000"
.text
$$get_random:
	sw  $ra 12($sp)
	addu $t1 $sp 0
	lw $t0 $$L3 
	lw $t3 $$L6 
	mul $t2 $t0 $t3
	lw $t3 $$L4 
	add $t0 $t2 $t3
	sw $t0 0($t1)
#
	la $t1 $$L6 
	lw $t2 0($sp) 
	lw $t3 $$L5 
	rem $t0 $t2 $t3
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	lw $ra 12($sp) 
	sw  $t0 8($sp)
	j   $ra
	lw $ra 12($sp) 
	j   $ra
.text
$$sum_of:
	sw  $ra 16($sp)
#
	lw $t1 0($sp) 
	seq $t0 $t1 0
	beqz  $t0 $$L9
#
	lw $ra 16($sp) 
	sw  $0 12($sp)
	j   $ra
	j   $$L10
$$L9:
#
$$L10:
#
	lw $t0 0($sp) 
	add $t1 $t0 8
	lw $t0 0($t1)
	lw $ra 16($sp) 
	sw  $t0 12($sp)
	j   $ra
	lw $ra 16($sp) 
	j   $ra
.text
$$update:
	sw  $ra 4($sp)
#
	lw $t0 0($sp) 
	add $t1 $t0 8
	lw $t0 0($sp) 
	add $t2 $t0 12
	lw $t0 0($t2)
	sub $sp $sp 20
	sw  $t0 0($sp)
	sw  $t1 4($sp)
	jal  $$sum_of 
	lw $t1 4($sp) 
	lw $t0 12($sp) 
	add $sp $sp 20
	lw $t2 0($sp) 
	add $t3 $t2 16
	lw $t2 0($t3)
	sub $sp $sp 20
	sw  $t2 0($sp)
	sw  $t0 4($sp)
	sw  $t1 8($sp)
	jal  $$sum_of 
	lw $t0 4($sp) 
	lw $t1 8($sp) 
	lw $t3 12($sp) 
	add $sp $sp 20
	add $t2 $t0 $t3
	add $t0 $t2 1
	sw $t0 0($t1)
	lw $ra 4($sp) 
	j   $ra
.text
$$rotate_left:
	sw  $ra 12($sp)
	addu $t1 $sp 4
	lw $t0 0($sp) 
	add $t2 $t0 16
	lw $t0 0($t2)
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	add $t1 $t0 16
	lw $t0 4($sp) 
	add $t2 $t0 12
	lw $t0 0($t2)
	sw $t0 0($t1)
#
	lw $t0 4($sp) 
	add $t1 $t0 12
	lw $t0 0($sp) 
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	sub $sp $sp 8
	sw  $t0 0($sp)
	jal  $$update 
	add $sp $sp 8
#
	lw $t0 4($sp) 
	sub $sp $sp 8
	sw  $t0 0($sp)
	jal  $$update 
	add $sp $sp 8
#
	lw $t0 4($sp) 
	lw $ra 12($sp) 
	sw  $t0 8($sp)
	j   $ra
	lw $ra 12($sp) 
	j   $ra
.text
$$rotate_right:
	sw  $ra 12($sp)
	addu $t1 $sp 4
	lw $t0 0($sp) 
	add $t2 $t0 12
	lw $t0 0($t2)
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	add $t1 $t0 12
	lw $t0 4($sp) 
	add $t2 $t0 16
	lw $t0 0($t2)
	sw $t0 0($t1)
#
	lw $t0 4($sp) 
	add $t1 $t0 16
	lw $t0 0($sp) 
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	sub $sp $sp 8
	sw  $t0 0($sp)
	jal  $$update 
	add $sp $sp 8
#
	lw $t0 4($sp) 
	sub $sp $sp 8
	sw  $t0 0($sp)
	jal  $$update 
	add $sp $sp 8
#
	lw $t0 4($sp) 
	lw $ra 12($sp) 
	sw  $t0 8($sp)
	j   $ra
	lw $ra 12($sp) 
	j   $ra
.text
$$insert_node:
	sw  $ra 20($sp)
#
	lw $t1 0($sp) 
	seq $t0 $t1 0
	beqz  $t0 $$L12
#
	lw $t0 4($sp) 
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	j   $$L13
$$L12:
#
$$L13:
#
	lw $t1 0($sp) 
	add $t0 $t1 0
	lw $t1 0($t0)
	lw $t2 4($sp) 
	add $t0 $t2 0
	lw $t2 0($t0)
	sgt $t0 $t1 $t2
	beqz  $t0 $$L15
#
	lw $t0 0($sp) 
	add $t1 $t0 12
	lw $t2 0($sp) 
	add $t0 $t2 12
	lw $t2 0($t0)
	lw $t0 4($sp) 
	sub $sp $sp 24
	sw  $t2 0($sp)
	sw  $t0 4($sp)
	sw  $t1 8($sp)
	jal  $$insert_node 
	lw $t1 8($sp) 
	lw $t0 16($sp) 
	add $sp $sp 24
	sw $t0 0($t1)
	j   $$L16
$$L15:
#
	lw $t0 0($sp) 
	add $t1 $t0 16
	lw $t2 0($sp) 
	add $t0 $t2 16
	lw $t2 0($t0)
	lw $t0 4($sp) 
	sub $sp $sp 24
	sw  $t2 0($sp)
	sw  $t0 4($sp)
	sw  $t1 8($sp)
	jal  $$insert_node 
	lw $t1 8($sp) 
	lw $t0 16($sp) 
	add $sp $sp 24
	sw $t0 0($t1)
$$L16:
#
	lw $t0 0($sp) 
	sub $sp $sp 8
	sw  $t0 0($sp)
	jal  $$update 
	add $sp $sp 8
#
	lw $t0 0($sp) 
	add $t1 $t0 12
	lw $t0 0($t1)
	sne $t1 $t0 0
	move $t0 $0 
	beq $t1 $0 $$L21
	lw $t2 0($sp) 
	add $t1 $t2 12
	lw $t2 0($t1)
	add $t1 $t2 4
	lw $t2 0($t1)
	lw $t3 0($sp) 
	add $t1 $t3 4
	lw $t3 0($t1)
	sgt $t1 $t2 $t3
	beq $t1 $0 $$L21
	li $t0 1
$$L21:
	beqz  $t0 $$L18
#
	lw $t0 0($sp) 
	sub $sp $sp 16
	sw  $t0 0($sp)
	jal  $$rotate_right 
	lw $t0 8($sp) 
	add $sp $sp 16
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	j   $$L19
$$L18:
#
$$L19:
#
	lw $t0 0($sp) 
	add $t1 $t0 16
	lw $t0 0($t1)
	sne $t1 $t0 0
	move $t0 $0 
	beq $t1 $0 $$L25
	lw $t2 0($sp) 
	add $t1 $t2 16
	lw $t2 0($t1)
	add $t1 $t2 4
	lw $t2 0($t1)
	lw $t3 0($sp) 
	add $t1 $t3 4
	lw $t3 0($t1)
	sgt $t1 $t2 $t3
	beq $t1 $0 $$L25
	li $t0 1
$$L25:
	beqz  $t0 $$L22
#
	lw $t0 0($sp) 
	sub $sp $sp 16
	sw  $t0 0($sp)
	jal  $$rotate_left 
	lw $t0 8($sp) 
	add $sp $sp 16
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	j   $$L23
$$L22:
#
$$L23:
#
	lw $t0 0($sp) 
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	lw $ra 20($sp) 
	j   $ra
.text
$$get_kth_element:
	sw  $ra 20($sp)
	addu $t0 $sp 8
	sw $0 0($t0)
#
	lw $t1 0($sp) 
	add $t0 $t1 12
	lw $t1 0($t0)
	sne $t0 $t1 0
	beqz  $t0 $$L26
#
	addu $t1 $sp 8
	lw $t0 0($sp) 
	add $t2 $t0 12
	lw $t0 0($t2)
	add $t2 $t0 8
	lw $t0 0($t2)
	sw $t0 0($t1)
	j   $$L27
$$L26:
#
$$L27:
#
	lw $t1 8($sp) 
	lw $t2 4($sp) 
	sgt $t0 $t1 $t2
	beqz  $t0 $$L29
#
	lw $t1 0($sp) 
	add $t0 $t1 12
	lw $t1 0($t0)
	lw $t0 4($sp) 
	sub $sp $sp 24
	sw  $t1 0($sp)
	sw  $t0 4($sp)
	jal  $$get_kth_element 
	lw $t0 16($sp) 
	add $sp $sp 24
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	j   $$L30
$$L29:
#
$$L30:
#
	lw $t1 8($sp) 
	lw $t2 4($sp) 
	slt $t0 $t1 $t2
	beqz  $t0 $$L32
#
	lw $t1 0($sp) 
	add $t0 $t1 16
	lw $t1 0($t0)
	lw $t0 4($sp) 
	lw $t3 8($sp) 
	sub $t2 $t0 $t3
	sub $t0 $t2 1
	sub $sp $sp 24
	sw  $t1 0($sp)
	sw  $t0 4($sp)
	jal  $$get_kth_element 
	lw $t0 16($sp) 
	add $sp $sp 24
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	j   $$L33
$$L32:
#
$$L33:
#
	lw $t0 0($sp) 
	add $t1 $t0 0
	lw $t0 0($t1)
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	lw $ra 20($sp) 
	j   $ra
.text
$$alloc_node:
	sw  $ra 20($sp)
	addu $t0 $sp 0
	li $v0 9
	li $a0 20 
syscall
	sw $v0 0($t0)
#
	lw $t1 0($sp) 
	add $t0 $t1 12
	lw $t2 0($sp) 
	add $t1 $t2 16
	sw $0 0($t1)
	sw $0 0($t0)
#
	lw $t1 0($sp) 
	add $t0 $t1 8
	li $s8 1
	sw $s8 0($t0)
#
	lw $t0 0($sp) 
	add $t1 $t0 4
	sub $sp $sp 16
	sw  $t1 4($sp)
	jal  $$get_random 
	lw $t1 4($sp) 
	lw $t0 8($sp) 
	add $sp $sp 16
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	add $t1 $t0 0
	sub $sp $sp 16
	sw  $t1 4($sp)
	jal  $$get_random 
	lw $t1 4($sp) 
	lw $t0 8($sp) 
	add $sp $sp 16
	sw $t0 0($t1)
#
	lw $t0 0($sp) 
	lw $ra 20($sp) 
	sw  $t0 16($sp)
	j   $ra
	lw $ra 20($sp) 
	j   $ra
.text
main:
	sub $sp $sp 8
	sw  $ra 4($sp)
#
	la $t1 $$L8 
	sub $sp $sp 24
	sw  $a2 4($sp)
	sw  $t1 8($sp)
	jal  $$alloc_node 
	lw $a2 4($sp) 
	lw $t1 8($sp) 
	lw $t0 16($sp) 
	add $sp $sp 24
	sw $t0 0($t1)
#
	la $t0 $$L7 
	sw $0 0($t0)
$$L35:
	lw $t1 $$L7 
	lw $t2 $$L2 
	slt $t0 $t1 $t2
	beqz  $t0 $$L37
#
	la $t1 $$L8 
	lw $t2 $$L8 
	sub $sp $sp 24
	sw  $a2 4($sp)
	sw  $t2 8($sp)
	sw  $t1 12($sp)
	jal  $$alloc_node 
	lw $a2 4($sp) 
	lw $t2 8($sp) 
	lw $t1 12($sp) 
	lw $t0 16($sp) 
	add $sp $sp 24
	sub $sp $sp 24
	sw  $t2 0($sp)
	sw  $t0 4($sp)
	sw  $a2 8($sp)
	sw  $t1 12($sp)
	jal  $$insert_node 
	lw $a2 8($sp) 
	lw $t1 12($sp) 
	lw $t0 16($sp) 
	add $sp $sp 24
	sw $t0 0($t1)
$$L36:
	la $t1 $$L7 
	lw $t2 $$L7 
	add $t0 $t2 1
	sw $t0 0($t1)
	j   $$L35
$$L37:
#
	la $t0 $$L7 
	sw $0 0($t0)
$$L38:
	lw $t1 $$L7 
	lw $t2 $$L2 
	sle $t0 $t1 $t2
	beqz  $t0 $$L40
#
	la $t0 $$L41 
	lw $t2 $$L8 
	lw $t1 $$L7 
	sub $sp $sp 24
	sw  $t2 0($sp)
	sw  $t1 4($sp)
	sw  $t0 12($sp)
	jal  $$get_kth_element 
	lw $t0 12($sp) 
	lw $t1 16($sp) 
	add $sp $sp 24
	la $v1 $$L0 
	sw  $t1 0($v1)
	move $s8 $t0 
jal $printf

$$L39:
	la $t1 $$L7 
	lw $t2 $$L7 
	add $t0 $t2 1
	sw $t0 0($t1)
	j   $$L38
$$L40:
#
	lw $ra 4($sp) 
	sw  $0 0($sp)
	add $sp $sp 8
	j   $ra
	lw $ra 4($sp) 
	add $sp $sp 8
	j   $ra
