all:
	mkdir -p bin
	cd src/compiler/syntactic && make
	javac src/compiler/*/*.java -classpath lib/java-cup-11a-runtime.jar:lib/gson-2.2.2.jar -d bin
clean:
	cd src/compiler/syntactic && make clean
	rm -rf bin
