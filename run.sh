#!/bin/bash

timeo=10s
outdir=$(pwd)


  source ./finaltermvars.sh #can't ensure SC here 
export PATH=$PATH:$outdir/statspim/spim
normaldir=$(pwd)/compiler2014-testcases/Normal
  wrong=0
   cd bin 
  for filec in $(ls $normaldir/*.c); do
	filea=${filec%.c}.ans
	echo $filec
	cp $filec data.c
	timeout $timeo $CCHK 1>assem.s
	cp data.s assem.s
	timeout $timeo spim  -stat -file assem.s 1> spimout
	diff spimout $filea
	echo $filec
	sleep 1
  done
  
