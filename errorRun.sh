#!/bin/bash
set -x
#Note that chinese name of directory may not be supported.
#make clean
#make all

#prepare outputs directory
mkdir -p outputs && rm outputs/*

source midtermvars.sh
for i in $(ls compiler2014-testcases/CompileError); do
	cp compiler2014-testcases/CompileError/$i bin/data.c
#for i in $(ls compiler2014-testcases/Normal); do
#	cp compiler2014-testcases/Normal/$i bin/data.c
	cd bin
	$CCHK data.c > ../outputs/${i%.*}.s
	echo "exiting status : $?"
	cd ..
done


